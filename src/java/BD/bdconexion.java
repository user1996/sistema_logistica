package BD;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

public final class bdconexion {

    private final String BD;
    public DataSource datasource;
    Connection con;

    public bdconexion(String dbname) {
       this.BD=dbname;
       InicializarDataSource();
    }
    public Connection getConnection(){
        try {
            con=datasource.getConnection();
        } catch (SQLException e) {
            System.err.println("Error al conectar con la BD "+e.getMessage());
        }
        return con;
    }
    public void InicializarDataSource() {
        BasicDataSource basicdata = new BasicDataSource();
        basicdata.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        basicdata.setUsername("root1996");
        basicdata.setPassword("12marzo1996");
        basicdata.setUrl("jdbc:sqlserver://192.168.2.3:1433;databaseName=" + BD);
        basicdata.setMaxActive(10);
        datasource = basicdata;
    }
}
