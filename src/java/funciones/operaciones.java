package funciones;

import bean.beanalarma;
import bean.beanalerta;
import bean.beandetalleordencompra;
import bean.beannotificacion;
import bean.beanrequerimiento;
import bean.beanresultado;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class operaciones {

    public static void main(String[] args) {
        operaciones op = new operaciones();
        System.err.println(op.redondear(78.969121, 2));
    }
    public List ListaIds(ArrayList<beanalerta> list) {
        List listaLimpia = new ArrayList();
        Map<Integer, String> mapaalerta = new HashMap<Integer, String>(list.size());
        for (beanalerta b : list) {
            mapaalerta.put(b.getIdalerta(), b.getFechavencimiento());
        }
        for (Map.Entry<Integer, String> co : mapaalerta.entrySet()) {
            listaLimpia.add(co.getKey());
        }
        return listaLimpia;
    }
    public double redondear(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
        return resultado;
    }

    public boolean ValidaNumero(String caracter) {
        return caracter.matches("[0-9]*");
    }

    public JsonObject getJsonDetalleRequerimiento(ArrayList<beanrequerimiento> lista) {
        JsonObject obj = null;
        JsonArray arr = new JsonArray();
        int indice = 0;
        for (beanrequerimiento br : lista) {
            obj = new JsonObject();
            br.setIndice(indice);
            obj.addProperty("indice", br.getIndice());
            obj.addProperty("idrequerimiento", br.getIdrequerimiento());
            obj.addProperty("cantidad", br.getCant());
            obj.addProperty("descripcion", br.getDescripcion());
            obj.addProperty("estado", br.getIdAlarma());
            arr.add(obj);
            indice++;
        }
        obj = new JsonObject();
        obj.add("data", arr);
        return obj;
    }

    public JsonObject getJsonDetalleReqDef(ArrayList<beannotificacion> lista) {
        JsonObject obj = null;
        JsonArray arr = new JsonArray();
        int indice = 0;
        for (beannotificacion br : lista) {
            obj = new JsonObject();
            br.setIndice(indice);
            obj.addProperty("indice", br.getIndice());
            obj.addProperty("cantidad", br.getCant());
            obj.addProperty("descripcion", br.getDescripcion());
            obj.addProperty("precio", br.getPrecio());
            obj.addProperty("subtotal", redondear(br.getCant() * br.getPrecio(), 2));
            obj.addProperty("razonsocial", br.getNomprov());
            obj.addProperty("ruc", br.getRuc());
            obj.addProperty("fecha", br.getFecha());
            obj.addProperty("nroorden", br.getCorrelativo());
            arr.add(obj);
            indice++;
        }
        obj = new JsonObject();
        obj.add("data", arr);
        return obj;
    }

    public JsonObject getJsonReqCompra(ArrayList<beannotificacion> lista) {
        JsonObject obj = null;
        JsonArray arr = new JsonArray();
        int indice = 0;
        for (beannotificacion bn : lista) {
            obj = new JsonObject();
            bn.setIndice(indice);
            obj.addProperty("indice", bn.getIndice());
            obj.addProperty("cant", bn.getCant());
            obj.addProperty("estado", bn.getEstado());
            obj.addProperty("idarea", bn.getIdarea());
            obj.addProperty("idusuario", bn.getIdusuario());
            obj.addProperty("precio", bn.getPrecio());
            obj.addProperty("total", bn.getSubtotal());
            obj.addProperty("descripcion", bn.getDescripcion());
            arr.add(obj);
            indice++;
        }
        obj = new JsonObject();
        obj.add("data", arr);
        return obj;
    }

    public JsonObject getJsonDetalleOrden(ArrayList<beanresultado> lista) {
        JsonObject obj = null;
        JsonArray arr = new JsonArray();
        int indice = 0;
        for (beanresultado bn : lista) {
            obj = new JsonObject();
            bn.setIndice(indice);
            obj.addProperty("indice", bn.getIndice());
            obj.addProperty("cant", bn.getCantidad());
            obj.addProperty("precio", bn.getPrecio());
            obj.addProperty("descripcion", bn.getDescripcion());
            obj.addProperty("subtotal", bn.getSubtotal());
            arr.add(obj);
            indice++;
        }
        obj = new JsonObject();
        obj.add("data", arr);
        return obj;
    }
    public JsonObject getJsonDetAlarma(ArrayList<beanalerta> lista) {
        JsonObject obj = null;
        JsonArray arr = new JsonArray();
        int indice = 0;
        for (beanalerta bn : lista) {
            obj = new JsonObject();
            bn.setIndice(indice);
            obj.addProperty("idndice", bn.getIndice());
            obj.addProperty("idalerta", bn.getIdalerta());
            obj.addProperty("razonsocial", bn.getRazonsocial());
            obj.addProperty("tipo", bn.getNombretipo());
            obj.addProperty("fechavencimiento", bn.getFechavencimiento());
            obj.addProperty("iddetalle", bn.getIddetalle());
            obj.addProperty("estado", bn.getEstado());
            arr.add(obj);
            indice++;
        }
        obj = new JsonObject();
        obj.add("data", arr);
        return obj;
    }
}
