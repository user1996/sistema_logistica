package funciones;

import BD.bdconexion;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

public class functions {

    private PreparedStatement pst;
    private ResultSet rs;
    private final Connection con;
    private ResultSetMetaData rmd;
    private DefaultTableModel table = new DefaultTableModel();
    bdconexion coneBD = new bdconexion("SIS_LOGISTICA");

    public functions() {
        con = coneBD.getConnection();
    }

    public void LeerStatement(String sql) {
        try {
            pst = con.prepareStatement(sql);
        } catch (SQLException e) {
            System.err.println("Error al leer SQL " + e.getMessage());
        }
    }

    public void Parametros(Object[] parametros) {
        Object param = null;
        try {
            pst.clearParameters();
            int contador = 1;
            for (Object valor : parametros) {
                param = valor;
                pst.setObject(contador++, valor);
            }
        } catch (SQLException e) {

            System.err.println("Error al leer parametros " + e.getMessage() + " - " + param);
        }
    }

    public int Execute(int i) {
        int id = 0;
        try {
            if (i == 1) {
                id = pst.executeUpdate();
                System.err.println("Registro Actualizado");
            } else {
                rs = pst.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    System.err.println("Nuevo Registro");
                }
            }
        } catch (SQLException e) {
            System.err.println("Error al Ejecutar PreparedStatement " + e.getMessage());
        }
        FinalizarTodo();
        return id;
    }

    public void EjecutarResultSet() {
        try {
            rs = pst.executeQuery();
        } catch (SQLException e) {
            System.err.println("Error al ejecutar consulta SQL " + e.getMessage());
        }
    }

    public Object[] getDato1() {
        Object[][] dato = getDato();
        if (dato != null) {
            int cols = dato[0].length;
            int filas = dato.length;
            Object[] data = new Object[filas];
            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < cols; j++) {
                    data[i] = dato[i][j];
                }
            }
            FinalizarTodo();
            return data;
        } else {
            FinalizarTodo();
            return null;
        }
    }

    public Object[] getDato1test() {
        Object[][] dato = getDato();
        if (dato != null) {
            int cols = dato[0].length;
            int filas = dato.length;
            Object[] data = new Object[cols];
            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < cols; j++) {
                    data[j] = dato[i][j];
                }
            }
            FinalizarTodo();
            return data;
        } else {
            FinalizarTodo();
            return null;
        }
    }

    public Object[][] getDato() {
        DefaultTableModel tab = getTable();
        int cols = tab.getColumnCount();
        int rows = tab.getRowCount();
        Object[][] dato = null;
        if (rows > 0) {
            try {
                dato = new Object[rows][cols];
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < cols; j++) {
                        dato[i][j] = tab.getValueAt(i, j);
                    }
                }
            } catch (Exception e) {
                System.err.println("Error al Obtener Dato " + e.getMessage());
            }
        }
        FinalizarTodo();
        return dato;
    }

    private Object[] getCabecera() {
        try {
            rmd = rs.getMetaData();
            int cols = rmd.getColumnCount();
            Object[] cabezera = new Object[cols];
            for (int i = 0; i < cols; i++) {
                cabezera[i] = rmd.getColumnName(i + 1);
                this.table.addColumn(cabezera[i]);
            }
            return cabezera;
        } catch (SQLException e) {
            System.err.println("Error al generar Encabezados de la tabla " + e.getMessage());
            return null;
        }
    }

    public DefaultTableModel getDataTable() {
        DefaultTableModel tablita = getTable();
        FinalizarTodo();
        return tablita;
    }

    private DefaultTableModel getTable() {
        try {
            int colcant = getCabecera().length;
            Object[] array = new Object[colcant];
            while (rs.next()) {
                for (int i = 0; i < colcant; i++) {
                    array[i] = rs.getObject(i + 1);
                }
                this.table.addRow(array);
            }
        } catch (SQLException e) {
            System.err.println("Error al generar tabla " + e.getMessage());
        }
        return table;
    }

    public JsonObject getJson() {
        JsonObject obj = null;
        JsonArray array = new JsonArray();
        DefaultTableModel tablita = getTable();
        int row = tablita.getRowCount();
        int col = tablita.getColumnCount();
        Object[] cols = getCabecera();
        for (int i = 0; i < row; i++) {
            obj = new JsonObject();
            for (int j = 0; j < col; j++) {
                obj.addProperty(cols[j].toString(), tablita.getValueAt(i, j) == null ? null : tablita.getValueAt(i, j).toString());
            }
            array.add(obj);
        }
        obj = new JsonObject();
        obj.add("data", array);
        FinalizarTodo();
        return obj;

    }

    public static boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException ex) {
            resultado = false;
        }
        return resultado;
    }

    protected void FinalizarTodo() {
        this.pst = null;
        this.rmd = null;
        this.rs = null;
        this.table = new DefaultTableModel();
    }
}
