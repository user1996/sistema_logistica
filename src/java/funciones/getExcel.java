package funciones;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import javax.swing.table.DefaultTableModel;

public class getExcel {

    private HttpServletResponse response;
    private DefaultTableModel table = new DefaultTableModel();

    public getExcel(HttpServletResponse response) {
        this.response = response;
    }

    public void setHeader(Object[] data) {
        for (Object dato : data) {
            this.table.addColumn(dato);
        }
    }

    public void setData(Object[] data) {
        this.table.addRow(data);
    }

    public void ExportarExcel(String name) throws IOException {
        response.setContentType("application/vmd.ms-excel");
        response.setHeader("Content-disposition", "filename=" + name + ".xls");
        PrintWriter out = response.getWriter();
        try {
            int cols = table.getColumnCount();
            int row = table.getRowCount();
            out.print("<table id='TBLEXCEL' class='table table-hover'>");
            out.print("<thead>");
            for (int i = 0; i < table.getColumnCount(); i++) {
                out.print("<th>" + table.getColumnName(i) + "</th>");
            }
            out.print("</thead>");
            for (int i = 0; i < row; i++) {
                out.print("<tr>");
                for (int j = 0; j < cols; j++) {
                    out.print("<td>" + table.getValueAt(i, j) + "</td>");
                }
                out.print("</tr>");
            }
            out.print("</table>");
        } catch (Exception e) {
            System.err.println("Error al Exportar Excel " + e.getMessage());
        } finally {
            out.close();
        }
    }

}
