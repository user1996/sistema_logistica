package servlet;

import bean.beanpersona;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import negocio.login_neg;

@WebServlet(description = "", urlPatterns = {"/login"})
public class logincontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String nombreusuario = request.getParameter("nombreusuario"),
                claveusuario = request.getParameter("claveusuario");
        if (!nombreusuario.equals("") && !claveusuario.equals("")) {
            login_neg login = new login_neg();
            int idUsuario = login.getIdusuario(nombreusuario, claveusuario);
            if (idUsuario > 0) {
                ArrayList<beanpersona> listaSesion = login.datoSesionPersona(idUsuario);
                if (!listaSesion.isEmpty()) {
                    HttpSession sesion = request.getSession(true);
                    sesion.setAttribute("dataSesion", login.datoSesionPersona(idUsuario));
                    response.sendRedirect("index?action=index");
                } else {
                    response.sendRedirect("index?action=login");
                }
            } else {
                response.sendRedirect("index?action=login");
            }
        }else{
            response.sendRedirect("index?action=login");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);

    }
}
