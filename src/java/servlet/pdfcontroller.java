package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pdf.daopdf;

public class pdfcontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        String action = request.getParameter("action");
        daopdf dpfdf = null;
        if (action != null) {
            switch (action) {
                case "getReporte":
                    dpfdf = new daopdf();
                    String id = request.getParameter("idOrden");
                    dpfdf.ExportaPDF(id, response);
                    break;
                case "getReporteReq":
                    dpfdf = new daopdf();
                    String idOrden = request.getParameter("idOrden");
                    if (idOrden != null) {
                        dpfdf.ExportaPdfReq(idOrden, response);
                    } else {
                        response.sendRedirect("index?action=index");
                    }
                    break;
            }
        }else{
            response.sendRedirect("index?action=index");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
