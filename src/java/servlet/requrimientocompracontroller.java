package servlet;

import bean.beandetalleordencompra;
import bean.beannotificacion;
import bean.beanordencompra;
import bean.beanpersona;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import funciones.operaciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.ordenpedido_neg;
import negocio.requerimiento_neg;

public class requrimientocompracontroller extends HttpServlet {

    boolean isIgv = false;
    Object[] filtros;
    ArrayList<beannotificacion> lista, listaOld;
    ArrayList<beanpersona> listaSesion;
    ordenpedido_neg ord = null;
    beanordencompra b_ord = null;
    beandetalleordencompra b_det = null;
    requerimiento_neg r_neg = null;

    public void LimpiaPrecio(ArrayList<beannotificacion> lista, int incluye) {
        for (int i = 0; i < lista.size(); i++) {
            lista.get(i).setPrecio(0);
            lista.get(i).setIncluye(incluye);
        }
    }

    public boolean ValidaLista(ArrayList<beannotificacion> lista) {
        int contador = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getPrecio() == 0) {
                contador++;
            }
        }
        return contador > 0;
    }

    public double NuevaCantidad(ArrayList<beannotificacion> lista, double cantidad, int iddetalle) {
        double oldcant = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getIddetalle() == iddetalle) {
                oldcant = lista.get(i).getCant();
                System.err.println("-" + (oldcant - cantidad) + "-");
            }
        }
        return oldcant - cantidad;
    }

    public boolean Verifica(ArrayList<beannotificacion> listaAnterior, double cant, int iddetalle) {
        double cantidad = 0;
        for (int i = 0; i < listaAnterior.size(); i++) {
            if (listaAnterior.get(i).getIddetalle() == iddetalle) {
                cantidad = listaAnterior.get(i).getCant();
                System.err.println(cantidad + "\t" + cant);
            }
        }
        return cantidad == cant;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action != null) {
                String destino = "";
                operaciones op = null;
                Object salida = null;
                switch (action) {
                    case "index":
                        destino = "reqcompra.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "viewReq":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        if (lista != null) {
                            op = new operaciones();
                            salida = op.getJsonReqCompra(lista);
                        }
                        out.print(salida);
                        break;
                    case "checkigv":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        if (lista != null) {
                            String igv = request.getParameter("rbigv");
                            if (igv != null) {
                                if (igv.equals("1")) {
                                    isIgv = true;
                                    salida = "1";
                                } else {
                                    isIgv = false;
                                    salida = "0";
                                }
                            } else {
                                isIgv = false;
                                salida = "0";
                            }
                        } else {
                            isIgv = false;
                            salida = "0";
                        }
                        LimpiaPrecio(lista, isIgv ? 1 : 0);
                        request.getSession().setAttribute("isigv", isIgv);
                        out.print(salida);
                        break;
                    case "changeCant":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        if (lista != null) {
                            if (!lista.isEmpty()) {
                                String indice = request.getParameter("indice");
                                String cantidad = request.getParameter("cantidad");
                                if (indice != null && cantidad != null) {
                                    lista.get(Integer.parseInt(indice)).setCant(Double.parseDouble(cantidad));
                                    op = new operaciones();
                                    salida = op.getJsonReqCompra(lista);
                                }
                            } else {
                                salida = "La Lista esta vacia";
                            }
                        } else {
                            salida = "Error Interno";
                        }
                        out.print(salida);
                        break;
                    case "addPrecio":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        if (lista != null) {
                            String indice = request.getParameter("indice");
                            String precio = request.getParameter("precio");
                            if (indice != null && !precio.equals("")) {
                                isIgv = (boolean) request.getSession().getAttribute("isigv");
                                lista.get(Integer.parseInt(indice)).setIncluye(isIgv ? 1 : 0);
                                lista.get(Integer.parseInt(indice)).setPrecio(Double.parseDouble(precio));
                                op = new operaciones();
                                salida = op.getJsonReqCompra(lista);
                            }
                        }
                        out.print(salida);
                        break;
                    case "delete":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        if (lista != null) {
                            String indice = request.getParameter("indice");
                            if (indice != null) {
                                lista.remove(Integer.parseInt(indice));
                                op = new operaciones();
                                salida = op.getJsonReqCompra(lista);
                            }
                        }
                        out.print(salida);
                        break;
                    case "getPrecios":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        double[] arreglo = new double[3];
                        JsonObject obj = null;
                        JsonArray arr = new JsonArray();
                        double subtotal = 0,
                         igv = 0,
                         neto = 0;
                        if (lista != null) {
                            for (beannotificacion bn : lista) {
                                subtotal = subtotal + bn.getSubtotal();
                                igv = igv + bn.getIgv();
                                neto = neto + bn.getNeto();
                            }
                        }
                        arreglo[0] = subtotal;
                        arreglo[1] = igv;
                        arreglo[2] = neto;
                        for (int i = 0; i < arreglo.length; i++) {
                            obj = new JsonObject();
                            obj.addProperty("item" + i, arreglo[i]);
                            arr.add(obj);
                        }
                        obj = new JsonObject();
                        obj.add("data", arr);
                        out.print(obj);
                        break;
                    case "regOrden":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        if (lista != null) {
                            String idproveedor = request.getParameter("idproveedor");
                            if (!idproveedor.equals("0")) {
                                boolean isigv = false;
                                isigv = (boolean) request.getSession().getAttribute("isigv");
                                System.err.println(lista.get(0).getIdtipo()+" -> <-");
                                listaSesion = new ArrayList<>();
                                listaSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                                b_ord = new beanordencompra();
                                ord = new ordenpedido_neg();
                                b_ord.setIdproveedor(Integer.parseInt(idproveedor));
                                b_ord.setIdsede(lista.get(0).getIdsede());//Sede o Destino
                                b_ord.setCantRows(ord.Cantordenes(lista.get(0).getIdsede(), lista.get(0).getIdtipo()));
                                b_ord.setNroorden(b_ord.NumeroCorrelativo());
                                b_ord.setIdusuario(listaSesion.get(0).getIdusuario());
                                b_ord.setIdarea(lista.get(0).getIdarea());
                                b_ord.setIddestino(lista.get(0).getIdsede());
                                b_ord.setIdtipo(lista.get(0).getIdtipo());//Tipo 2 por que es requerimiento
                                b_ord.setIncluyeIGV(isigv ? 1 : 0);
                                b_ord.setIdmoneda(1);//Tipo de moneda Soles
                                //System.err.println("idSede "+lista.get(0).getIdsede()+" Idusuario "+lista.get(0).getIdusuario());
                                if (!ValidaLista(lista)) {
                                    int contador = 0;
                                    int idorden = ord.RegistraOrdenCompra(b_ord);
                                    int iddetalle = 0;
                                    if (idorden > 0) {
                                        r_neg = new requerimiento_neg();
                                        //Para ver la Lista Anterior creada
                                        filtros = (Object[]) request.getSession().getAttribute("param");
                                        int area = Integer.parseInt(filtros[0].toString());
                                        int sede = Integer.parseInt(filtros[1].toString());
                                        int estado = Integer.parseInt(filtros[2].toString());
                                        listaOld = r_neg.listaCompra(area, sede, estado);
                                        /////
                                        for (int i = 0; i < lista.size(); i++) {
                                            b_det = new beandetalleordencompra();
                                            b_det.setCantidad(lista.get(i).getCant());
                                            b_det.setIdorden(idorden);
                                            b_det.setPrecio(lista.get(i).getPrecio());
                                            b_det.setIgv(isigv ? 1 : 0);
                                            b_det.setDescripcionaux(lista.get(i).getDescripcion());
                                            iddetalle = ord.RegistraDetalleOrdenCompraReq(b_det);
                                            if (iddetalle > 0) {
                                                if (!Verifica(listaOld, lista.get(i).getCant(), lista.get(i).getIddetalle())) {
                                                    r_neg.updateDetalleRequerimiento(lista.get(i).getIddetalle(), NuevaCantidad(listaOld, lista.get(i).getCant(), lista.get(i).getIddetalle()));
                                                } else {
                                                    r_neg.Actualizaestado(lista.get(i).getIdalarma(), 3, "SE REALIZO COMPRA");
                                                }
                                                contador++;
                                            }
                                        }
                                        salida = contador == lista.size() ? "1" : "0";
                                    } else {
                                        salida = "Orden no Registrada";
                                    }
                                } else {
                                    salida = "Ingrese Precios";
                                }
                            } else {
                                salida = "Seleccione Proveedor";
                            }
                        } else {
                            salida = "No Registrado";
                        }
                        out.print(salida);
                        break;
                    case "test":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataCompra");
                        for (beannotificacion b : lista) {
                            out.print(b.getCant() + "\t" + b.getDescripcion() + "\t" + b.getIddetalle());
                        }
                        break;
                }
            } else {
                response.sendRedirect("index?action=index");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
