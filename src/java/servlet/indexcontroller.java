package servlet;

import bean.beanpersona;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.requerimiento_neg;

public class indexcontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            String destino = "";
            if (action != null) {
                switch (action) {
                    case "login":
                        destino = "login.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "index":
                        destino = "index.jsp";
                        ArrayList<beanpersona> listaSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                        if (listaSesion != null && !listaSesion.isEmpty()) {
                            String tipousuario = listaSesion.get(0).getNombretipo();
                            requerimiento_neg req_n = new requerimiento_neg();
                            if (tipousuario.equals("ADMINISTRADOR")) {
                                request.getSession().setAttribute("dataNotif", req_n.listNotificacion());
                            } else {
                                request.getSession().setAttribute("dataNotif", req_n.listNotificacionUsuario(listaSesion.get(0).getIdusuario()));
                            }
                            request.getRequestDispatcher(destino).forward(request, response);
                        } else {
                            response.sendRedirect("index?action=login");
                        }
                        break;
                    case "ordServ":
                        destino = "ordenservice?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "req":
                        destino = "requerimiento?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "reqGen":
                        destino = "requerimiento?action=genReq";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listReq":
                        destino = "requerimiento?action=list";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "persona":
                        destino = "persona?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "insumo":
                        destino = "insumo.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "prov":
                        destino = "proveedor?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "cotiz":
                        destino = "cotizacion?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "orden":
                        destino = "orden?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "ordenList":
                        destino = "orden?action=list";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "alarm":
                        destino = "alarma?action=index";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    default:
                        destino = "index?action=index";
                        response.sendRedirect(destino);
                        break;
                }
            } else {
                destino = "index?action=index";
                response.sendRedirect(destino);
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
