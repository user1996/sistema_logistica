package servlet;

import bean.beancotizacion;
import bean.beanproveedor;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.cotizacion_neg;
import negocio.proveedor_neg;

public class contizacioncontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            cotizacion_neg cot = new cotizacion_neg();
            proveedor_neg pneg = new proveedor_neg();
            String action = request.getParameter("action");
            String destino = "";
            int idProveedor = 0, idInsumo = 0, idprovInsumo = 0, igv = 0;
            double precio = 0;
            Object salida = null;
            if (action != null) {
                switch (action) {
                    case "index":
                        destino = "proveedorinsumo.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "RegInsumo":
                        //Esto solo Registra el insumo en la tabla ProvInsumo
                        idInsumo = Integer.parseInt(request.getParameter("idInsumo"));
                        out.print(cot.GuardaInsumoCotizacion(idInsumo));
                        break;
                    case "RegistraProv":
                        //Esto Registra el idProveedor de la tabla ProvInsumo enterior registrada
                        idProveedor = Integer.parseInt(request.getParameter("idproveedor").equals("0") ? "0" : request.getParameter("idproveedor"));
                        idInsumo = Integer.parseInt(request.getParameter("idinsumo2"));
                        igv = Integer.parseInt(request.getParameter("rbigv") != null ? request.getParameter("rbigv") : "0");
                        precio = Double.parseDouble(request.getParameter("precio").equals("") ? "0" : request.getParameter("precio"));
                        String ruc = request.getParameter("ruc");
                        String razonSocial = request.getParameter("razonsocial");
                        if (!ruc.equals("") && !razonSocial.equals("") && precio != 0) {
                            if (idProveedor > 0) {
                                beancotizacion bcot = new beancotizacion();
                                bcot.setIdinsumo(idInsumo);
                                bcot.setIdproveedor(idProveedor);
                                bcot.setIgv(igv);
                                bcot.setPrecio(precio);
                                idprovInsumo = cot.GuardaCotizacion(bcot);
                                salida=idprovInsumo>0?idprovInsumo:"No Registrado";
                            } else {
                                //Registra el proveedor
                                beanproveedor bprov = new beanproveedor();
                                bprov.setRuc(ruc);
                                bprov.setRazonsocial(razonSocial);
                                idProveedor = pneg.GuardaProveedorAux(bprov);
                                //Actualiza el provInsumo
                                if (idProveedor > 0) {
                                    beancotizacion bcot = new beancotizacion();
                                    bcot.setIdinsumo(idInsumo);
                                    bcot.setIdproveedor(idProveedor);
                                    bcot.setIgv(igv);
                                    bcot.setPrecio(precio);
                                    idprovInsumo = cot.GuardaCotizacion(bcot);
                                    salida=idprovInsumo>0?idprovInsumo:"No Registrado";
                                }
                            }
                        }else{
                            salida="Rellene campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    case "filtroProv":
                        String filtroRuc = request.getParameter("term");
                        out.print(cot.FiltraProveedorTest(filtroRuc));
                        break;
                    case "regCotizacion":
                        idprovInsumo = Integer.parseInt(request.getParameter("idprovinsumo").equals("") ? "0" : request.getParameter("idprovinsumo"));
                        idInsumo = Integer.parseInt(request.getParameter("idinsumo").equals("") ? "0" : request.getParameter("idinsumo"));
                        idProveedor = Integer.parseInt(request.getParameter("idproveedor").equals("") ? "0" : request.getParameter("idproveedor"));
                        precio = Double.parseDouble(request.getParameter("precio").equals("") ? "0" : request.getParameter("precio"));
                        igv = Integer.parseInt(request.getParameter("rbigv") != null ? request.getParameter("rbigv") : "0");
                        if (idInsumo != 0 && idProveedor != 0 && precio != 0) {
                            beancotizacion bcot = new beancotizacion();
                            bcot.setIdprovinsumo(idprovInsumo);
                            bcot.setIdinsumo(idInsumo);
                            bcot.setIdproveedor(idProveedor);
                            bcot.setPrecio(precio);
                            bcot.setIgv(igv);
                            salida = cot.GuardaCotizacion(bcot) > 0 ? "Datos guardados !" : "No registrado !";
                        } else {
                            salida = "Ingrese campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    case "provInsumo":
                        proveedor_neg pr = new proveedor_neg();
                        out.print(pr.ListaProvInsumo());
                        break;
                    case "getCotizacion":
                        int idprovinsumo = Integer.parseInt(request.getParameter("idProvInsumo").equals("") ? "0" : request.getParameter("idProvInsumo"));
                        out.print(cot.getCotizacionId(idprovinsumo));
                        break;
                    default:
                        destino = "index?action=index";
                        response.sendRedirect(destino);
                        break;
                }
            } else {
                destino = "index?action=index";
                response.sendRedirect(destino);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
