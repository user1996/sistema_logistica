package servlet;

import bean.beantelefono;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.telefono_neg;

public class telefonocontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            telefono_neg tel = new telefono_neg();
            beantelefono btel = null;
            int idProvedor = 0, idTelefono = 0;
            Object salida = "";
            if (action != null) {
                switch (action) {
                    case "listTelf":
                        idProvedor = Integer.parseInt(request.getParameter("id"));
                        out.print(tel.getListaTelefono(idProvedor));
                        break;
                    case "regTelf":
                        idProvedor = Integer.parseInt(request.getParameter("idProveedor"));
                        String numero = request.getParameter("numerotelefono");
                        if (!numero.equals("") && idProvedor != 0) {
                            btel = new beantelefono();
                            btel.setIdProveedor(idProvedor);
                            btel.setNroTelefono(numero);
                            idTelefono = tel.GuardaTelefono(btel);
                            salida = idTelefono > 0 ? 1 : 0;
                        } else {
                            salida = "Campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    default:
                        response.sendRedirect("index?action=index");
                        break;
                }
            } else {
                response.sendRedirect("index?action=index");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
