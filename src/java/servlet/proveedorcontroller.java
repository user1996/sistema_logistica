package servlet;

import bean.beanproveedor;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.proveedor_neg;

public class proveedorcontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            String destino = "", ruc, razonsocial, direccion;
            if (action != null) {
                proveedor_neg pro = new proveedor_neg();
                beanproveedor bprov = null;
                Object salida = null;
                int idProveedor = 0, idTipo = 0;
                switch (action) {
                    case "index":
                        destino = "proveedor.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "detalle":
                        destino = "proveedordetalle.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listprov":
                        out.print(pro.ListaProveedor());
                        break;
                    case "getProv":
                        idProveedor = Integer.parseInt(request.getParameter("id"));
                        out.print(pro.getProveedorId(idProveedor));
                        break;
                    case "regProv":
                        idProveedor = Integer.parseInt(request.getParameter("idproveedor"));
                        ruc = request.getParameter("ruc");
                        razonsocial = request.getParameter("razonsocial");
                        direccion = request.getParameter("direccion");
                        idTipo = Integer.parseInt(request.getParameter("idtipo"));
                        int id = 0;
                        if (!ruc.equals("") && !razonsocial.equals("") && !direccion.equals("") && idTipo != 0) {
                            bprov = new beanproveedor();
                            bprov.setIdproveedor(idProveedor);
                            bprov.setRuc(ruc);
                            bprov.setRazonsocial(razonsocial);
                            bprov.setDireccion(direccion);
                            bprov.setIdtipo(idTipo);
                            pro = new proveedor_neg();
                            id = pro.GuardaProveedor(bprov);
                            salida = bprov.getIdproveedor() == 0 ? id : bprov.getIdproveedor();
                        } else {
                            salida = "Rellene campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    default:
                        destino = "index?action=index";
                        response.sendRedirect(destino);
                        break;
                }
            } else {
                destino = "index?action=index";
                response.sendRedirect(destino);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
