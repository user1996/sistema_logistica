package servlet;

import bean.beanusuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.persona_neg;

@WebServlet(description = "", urlPatterns = {"/persona"})
public class personacontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String destino = "";
        if (action != null) {
            PrintWriter out = response.getWriter();
            persona_neg per = new persona_neg();
            Object salida = null;
            String id = null;
            int idPersona = 0, idUsuario = 0;
            switch (action) {
                case "index":
                    destino = "personalist.jsp";
                    request.getRequestDispatcher(destino).forward(request, response);
                    break;
                case "list":
                    out.print(per.getLista());
                    break;
                case "listPersona":
                    id = request.getParameter("idPersona");
                    if (id != null) {
                        destino = "detallepersona.jsp";
                        idPersona = Integer.parseInt(id);
                        Object[] arreglo=per.getDetalleLista(idPersona);
                        request.getSession().setAttribute("datos", arreglo);
                        request.getRequestDispatcher(destino).forward(request, response);
                    } else {
                        destino = "index?action=persona";
                        response.sendRedirect(destino);
                    }
                    break;
                case "listaUsuario":
                    destino = "usuariolist.jsp";
                    request.getRequestDispatcher(destino).forward(request, response);
                    break;
                case "getlistausuario":
                    out.print(per.getListUsuario());
                    break;
                case "registra":
                    beanusuario busu = null;
                    String idpersona = "",
                     documento = "",
                     primernombre = "",
                     segundonombre = "",
                     apellidopaterno = "",
                     apellidomaterno = "",
                     genero = "",
                     telefono = "";
                    idpersona = request.getParameter("idpersona");
                    documento = request.getParameter("documento");
                    primernombre = request.getParameter("primernombre");
                    segundonombre = request.getParameter("segundonombre");
                    apellidopaterno = request.getParameter("apellidopaterno");
                    apellidomaterno = request.getParameter("apellidomaterno");
                    genero = request.getParameter("rbgenero");
                    telefono = request.getParameter("telefono");
                    if (!documento.equals("") && !primernombre.equals("") && !apellidopaterno.equals("") && !apellidomaterno.equals("") && genero != null) {
                        if (isNumeric(documento) && documento.length() >= 8) {
                            busu = new beanusuario();
                            busu.setIdpersona(Integer.parseInt(idpersona));
                            busu.setDocumento(documento);
                            busu.setPrimernombre(primernombre);
                            busu.setSegundonombre(segundonombre);
                            busu.setApellidopaterno(apellidopaterno);
                            busu.setApellidomaterno(apellidomaterno);
                            busu.setGenero(genero);
                            busu.setTelefono(telefono);
                            idPersona = per.GuardaPersona(busu);
                            salida = idPersona > 0 ? "1" : "Error al guadar Cambios o Documento Existe";
                        } else {
                            salida = "Documento no valido";
                        }
                    } else {
                        salida = "Rellene campos obligatorios";
                    }
                    out.print(salida);
                    break;
                case "registrausuario":
                    String idusuario,
                     nombreusuario,
                     claveusuario,
                     idsede,
                     personaid,
                     idtipo,
                     idarea;
                    idusuario = request.getParameter("idusuario");
                    nombreusuario = request.getParameter("nombreusuario");
                    claveusuario = request.getParameter("claveusuario");
                    idsede = request.getParameter("idsede");
                    personaid = request.getParameter("idpersona");
                    idtipo = request.getParameter("idtipo");
                    idarea=request.getParameter("idarea");
                    if (!nombreusuario.equals("") && !claveusuario.equals("") && !idsede.equals("0") && !personaid.equals("0") && !idtipo.equals("0") && !idarea.equals("0")) {
                        busu = new beanusuario();
                        busu.setIdusuario(Integer.parseInt(idusuario));
                        busu.setIdpersona(Integer.parseInt(personaid));
                        busu.setIdtipo(Integer.parseInt(idtipo));
                        busu.setNombreusuario(nombreusuario);
                        busu.setClaveusuario(claveusuario);
                        busu.setIdsede(Integer.parseInt(idsede));
                        busu.setIdarea(Integer.parseInt(idarea));
                        idUsuario = per.GuardaUsuario(busu);
                        salida = idUsuario > 0 ? idUsuario : "Erro al Guardar Cambios o Usuario Existe";
                    } else {
                        salida = "Rellene campos Obligatorios";
                    }
                    //System.err.println("idusuario "+idusuario+"\tnombreusuario "+nombreusuario+"\t clave usuario "+claveusuario+"\t idSede "+idsede+"\t idTipo "+idtipo+"\t personaId "+personaid);
                    out.print(salida);
                    break;
                default:
                    destino = "index?action=persona";
                    response.sendRedirect(destino);
                    break;
            }
        } else {
            destino = "index?action=index";
            response.sendRedirect(destino);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);

    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
