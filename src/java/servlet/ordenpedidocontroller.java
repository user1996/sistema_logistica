package servlet;

import bean.beandetalleordencompra;
import bean.beanordencompra;
import bean.beanpersona;
import funciones.operaciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.insumo_neg;
import negocio.ordenpedido_neg;

public class ordenpedidocontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String destino = "";
            String action = request.getParameter("action");
            if (action != null) {
                operaciones op = new operaciones();
                insumo_neg ins = new insumo_neg();
                ordenpedido_neg ord = new ordenpedido_neg();
                beanordencompra bor = null;
                beandetalleordencompra bdet;
                Object salida = null;
                switch (action) {
                    case "index":
                        destino = "ordenproduccion.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listaOrdenes":
                        ArrayList<beanpersona> lst = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                        out.print(ord.ListaOrden(lst.get(0).getIdtipo(),lst.get(0).getIdsede(), 1));
                        break;
                    case "listDetalle":
                        destino = "ordenproduccionlistdetalle.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listaOrdenDetalle":
                        int idOrden = Integer.parseInt(request.getParameter("idOrden"));
                        out.print(ord.ListaDetalleOrden(idOrden));
                        break;
                    case "listaOrdenDetallegetId":
                        int iddetalle = Integer.parseInt(request.getParameter("iddetalle"));
                        out.print(ord.ListaDetalleOrdenGetId(iddetalle));
                        break;
                    case "testData":
                        String codinsumo[] = request.getParameterValues("codigo[]");
                        String cantidad[] = request.getParameterValues("cantidad[]");
                        String descripcion[] = request.getParameterValues("descripcion[]");
                        String unidad[] = request.getParameterValues("unidad[]");
                        if (codinsumo != null) {
                            ArrayList<beandetalleordencompra> listita = new ArrayList<>();
                            int indice = 0;
                            for (int i = 0; i < codinsumo.length; i++) {
                                bdet = new beandetalleordencompra();
                                bdet.setIndice(indice);
                                bdet.setCodigoInsumo(codinsumo[i]);
                                bdet.setCantidad(Double.parseDouble(cantidad[i]));
                                bdet.setDescripcion(descripcion[i]);
                                bdet.setUnidad(unidad[i]);
                                listita.add(bdet);
                                indice++;
                            }
                            request.getSession().setAttribute("listaExcel", listita);
                            response.sendRedirect("orden?action=list");
                        } else {
                            response.sendRedirect("orden?action=index");
                        }
                        break;
                    case "list":
                        ArrayList<beandetalleordencompra> lista = (ArrayList<beandetalleordencompra>) request.getSession().getAttribute("listaExcel");
                        if (lista != null && !lista.isEmpty()) {
                            destino = "ordenproduccionlist.jsp";
                            ArrayList<beandetalleordencompra> listaSQL = new ArrayList<>();
                            int indice = 0;
                            for (beandetalleordencompra bd : lista) {
                                bdet = new beandetalleordencompra();
                                Object[][] idInsumo = ins.getIdInsumo(bd.getCodigoInsumo());
                                Object[] valor = ins.getInsumoConsulta(idInsumo != null ? idInsumo[0][0].toString() : "0");
                                bdet.setIndice(indice);
                                bdet.setIdInsumo(idInsumo != null ? Integer.parseInt(idInsumo[0][0].toString()) : 0);
                                bdet.setCodigoInsumo(bd.getCodigoInsumo());
                                bdet.setDescripcion(bd.getDescripcion());
                                bdet.setIdprovinsumo(valor != null ? Integer.parseInt(valor[0].toString()) : 0);
                                bdet.setIdProveedor(valor != null ? Integer.parseInt(valor[4].toString()) : 0);
                                bdet.setRazonSocial(valor != null ? valor[5].toString() : "Sin proveedor");
                                //
                                bdet.setTipo(valor != null ? valor[9].toString() : "SIN NADA");
                                //                                
                                bdet.setUnidad(bd.getUnidad());
                                bdet.setUnidadmedida(valor != null ? Double.parseDouble(valor[7].toString()) : 1000);
                                bdet.setUnidadInsumo(valor != null ? valor[3].toString() : "KGS");
                                bdet.setCantPedido(bd.getCantidad());
                                bdet.setPrecio(valor != null ? Double.parseDouble(valor[6].toString()) : 0);
                                bdet.setIgv(valor != null ? Integer.parseInt(valor[8].toString()) : 1);
                                listaSQL.add(bdet);
                                //System.err.println(bdet.getCodigoInsumo()+"\t"+bdet.getCantPedido()+"\t"+bdet.getUnidadmedida()+"\t"+bd.getCantidad());
                                indice++;
                            }
                            request.getSession().setAttribute("listaDetalle", listaSQL);
                        } else {
                            destino = "orden?action=index";
                        }
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "detList":
                        destino = "ordenproduccionlistdetalle.jsp";
                        ArrayList<beanordencompra> listaDet = (ArrayList<beanordencompra>) request.getSession().getAttribute("listaDetalle");
                        request.getSession().setAttribute("listDet", listaDet);
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "saveOrden":
                        ArrayList<beandetalleordencompra> listaSave = (ArrayList<beandetalleordencompra>) request.getSession().getAttribute("listaDetalle");
                        if (listaSave != null && !listaSave.isEmpty()) {
                            ArrayList<beanpersona> listaSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                            List listaIds = ListaIds(listaSave);
                            for (int i = 0; i < listaIds.size(); i++) {
                                int idprov = Integer.parseInt(listaIds.get(i).toString());
                                bor = new beanordencompra();
                                bor.setIdorden(0);
                                bor.setIdproveedor(idprov);
                                bor.setIdsede(listaSesion.get(0).getIdsede());
                                bor.setCantRows(ord.Cantordenes(bor.getIdsede(), 1));
                                bor.setNroorden(bor.NumeroCorrelativo());
                                bor.setIdusuario(listaSesion.get(0).getIdusuario());
                                bor.setIdarea(10);//el area en la BD
                                bor.setIddestino(listaSesion.get(0).getIdsede());
                                bor.setIdtipo(1);//El tipo de Orden---Orden Pedido Nro 1
                                bor.setIncluyeIGV(1);
                                bor.setIdmoneda(1);//Idmoneda 1 para soles
                                idOrden = ord.RegistraOrdenCompra(bor);
                                for (int j = 0; j < listaSave.size(); j++) {
                                    int idprovsave = listaSave.get(j).getIdProveedor();
                                    if (idprov == idprovsave) {
                                        bdet = new beandetalleordencompra();
                                        bdet.setIddetalle(0);
                                        bdet.setCantidad(listaSave.get(j).getCantPedido());
                                        bdet.setIdorden(idOrden);
                                        bdet.setIdInsumo(listaSave.get(j).getIdInsumo());
                                        bdet.setPrecio(listaSave.get(j).getPrecio());
                                        bdet.setIgv(listaSave.get(j).getIgv());
                                        bdet.setIdprovinsumo(listaSave.get(j).getIdprovinsumo());
                                        //System.err.println(listaSave.get(j).getCodigoInsumo()+"\t"+listaSave.get(j).getIgv()+" el Igv Aqui");
                                        int id = ord.RegistraDetalleOrdenCompra(bdet);
                                        //System.err.println(listaSave.get(j).getCodigoInsumo()+"\t"+bdet.getIgv());
                                    }
                                }
                            }
                            listaSave.clear();
                            out.print("Ordenes Registradas");
                        } else {
                            out.print("Lista Vacia");
                        }
                        break;
                    case "listOrdenH":
                        destino = "listaorden.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listaOrdenHDetalle":
                        destino = "listaordendetalle.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "editDetalle":
                        double precio = request.getParameter("precio").equals("") ? Double.parseDouble("0") : Double.parseDouble(request.getParameter("precio"));
                        int iddet = request.getParameter("iddetalle").equals("") ? Integer.parseInt("0") : Integer.parseInt(request.getParameter("iddetalle"));
                        int idprovinsumo = request.getParameter("idprovinsumo").equals("") ? Integer.parseInt("0") : Integer.parseInt(request.getParameter("idprovinsumo"));
                        if (iddet != 0 && precio != 0 && idprovinsumo != 0) {
                            if (ord.ActualizaDetalle(iddet, precio) > 0) {
                                if (ord.ActualizaCotiz(idprovinsumo, precio) > 0) {
                                    salida = 1;
                                } else {
                                    salida = 0;
                                }
                            } else {
                                salida = 0;
                            }
                        } else {
                            salida = "Rellene campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    case "loadInsumo":
                        int id = Integer.parseInt(request.getParameter("idOrden"));
                        out.print(ord.ListaInsumoFiltro(id));
                        break;
                    case "addItemDetalle":
                        int idord = Integer.parseInt(request.getParameter("idorden").equals("") ? "0" : request.getParameter("idorden"));
                        double cant = Double.parseDouble(request.getParameter("cantidad").equals("") ? "0" : request.getParameter("cantidad"));
                        int idinsumo = Integer.parseInt(request.getParameter("idinsumo").equals("") ? "0" : request.getParameter("idinsumo"));
                        int idpin = Integer.parseInt(request.getParameter("idprovinsumo").equals("") ? "0" : request.getParameter("idprovinsumo"));
                        int igv = Integer.parseInt(request.getParameter("igv").equals("") ? "0" : request.getParameter("igv"));
                        double precioP = Double.parseDouble(request.getParameter("precio").equals("") ? "0" : request.getParameter("precio"));
                        if (idpin != 0 && idord != 0 && (cant != 0 || cant > 0) && idinsumo != 0 && precioP != 0 && igv != 0) {
                            int iddetallesito = ord.AddDetalleCompra(cant, idinsumo, idord, precioP, 0, igv, idpin);
                            salida = iddetallesito > 0 ? 1 : "No Registrado";
                        } else {
                            salida = "Rellene Campos OBligatorios";
                        }
                        out.print(salida);
                        break;
                    default:
                        destino = "index?action=index";
                        response.sendRedirect(destino);
                        break;
                }
            } else {
                destino = "index?action=index";
                response.sendRedirect(destino);
            }

        }

    }

    public List ListaIds(ArrayList<beandetalleordencompra> list) {
        List listaLimpia = new ArrayList();
        Map<Integer, String> mapaproveedor = new HashMap<Integer, String>(list.size());
        for (beandetalleordencompra b : list) {
            mapaproveedor.put(b.getIdProveedor(), b.getRazonSocial());
        }
        for (Map.Entry<Integer, String> co : mapaproveedor.entrySet()) {
            listaLimpia.add(co.getKey());
        }
        return listaLimpia;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
