
package servlet;

import bean.beanalarma;
import bean.beannotificacion;
import bean.beanpersona;
import bean.beanrequerimiento;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import funciones.operaciones;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import negocio.ordenpedido_neg;
import negocio.requerimiento_neg;
import org.apache.commons.fileupload.FileUploadException;

public class requerimientocontroller extends HttpServlet {

    private HttpSession sesionData;
    Object salida = null;
    beanrequerimiento br = null;
    beanalarma balar;
    String destino = "";
    operaciones op = null;
    requerimiento_neg re = null;
    ArrayList<beanrequerimiento> lista;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action != null) {
                String RUTAIMAGENES = getServletContext().getRealPath("/") + "/../../web/assets/imgs";
                Object salida = "";
                switch (action) {
                    case "index":
                        destino = "requerimiento.jsp";
                        lista = new ArrayList<>();
                        request.getSession().setAttribute("dataItem", lista);
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "genReq":
                        destino = "reqgenera.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "list":
                        destino = "listreq.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listDet":
                        destino = "listdetreq.jsp";
                        String idOrden = request.getParameter("id");
                        if (idOrden != null) {
                            re = new requerimiento_neg();
                            ArrayList<beannotificacion> listaDet = re.listaDet(Integer.parseInt(idOrden));
                            request.getSession().setAttribute("listatotales", listaDet);
                        }
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "getHist":
                        //re = new requerimiento_neg();
                        ArrayList<beanpersona> lstSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                        ordenpedido_neg ord = new ordenpedido_neg();
                        out.print(ord.ListaOrden(lstSesion.get(0).getIdtipo(), lstSesion.get(0).getIdsede(), 1));
                        break;
                    case "getDetHist":
                        ArrayList<beannotificacion> lstTot = (ArrayList<beannotificacion>) request.getSession().getAttribute("listatotales");
                        if (lstTot != null) {
                            op = new operaciones();
                            salida = op.getJsonDetalleReqDef(lstTot);
                        }
                        out.print(salida);
                        break;
                    case "getTotales":
                        ArrayList<beannotificacion> l = (ArrayList<beannotificacion>) request.getSession().getAttribute("listatotales");
                        if (l != null) {
                            double sub = 0, igv = 0, neto = 0;
                            for (beannotificacion bn : l) {
                                sub = sub + bn.getSubtotal();
                                igv = igv + bn.getIgv();
                                neto = neto + bn.getNeto();
                            }
                            JsonObject obj = new JsonObject();
                            obj.addProperty("correlativo", l.get(0).getCorrelativo());
                            obj.addProperty("nombreprov", l.get(0).getNomprov());
                            obj.addProperty("ruc", l.get(0).getRuc());
                            obj.addProperty("fecha", l.get(0).getFecha());
                            obj.addProperty("subtotal", sub);
                            obj.addProperty("igv", igv);
                            obj.addProperty("neto", neto);
                            JsonArray arr = new JsonArray();
                            arr.add(obj);
                            obj = new JsonObject();
                            obj.add("data", arr);
                            out.print(obj);
                        }
                        break;
                    case "viewFiltro":
                        String area = request.getParameter("idarea");
                        String idsede = request.getParameter("idsede");
                        String estado = request.getParameter("rbestado");
                        if (!area.equals("") && !idsede.equals("")) {
                            re = new requerimiento_neg();
                            request.getSession().setAttribute("param", new Object[]{area, idsede, estado});
                            salida = re.listaJsonfiltro(Integer.parseInt(area), Integer.parseInt(idsede), estado == null ? -1 : Integer.parseInt(estado));
                        } else {
                            salida = "no se puede filtrar";
                        }
                        out.print(salida);
                        break;
                    case "regOrden":
                        Object[] d = (Object[]) request.getSession().getAttribute("param");
                        if (d != null) {
                            int sede = 0, arear = 0, estadop = 0;
                            arear = Integer.parseInt(d[0].toString());
                            sede = Integer.parseInt(d[1].toString());
                            estadop = Integer.parseInt(d[2].toString());
                            requerimiento_neg r = new requerimiento_neg();
                            if (sede != 0 && estadop == 1) {
                                ArrayList<beannotificacion> listaData = r.listaCompra(arear, sede, estadop);
                                if (listaData != null) {
                                    destino = "reqCompra?action=index";
                                    HttpSession newSesion = request.getSession(true);
                                    newSesion.setAttribute("dataCompra", listaData);
                                }
                            } else {
                                destino = "index?action=reqGen";
                            }
                        } else {
                            destino = "index?action=reqGen";
                        }
                        response.sendRedirect(destino);
                        break;
                    case "addItem":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beanrequerimiento>) request.getSession().getAttribute("dataItem");
                        if (!lista.isEmpty() || lista != null) {
                            String cant = request.getParameter("cantidad");
                            String descripcion = request.getParameter("descripcion");
                            if ((!cant.equals("") || !cant.equals("0")) && !descripcion.equals("")) {
                                br = new beanrequerimiento();
                                br.setCant(Integer.parseInt(cant));
                                br.setDescripcion(descripcion);
                                br.setIdalarma(0);//se pone 0 por que el estado es pendiente
                                lista.add(br);
                                op = new operaciones();
                                salida = op.getJsonDetalleRequerimiento(lista);
                            } else {
                                salida = "Rellene campos Obligatorios";
                            }
                        } else {
                            salida = "No se puede Añadir";
                        }
                        out.print(salida);
                        break;
                    case "deleteItem":
                        String indice = request.getParameter("indice");
                        op = new operaciones();
                        if (op.ValidaNumero(indice)) {
                            lista = new ArrayList<>();
                            lista = (ArrayList<beanrequerimiento>) request.getSession().getAttribute("dataItem");
                            lista.remove(Integer.parseInt(indice));
                            op = new operaciones();
                            salida = op.getJsonDetalleRequerimiento(lista);
                        } else {
                            salida = "No se puede eliminar item";
                        }
                        out.print(salida);
                        break;
                    case "addTest":
                        br = new beanrequerimiento();
                        br.setRequest(request);
                        br.setResponse(response);
                        br.Init();
                        if (br.getCant() > 0 && br.getDescripcion() != null) {
                            lista = new ArrayList<>();
                            lista = (ArrayList<beanrequerimiento>) request.getSession().getAttribute("dataItem");
                            lista.add(br);
                            op = new operaciones();
                            salida = op.getJsonDetalleRequerimiento(lista);
                        } else {
                            salida = "No se puede Añadir item";
                        }
                        out.print(salida);
                        break;
                    case "registraTest":
                        lista = new ArrayList<>();
                        lista = (ArrayList<beanrequerimiento>) request.getSession().getAttribute("dataItem");
                        if (lista != null) {
                            if (!lista.isEmpty()) {
                                int idrequerimiento = 0;
                                re = new requerimiento_neg();
                                br = new beanrequerimiento();
                                br.setIdusuario(lista.get(0).getIdusuario());
                                br.setIdsede(lista.get(0).getIdsede());
                                br.setIdarea(lista.get(0).getIdarea());
                                br.setCantRow(re.CantRows(br.getIdsede()));
                                br.setIdtipo(lista.get(0).getIdtipo() == 1 ? 2 : 1);// id 2-> Orden de Servicio -_- id 1 -> requerimiento
                                idrequerimiento = re.RegistraRequerimiento(br);
                                if (idrequerimiento > 0) {
                                    int contador = 0;
                                    for (beanrequerimiento breq : lista) {
                                        //Primero REgistrar Alarmas
                                        balar = new beanalarma();
                                        balar.setEstado(0);
                                        balar.setDescripcion("");
                                        balar.setFechacambio("");
                                        int idalarma = re.RegistraAlarma(balar);
                                        //
                                        int iddetalle = 0;
                                        if (idalarma > 0) {
                                            br = new beanrequerimiento();
                                            br.setIdrequerimiento(idrequerimiento);
                                            br.setCant(breq.getCant());
                                            br.setDescripcion(breq.getDescripcion());
                                            br.setIdalarma(idalarma);
                                            br.setImagen(breq.getArchivo() != null ? breq.newFileName(idrequerimiento) : null);
                                            iddetalle = re.RegistrarDetalleRequrimiento(br);
                                            if (iddetalle > 0) {
                                                File archivo = breq.getArchivo();
                                                if (archivo != null) {
                                                    File archivo2 = new File(RUTAIMAGENES + "\\" + br.getImagen());
                                                    archivo.renameTo(archivo2);
                                                }
                                                contador++;
                                            }
                                        } else {
                                            salida = "No Registrado (Alarma no Registrada)";
                                        }
                                    }
                                    salida = contador == lista.size() ? "1" : "Requerimientos No Registrados";
                                } else {
                                    salida = "Requerimiento no Registrado";
                                }
                            } else {
                                salida = "No Añadio ningun requerimiento";
                            }
                        } else {
                            salida = "Error Interno";
                        }
                        out.print(salida);
                        break;
                    case "registra":
                        String idresponsable = request.getParameter("idresponsable").equals("") || request.getParameter("idresponsable").equals("0") ? "0" : request.getParameter("idresponsable");
                        String idsala = request.getParameter("idsede").equals("") || request.getParameter("idsede").equals("0") ? "0" : request.getParameter("idsede");
                        String idarea = request.getParameter("idarea").equals("") || request.getParameter("idarea").equals("0") ? "0" : request.getParameter("idarea");
                        String idtipo = request.getParameter("tipo");
                        if (!idresponsable.equals("0") && !idarea.equals("0") && !idsala.equals("0")) {
                            lista = new ArrayList<>();
                            lista = (ArrayList<beanrequerimiento>) request.getSession().getAttribute("dataItem");
                            if (!lista.isEmpty() && lista != null) {
                                //primero registra la tabla REquerimiento
                                int idrequerimiento = 0;
                                requerimiento_neg req = new requerimiento_neg();
                                br = new beanrequerimiento();
                                br.setIdusuario(Integer.parseInt(idresponsable));
                                br.setIdsede(Integer.parseInt(idsala));
                                br.setIdarea(Integer.parseInt(idarea));
                                br.setCantRow(req.CantRows(br.getIdsede()));
                                br.setIdtipo(idtipo != null ? 3 : 2);// id 3-> Orden de Servicio -_- id 2 -> requerimiento
                                idrequerimiento = req.RegistraRequerimiento(br);
                                //
                                int iddetalle = 0;
                                int contador = 0;
                                beanalarma balar = null;
                                for (beanrequerimiento bre : lista) {
                                    //Primero REgistrar Alarmas
                                    balar = new beanalarma();
                                    balar.setEstado(0);
                                    balar.setDescripcion("");
                                    balar.setFechacambio("");
                                    int idalarma = req.RegistraAlarma(balar);
                                    //
                                    if (idalarma > 0) {
                                        br = new beanrequerimiento();
                                        br.setIdrequerimiento(idrequerimiento);
                                        br.setCant(bre.getCant());
                                        br.setDescripcion(bre.getDescripcion());
                                        br.setIdalarma(idalarma);
                                        iddetalle = req.RegistrarDetalleRequrimiento(br);
                                        if (iddetalle > 0) {
                                            System.err.println(bre.getIdrequerimiento() + "\t" + bre.getCant() + "\t" + bre.getDescripcion() + "\t" + bre.getIdAlarma() + "Registrado");
                                            contador++;
                                        }
                                    } else {
                                        salida = "No Registrado por error Interno";
                                    }
                                }
                                salida = contador == lista.size() ? "1" : "0";
                            } else {
                                salida = "No se Puede Registrar Detalle";
                            }
                        } else {
                            salida = "Rellene campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    case "viewAlarma":
                        requerimiento_neg rn = new requerimiento_neg();
                        out.print(rn.listAlarmas());
                        break;
                }
            } else {
                response.sendRedirect("index?action=index");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FileUploadException ex) {
            Logger.getLogger(requerimientocontroller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FileUploadException ex) {
            Logger.getLogger(requerimientocontroller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
