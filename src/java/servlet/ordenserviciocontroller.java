package servlet;

import bean.beandetalleordencompra;
import bean.beannotificacion;
import bean.beanordencompra;
import bean.beanpersona;
import bean.beanresultado;
import funciones.operaciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.ordenpedido_neg;
import negocio.requerimiento_neg;

public class ordenserviciocontroller extends HttpServlet {

    ArrayList<beanpersona> listSesion;
    ArrayList<beanresultado> listaDetalle;
    beanresultado bres;
    beanordencompra bord;
    operaciones op;
    ordenpedido_neg ord;
    beandetalleordencompra bdet;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action != null) {
                String destino = "";
                ordenpedido_neg ord = null;
                requerimiento_neg re = null;
                Object salida = null;
                switch (action) {
                    case "index":
                        destino = "listserv.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "newServ":
                        destino = "ordservgen.jsp";
                        listaDetalle = new ArrayList<>();
                        request.getSession().setAttribute("dataDetalle", listaDetalle);
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listDet":
                        destino = "listservdet.jsp";
                        String idOrden = request.getParameter("id");
                        if (idOrden != null) {
                            re = new requerimiento_neg();
                            ArrayList<beannotificacion> listaDet = re.listaDet(Integer.parseInt(idOrden));
                            request.getSession().setAttribute("listatotales", listaDet);
                        }
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listOrden":
                        listSesion = new ArrayList<>();
                        listSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                        if (listSesion != null) {
                            ord = new ordenpedido_neg();
                            salida = ord.ListaOrden(listSesion.get(0).getIdtipo(), listSesion.get(0).getIdsede(), 2);
                        } else {
                            salida = "{\"data\":[]}";
                        }
                        out.print(salida);
                        break;
                    case "addItem":
                        String cantidad = request.getParameter("cantidad");
                        String descripcion = request.getParameter("descripcion");
                        String precio = request.getParameter("precio");
                        if (!cantidad.equals("") && !descripcion.equals("") && !precio.equals("")) {
                            bres = new beanresultado();
                            bres.setCantidad(Double.parseDouble(cantidad));
                            bres.setPrecio(Double.parseDouble(precio));
                            bres.setDescripcion(descripcion);
                            listaDetalle = new ArrayList<>();
                            listaDetalle = (ArrayList<beanresultado>) request.getSession().getAttribute("dataDetalle");
                            if (listaDetalle != null) {
                                listaDetalle.add(bres);
                                op = new operaciones();
                                salida = op.getJsonDetalleOrden(listaDetalle);
                            } else {
                                salida = "Error";
                            }
                        } else {
                            salida = "Ingrese Campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    case "deleteItem":
                        String indice = request.getParameter("indice");
                        if (indice != null) {
                            listaDetalle = new ArrayList<>();
                            listaDetalle = (ArrayList<beanresultado>) request.getSession().getAttribute("dataDetalle");
                            if (listaDetalle != null) {
                                listaDetalle.remove(Integer.parseInt(indice));
                                op = new operaciones();
                                salida = op.getJsonDetalleOrden(listaDetalle);
                            } else {
                                salida = "Error";
                            }
                        } else {
                            salida = "Error";
                        }
                        out.print(salida);
                        break;
                    case "regServicio":
                        listaDetalle = new ArrayList<>();
                        listaDetalle = (ArrayList<beanresultado>) request.getSession().getAttribute("dataDetalle");
                        if (listaDetalle != null) {
                            listSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                            if (listSesion != null) {
                                String idproveedor = request.getParameter("idproveedor");
                                String idsede=request.getParameter("idsede");
                                if (!idproveedor.equals("0")) {
                                    ord = new ordenpedido_neg();
                                    bord = new beanordencompra();
                                    bord.setIdsede(Integer.parseInt(idsede));
                                    bord.setIdproveedor(Integer.parseInt(idproveedor));
                                    bord.setCantRows(ord.Cantordenes(Integer.parseInt(idsede), 2));//2 es el Tipo de Orden =Servicio
                                    bord.setNroorden(bord.NumeroCorrelativo());
                                    bord.setIdusuario(listSesion.get(0).getIdusuario());
                                    bord.setIdarea(listSesion.get(0).getIdarea());
                                    bord.setIddestino(bord.getIdsede());
                                    bord.setIdtipo(2);//Tipo Orden de servicio
                                    bord.setIncluyeIGV(1);
                                    bord.setIdmoneda(1);//Para Tipo moneda Soles
                                    int iddetalle = 0, contador = 0;
                                    int idord = ord.RegistraOrdenCompra(bord);
//                                    Object[] r=bord.getRegistro();
//                                    for (Object b:r){
//                                        System.err.println(b);
//                                    }
                                    if (idord > 0) {
                                        if (listaDetalle.size() > 0) {
                                            for (beanresultado br : listaDetalle) {
                                                bdet = new beandetalleordencompra();
                                                bdet.setCantidad(br.getCantidad());
                                                bdet.setIdorden(idord);
                                                bdet.setPrecio(br.getPrecio());
                                                bdet.setIgv(1);
                                                bdet.setDescripcionaux(br.getDescripcion());
                                                //System.err.println(bdet.getCantidad()+" "+bdet.getIdorden()+" "+bdet.getPrecio()+" "+bdet.getDescripcionaux());
                                                iddetalle = ord.RegistraDetalleOrdenCompraReq(bdet);
                                                if (iddetalle > 0) {
                                                    contador++;
                                                }
                                            }
                                            salida = contador == listaDetalle.size() ? "1" : "Error al Registrar";
                                        } else {
                                            salida = "La lista esta VAcia";
                                        }
                                    } else {
                                        salida = "Orden no registrada";
                                    }
                                } else {
                                    salida = "Rellene Campos Obligatorios";
                                }
                            } else {
                                salida = "La Sesion no es valida";
                            }
                        }
                        out.print(salida);
                        break;
                }
            } else {
                response.sendRedirect("index?action=index");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
