package servlet;

import bean.beaninsumo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.insumo_neg;

public class insumocontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            String destino = "";
            insumo_neg ins;
            Object salida = null;
            if (action != null) {
                switch (action) {
                    case "index":
                        destino = "insumo.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "detalle":
                        destino = "insumodetalle.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "list":
                        ins=new insumo_neg();
                        out.print(ins.getInsumo(0));
                        break;
                    case "listInsumoId":
                        ins=new insumo_neg();
                        out.print(ins.getInsumo(Integer.parseInt(request.getParameter("idInsumo"))));
                        break;
                    case "registraInsumo":
                        String idinsumo = request.getParameter("idinsumo").equals("") ? "0" : request.getParameter("idinsumo");
                        String codigoinsumo = request.getParameter("codigoinsumo");
                        String descripcioninsumo = request.getParameter("descripcioninsumo");
                        String unidadmedida = request.getParameter("unidadmedida");
                        String idunidad = request.getParameter("idunidad");
                        String idcategoria = request.getParameter("idcategoria");
                        String idclas=request.getParameter("idclasificacion");
                        System.err.println(idclas);
                        if (!codigoinsumo.equals("") && !descripcioninsumo.equals("") && !unidadmedida.equals("") && !idunidad.equals("0") && !idcategoria.equals("0") && !idclas.equals("0")) {
                            if (codigoinsumo.length() == 9) {
                                beaninsumo bin = new beaninsumo();
                                bin.setIdinsumo(Integer.parseInt(idinsumo));
                                bin.setCodigoinsumo(codigoinsumo);
                                bin.setDescripcioninsumo(descripcioninsumo);
                                bin.setUnidadmedida(Double.parseDouble(unidadmedida));
                                bin.setIdunidad(Integer.parseInt(idunidad));
                                bin.setIdcategoria(Integer.parseInt(idcategoria));
                                bin.setIdclasificacion(Integer.parseInt(idclas));
                                int id = 0;
                                if (bin.getIdinsumo() == 0) {
                                    ins=new insumo_neg();
                                    if (!ins.verificaExistencia(bin.getCodigoinsumo())) {
                                        id = ins.GuardaInsumo(bin);
                                    } else {
                                        id = -1;
                                    }
                                } else {
                                    ins=new insumo_neg();
                                    id = ins.GuardaInsumo(bin);
                                }
                                salida = id > 0 ? id == 1 ? "Registro Actualizado !!" : id : id == -1 ? "Codigo " + bin.getCodigoinsumo() + " existe en la BD !" : "No Registrado";
                            } else {
                                salida = "El codigo debe de tener 9 digitos";
                            }
                        } else {
                            salida = "Rellene campos Obligatorios!!!";
                        }
                        out.print(salida);
                        break;
                    default:
                        destino = "index?action=index";
                        response.sendRedirect(destino);
                        break;
                }
            } else {
                destino = "index?action=index";
                response.sendRedirect(destino);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
