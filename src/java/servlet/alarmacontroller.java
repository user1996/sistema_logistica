package servlet;

import bean.beanalerta;
import bean.beandetalleordencompra;
import bean.beanordencompra;
import bean.beanpersona;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.alarma_neg;
import negocio.ordenpedido_neg;

public class alarmacontroller extends HttpServlet {

    alarma_neg al;
    beanalerta bal;
    beanordencompra bord;
    beandetalleordencompra bdet;
    ordenpedido_neg ord_n;

    private int RegistrarAlertaNext2(int idalerta, String fechavencimiento, int estado, int ndias, alarma_neg al) {
        bal = new beanalerta();
        bal.setIdalerta(idalerta);
        bal.setFechavencimiento(fechavencimiento);
        bal.setEstado(estado);
        bal.setNdias(ndias);
        return al.GuardaDetalleAlerta(bal);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action != null) {
                String destino = "";
                Object salida = null;
                switch (action) {
                    case "index":
                        destino = "listalarma.jsp";
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "detAlarma":
                        destino = "listalarmadet.jsp";
                        String iddd = request.getParameter("idalerta");
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "getJsonList":
                        al = new alarma_neg();
                        out.print(al.listAlertas());
                        break;
                    case "regAlarma":
                        String suministro = request.getParameter("codsuministro");
                        String idproveedor = request.getParameter("idproveedor");
                        String idsede = request.getParameter("idsede");
                        String idtipo = request.getParameter("idtipo");
                        String fechavenc = request.getParameter("fechavencimiento");
                        String diasantes = request.getParameter("nrodias");
                        if (!suministro.equals("") && !idproveedor.equals("0") && !idsede.equals("0") && !idtipo.equals("0") && !fechavenc.equals("") && !diasantes.equals("0")) {
                            bal = new beanalerta();
                            bal.setNrosuministro(suministro);
                            bal.setIdtipo(Integer.parseInt(idtipo));
                            bal.setIdprov(Integer.parseInt(idproveedor));
                            bal.setIdsede(Integer.parseInt(idsede));
                            int idalerta = 0;
                            al = new alarma_neg();
                            idalerta = al.GuardaAlerta(bal);
                            if (idalerta > 0) {
                                int iddetalle = 0;
                                iddetalle = RegistrarAlertaNext2(idalerta, fechavenc, 0, Integer.parseInt(diasantes), al);  //al.GuardaDetalleAlerta(bal);
                                if (iddetalle > 0) {
                                    //salida=al.ActualizaEstado(iddetalle)>0?"1":"No Actualizado";
                                    salida = "1";
                                } else {
                                    salida = "Detalle de alerta no REgistrado";
                                }
                            } else {
                                salida = "No Registrado";
                            }
                        } else {
                            salida = "Rellene campos Obligatorios";
                        }
                        out.print(salida);
                        break;
                    case "updateAl":
                        String iddetalle = request.getParameter("iddetalle");
                        String idmoneda = request.getParameter("idmoneda");
                        String precio = request.getParameter("precio");
                        String val = request.getParameter("val");
                        System.err.println(precio);
                        ArrayList<beanpersona> listaSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                        if (listaSesion != null) {
                            if (iddetalle != null) {
                                al = new alarma_neg();
                                Object[] array = al.nexDetalle(Integer.parseInt(iddetalle));
                                if (array != null) {
                                    //Obtiene datos de la siguiente alerta a registrar
                                    int idalerta = Integer.parseInt(array[0].toString());
                                    String fvenc = array[1].toString();
                                    int ndias = Integer.parseInt(array[2].toString());
                                    int estado = Integer.parseInt(array[3].toString());
                                    int idprov = Integer.parseInt(array[4].toString());
                                    int idsedee = Integer.parseInt(array[5].toString());
                                    //////////
                                    if (val.equals("1")) {
                                        if (!precio.equals("0") && !precio.equals("")) {
                                            //Ahora se registrara la Orden de compra:
                                            ord_n = new ordenpedido_neg();
                                            bord = new beanordencompra();
                                            bord.setIdproveedor(idprov);
                                            bord.setIdsede(idsedee);
                                            bord.setCantRows(ord_n.Cantordenes(idsedee, 2));
                                            bord.setNroorden(bord.NumeroCorrelativo());
                                            bord.setIdusuario(listaSesion.get(0).getIdusuario());
                                            bord.setIddestino(idsedee);
                                            bord.setIdarea(4);//4 es para oficina
                                            bord.setIdtipo(2);//tipo de ordende compra
                                            bord.setIncluyeIGV(1);
                                            bord.setIdmoneda(Integer.parseInt(idmoneda));
                                            int idorden = ord_n.RegistraOrdenCompra(bord);
                                            if (idorden > 0) {//Ahora registrar el detalle de la orden de compra
                                                bdet = new beandetalleordencompra();
                                                bdet.setCantidad(1.0);//por defecto la cantidad es 1;
                                                bdet.setIdorden(idorden);
                                                bdet.setPrecio(Double.parseDouble(precio));
                                                bdet.setIgv(0);
                                                bdet.setDescripcionaux("ESTO ES UNA DRECIPCION DE EJEMPLO");
                                                int iddet = ord_n.RegistraDetalleOrdenCompraReq(bdet);
                                                if (iddet > 0) {//Si se registra el detalle , se actualiza el estado de la alerta
                                                    al = new alarma_neg();
                                                    System.err.println(iddetalle + "  .> " + "Aquiii");
                                                    int id = al.ActualizaEstado(Integer.parseInt(iddetalle));
                                                    if (id > 0) {
                                                        int detalleId = RegistrarAlertaNext2(idalerta, fvenc, estado, ndias, al);
                                                        salida = detalleId > 0 ? "1" : "Alerta no registrada";
                                                    } else {
                                                        salida = "Alerta No Atualizada";
                                                    }
                                                } else {
                                                    salida = "DEtalle no Registrado";
                                                }
                                            } else {
                                                salida = "Orden no registrada";
                                            }
                                        } else {
                                            salida = "Precio no puede ser 0";
                                        }
                                    } else {
                                        //Registra
                                        al = new alarma_neg();
                                        int idupdate = al.ActualizaEstado(Integer.parseInt(iddetalle));
                                        if (idupdate > 0) {
                                            int id = RegistrarAlertaNext2(idalerta, fvenc, estado, ndias, al);
                                            salida = id > 0 ? "1" : "Proxima alerta no registrada";
                                        }else{
                                            salida="Alerta no Actualizada";
                                        }
                                    }
                                } else {
                                    salida = "el array esta vacio o nulo";
                                }
                            } else {
                                salida = "Error al obtener ID detalle";
                            }
                        } else {
                            salida = "Error no se puede Obtener Datos de Usuario";
                        }
                        out.print(salida);
                        break;
                    default:
                        response.sendRedirect("index?action=index");
                        break;
                }
            } else {
                response.sendRedirect("index?action=index");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
