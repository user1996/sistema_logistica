package servlet;

import bean.beanpersona;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.requerimiento_neg;

public class notificacioncontroller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action != null) {
                String destino = "";
                requerimiento_neg req = null;
                Object salida = "";
                switch (action) {
                    case "getNotifM":
                        destino = "listnotif.jsp";
                        ArrayList<beanpersona> listaaa = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
                        if (listaaa != null) {
                            String nombreTipo = listaaa.get(0).getNombretipo();
                            req = new requerimiento_neg();
                            request.getSession().setAttribute("dataNotifList", req.listNotif(nombreTipo.equals("ADMINISTRADOR") ? 0 : listaaa.get(0).getIdusuario()));
                            request.getSession().setAttribute("listaIds", req.listaId(nombreTipo.equals("ADMINISTRADOR") ? 0 : listaaa.get(0).getIdusuario()));
                            request.getRequestDispatcher(destino).forward(request, response);
                        }
                        break;
                    case "getNotif":
                        destino = "detnotif.jsp";
                        String idrequerimiento = request.getParameter("idreq");
                        req = new requerimiento_neg();
                        request.getSession().setAttribute("dataDetalleNotif", req.listDetalleNotif(idrequerimiento));
                        request.getRequestDispatcher(destino).forward(request, response);
                        break;
                    case "listDet":
                        String idreq = request.getParameter("idreq");
                        req = new requerimiento_neg();
                        out.print(req.getjsonDetalleNotif(idreq));
                        break;
                    case "cambiaEstado":
                        String estado = request.getParameter("estado");
                        String idalarma = request.getParameter("idalarma");
                        String cant = request.getParameter("cantidad");
                        String iddetalle = request.getParameter("iddetalle");
                        if (!estado.equals("") && !idalarma.equals("") && !cant.equals("0")) {
                            String motivo = request.getParameter("motivo");
                            req = new requerimiento_neg();
                            if (req.ActualizaCant(Integer.parseInt(iddetalle), Double.parseDouble(cant)) > 0) {
                                int id = req.Actualizaestado(Integer.parseInt(idalarma), Integer.parseInt(estado), motivo.equals("") ? null : motivo.toUpperCase());
                                if (id > 0) {
                                    salida = "Registro Actualizado";
                                } else {
                                    salida = "REgistro no actualizado";
                                }
                            } else {
                                salida = "No Actualizado";
                            }
                        } else {
                            salida = "Error al Obtener Datos";
                        }
                        out.print(salida);
                        break;
                }
            } else {
                response.sendRedirect("index?action=index");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
