package pdf;

import BD.bdconexion;
import funciones.conversordinero;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import negocio.ordenpedido_neg;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class daopdf {

    private final String rutaIMG1 = "/pdf/logosun.JPG";
    private final String rutaIMG2 = "/pdf/sello.png";
    private final String rutaIMGFONDO = "/reportes/Logo-Fondo.png";

    public void ExportaPDF(String idOrden, HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        bdconexion con = new bdconexion("SIS_LOGISTICA");
        ordenpedido_neg ord = new ordenpedido_neg();
        conversordinero conv = new conversordinero();
        try {
            URL in = this.getClass().getResource("/pdf/ordencompra.jasper");
            JasperReport reporte = (JasperReport) net.sf.jasperreports.engine.util.JRLoader.loadObject(in);
            Map parametros = new HashMap();
            parametros.put("idOrden", idOrden);
            parametros.put("img1", this.getClass().getResourceAsStream(rutaIMG1));
            parametros.put("imgsello", this.getClass().getResourceAsStream(rutaIMG2));
            parametros.put("montodinero", conv.Convertir(ord.getTotalPagar(idOrden), true, "NUEVOS SOLES"));
            //  parametros.put("imgfondo", this.getClass().getResourceAsStream(rutaIMGFONDO));
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, con.getConnection());
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            exporter.exportReport();
        } catch (JRException e) {
            System.out.print(e.getMessage()+" ");
        }
    }

    public void ExportaPdfReq(String idOrden, HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        bdconexion con = new bdconexion("SIS_LOGISTICA");
        ordenpedido_neg ord = new ordenpedido_neg();
        conversordinero conv = new conversordinero();
        try {
            Object[] datosPAgo = ord.getTotalPagarReq(idOrden);
            URL in = this.getClass().getResource("/pdf/ordencomprareq.jasper");
            JasperReport reporte = (JasperReport) net.sf.jasperreports.engine.util.JRLoader.loadObject(in);
            Map parametros = new HashMap();
            parametros.put("idOrden", idOrden);
            parametros.put("img1", this.getClass().getResourceAsStream(rutaIMG1));
            parametros.put("imgsello", this.getClass().getResourceAsStream(rutaIMG2));
            parametros.put("montodinero", conv.Convertir(datosPAgo != null ? datosPAgo[0].toString() : "", true, datosPAgo!=null?datosPAgo[1].toString():""));
            //parametros.put("imgfondo", this.getClass().getResourceAsStream(rutaIMGFONDO));
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, con.getConnection());
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            exporter.exportReport();
        } catch (JRException e) {
            System.out.print(e.getMessage());
        }
    }
}
