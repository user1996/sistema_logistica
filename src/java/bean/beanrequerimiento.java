package bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class beanrequerimiento {

    //
    //Para el Servlet
    private HttpServletRequest request;
    private HttpServletResponse response;
    private File archivo;

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    //
    //
    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    //
    public void Init() throws FileUploadException, IOException {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        Object[] fieldsForm = new Object[items.size()];
        int indice = 0;
        for (FileItem item : items) {
            if (item.isFormField()) {
                fieldsForm[indice] = item.getString().equals("") ? null : item.getString();
            } else {
                File file = null;
                if (!item.getString().equals("")) {
                    file = getArchivo(item.getInputStream(), getNameFile(item.getName()), getExtension(item.getName()));
                }
                fieldsForm[indice] = file;
            }
            indice++;
        }
        setData(fieldsForm);
    }

    public File getArchivo(InputStream is, String filename, String extension) throws IOException {
        if (is != null) {
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            File tempFile = File.createTempFile(filename, extension);
            FileOutputStream out = new FileOutputStream(tempFile);
            out.write(buffer);
            out.flush();
            if (out != null) {
                out.close();
            }
            return tempFile;
        } else {
            return null;
        }
    }

    private String getExtension(String filename) {
        return filename.substring(filename.indexOf("."), filename.length());
    }

    private String getNameFile(String filename) {
        return filename.replace(getExtension(filename), "");
    }

    public String FileGetName() {
        return getNameFile(archivo.getName());
    }

    public String FileGetExtension() {
        return getExtension(archivo.getName());
    }

    public String newFileName(int id) {
        return "reqimg" + (int)getCant() + id + FileGetExtension();
    }

    //
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    private int indice, iddetalle, idrequerimiento, idusuario, idsede, idarea, idalarma, idtipo, cantRow;
    private String fechahora, descripcion, nrorequerimiento,imagen;
    private double cant;
    DecimalFormat format = new DecimalFormat("00000");

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public int getIdrequerimiento() {
        return idrequerimiento;
    }

    public void setIdrequerimiento(int idrequerimiento) {
        this.idrequerimiento = idrequerimiento;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public int getIdarea() {
        return idarea;
    }

    public void setIdarea(int idarea) {
        this.idarea = idarea;
    }

    public String getFechahora() {
        return fechahora;
    }

    public void setFechahora(String fechahora) {
        this.fechahora = fechahora;
    }

    public Object[] getDatoRequerimiento() {
        return new Object[]{idrequerimiento, idusuario, idsede, idarea, correlativo(), idtipo};
    }

    public int getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(int iddetalle) {
        this.iddetalle = iddetalle;
    }

    public double getCant() {
        return cant;
    }

    public void setCant(double cant) {
        this.cant = cant;
    }

    public int getIdAlarma() {
        return idalarma;
    }

    public void setIdalarma(int idalarma) {
        this.idalarma = idalarma;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion.equals("") ? null : descripcion.toUpperCase().trim();
    }

//para tipo de estado para 0-> Pendiente 1 -> aprobado 2->rechazado
    private void setData(Object[] aData) {
        this.idsede = Integer.parseInt(aData[0].toString());
        this.idusuario = Integer.parseInt(aData[1].toString());
        this.idarea = Integer.parseInt(aData[2].toString());
        this.cant = Double.parseDouble(aData[3] != null ? aData[3].toString() : "0");
        this.idtipo = Integer.parseInt(aData[4].toString());
        this.descripcion = aData[5] != null ? aData[5].toString() : "";
        this.archivo = (File) aData[6];
        this.idalarma = 0;
        setIdsede(idsede);
        setIdusuario(idusuario);
        setIdarea(idarea);
        setCant(cant);
        setIdtipo(idtipo);
        setDescripcion(descripcion);
        setArchivo(archivo);
        setIdalarma(idalarma);
    }

    public Object[] getDatoDetalleRequrimiento() {
        return new Object[]{iddetalle, idrequerimiento, cant, descripcion, idalarma,imagen};
    }

    public String correlativo() {
        return NumeroSede() + "-" + format.format(cantRow + 1);
    }

    public String NumeroSede() {
        String numero = "";
        switch (idsede) {
            case 1:
                numero = format.format(1);
                break;
            case 2:
                numero = format.format(3);
                break;
            case 3:
                numero = format.format(2);
                break;
            case 4:
                numero = format.format(9);
                break;
            case 5:
                numero = format.format(5);
                break;
            default:
                numero = format.format(0);
                break;

        }
        return numero;
    }

    public int getCantRow() {
        return cantRow;
    }

    public void setCantRow(int cantRow) {
        this.cantRow = cantRow;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
