
package bean;

public class beantipoalerta {
    private int idtipo;
    private String nombretipo;

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public String getNombretipo() {
        return nombretipo;
    }

    public void setNombretipo(String nombretipo) {
        this.nombretipo = nombretipo.equals("")?null:nombretipo.trim().toUpperCase();
    }
    
}
