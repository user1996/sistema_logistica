
package bean;

public class beaninsumo {
   private int idinsumo,idunidad,idcategoria,idclasificacion;
   private String codigoinsumo,descripcioninsumo;
   private double unidadmedida;

    public int getIdclasificacion() {
        return idclasificacion;
    }

    public void setIdclasificacion(int idclasificacion) {
        this.idclasificacion = idclasificacion;
    }
   
    public int getIdinsumo() {
        return idinsumo;
    }

    public void setIdinsumo(int idinsumo) {
        this.idinsumo = idinsumo;
    }

    public int getIdunidad() {
        return idunidad;
    }

    public void setIdunidad(int idunidad) {
        this.idunidad = idunidad;
    }

    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    public String getCodigoinsumo() {
        return codigoinsumo;
    }

    public void setCodigoinsumo(String codigoinsumo) {
        this.codigoinsumo = codigoinsumo;
    }

    public String getDescripcioninsumo() {
        return descripcioninsumo;
    }

    public void setDescripcioninsumo(String descripcioninsumo) {
        this.descripcioninsumo = descripcioninsumo;
    }

    public double getUnidadmedida() {
        return unidadmedida;
    }

    public void setUnidadmedida(double unidadmedida) {
        this.unidadmedida = unidadmedida;
    }
   public Object[] getRegistro(){
       return new Object[]{idinsumo,codigoinsumo,descripcioninsumo,unidadmedida,idunidad,idcategoria,idclasificacion};
   }
}
