package bean;

import java.text.DecimalFormat;

public class beanordencompra {

    DecimalFormat format = new DecimalFormat("00000");
    private int idorden, idproveedor, idusuario, idsede, cantRows, idarea, iddestino,incluyeIGV;
    private int idtipo,idmoneda;//Tipo 1-> ordenCompra ///// Tipo 2-> requerimiento
    private String fecha, nroorden, hora;

    public int getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(int idmoneda) {
        this.idmoneda = idmoneda;
    }

    public int getIncluyeIGV() {
        return incluyeIGV;
    }

    public void setIncluyeIGV(int incluyeIGV) {
        this.incluyeIGV = incluyeIGV;
    }

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public int getIddestino() {
        return iddestino;
    }

    public void setIddestino(int iddestino) {
        this.iddestino = iddestino;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getIdarea() {
        return idarea;
    }

    public void setIdarea(int idarea) {
        this.idarea = idarea;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getNroorden() {
        return nroorden;
    }

    public void setNroorden(String nroorden) {
        this.nroorden = nroorden;
    }

    public int getIdorden() {
        return idorden;
    }

    public void setIdorden(int idorden) {
        this.idorden = idorden;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(int idproveedor) {
        this.idproveedor = idproveedor;
    }

    public Object[] getRegistro() {
        return new Object[]{idorden, idproveedor, nroorden, idusuario, idarea, iddestino,idtipo,incluyeIGV,idmoneda};
    }

    public String NumeroSede() {
        String numero = "";
        switch (idsede) {
            case 1:
                numero = format.format(1);
                break;
            case 2:
                numero = format.format(3);
                break;
            case 3:
                numero = format.format(2);
                break;
            case 4:
                numero = format.format(9);
                break;
            case 5:
                numero = format.format(5);
                break;
            default:
                numero = format.format(0);
                break;

        }
        return numero;
    }

    public String NumeroCorrelativo() {
        return NumeroSede() + "-" + format.format(cantRows + 1);
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public int getCantRows() {
        return cantRows;
    }

    public void setCantRows(int cantRows) {
        this.cantRows = cantRows;
    }
}
