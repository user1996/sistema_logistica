package bean;

import java.math.BigDecimal;

public class beandetalleordencompra {

    private static String TIPO;
    private int indice, idInsumo, iddetalle, idorden, idProveedor, igv;
    private Double cantidad, unidadmedida, precio, cantPedido;
    private String codigoInsumo, descripcion, descripcionaux, razonSocial, unidad, unidadInsumo, tipo;
    private int idprovinsumo;

    public static String getTIPO() {
        return TIPO;
    }

    public static void setTIPO(String TIPO) {
        beandetalleordencompra.TIPO = TIPO;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public int getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(int iddetalle) {
        this.iddetalle = iddetalle;
    }

    public int getIdorden() {
        return idorden;
    }

    public void setIdorden(int idorden) {
        this.idorden = idorden;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public int getIdprovinsumo() {
        return idprovinsumo;
    }

    public void setIdprovinsumo(int idprovinsumo) {
        this.idprovinsumo = idprovinsumo;
    }

    public String getDescripcionaux() {
        return descripcionaux;
    }

    public void setDescripcionaux(String descripcionaux) {
        this.descripcionaux = descripcionaux == null ? null : descripcionaux.toUpperCase();
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Double getUnidadmedida() {
        return unidadmedida;
    }

    public void setUnidadmedida(Double unidadmedida) {
        this.unidadmedida = unidadmedida;
    }

    public Object[] getRegistro() {
        return new Object[]{iddetalle, cantidad, idorden, idInsumo, precio, igv, idprovinsumo, descripcionaux};
    }

    public Object[] getRegistroRequerimiento() {
        return new Object[]{iddetalle, cantidad, idorden, null, precio, igv, null, descripcionaux};
    }

    public String getUnidadInsumo() {
        return unidadInsumo;
    }

    public void setUnidadInsumo(String unidadInsumo) {
        this.unidadInsumo = unidadInsumo;
    }

    public double redondear_mas(double numero, int decimales) {
        BigDecimal num = new BigDecimal(numero);
        return num.setScale(decimales, BigDecimal.ROUND_UP).doubleValue();
    }

    public static void main(String[] args) {
        beandetalleordencompra b = new beandetalleordencompra();
        b.setTipo("TIPO A");
        b.setUnidadmedida(23.36);
        b.setCantPedido(10.0);

    }

    public int getIdInsumo() {
        return idInsumo;
    }

    public void setIdInsumo(int idInsumo) {
        this.idInsumo = idInsumo;
    }

    public String getCodigoInsumo() {
        return codigoInsumo;
    }

    public void setCodigoInsumo(String codigoInsumo) {
        this.codigoInsumo = codigoInsumo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getCantPedido() {
        return cantPedido;
    }

    public void setCantPedido(Double cantPedido) {
        double aux0 = 0, aux1 = 0, valor = 0;
        switch (tipo) {
            case "TIPO A (0)":
                aux0 = redondear_mas(unidadmedida > 0 ? (cantPedido / unidadmedida) < 1 ? 1 : cantPedido / unidadmedida : cantPedido, 0);
                //aux0 = RedondeaMas(unidadmedida > 0 ? (cantPedido / unidadmedida) < 1 ? 1 : cantPedido / unidadmedida : cantPedido, 0);
                break;
            case "TIPO A (1)":
                aux0 = redondear_mas(unidadmedida > 0 ? (cantPedido / unidadmedida) : cantPedido, 1);
                //aux0 = RedondeaMas(unidadmedida > 0 ? (cantPedido / unidadmedida) : cantPedido, 1);
                break;
            case "TIPO < 1":
                aux1 = unidadmedida > 0 ? (cantPedido / unidadmedida) : cantPedido;
                //aux0 = aux1 < 0.99 ? 1 : RedondeaMas(aux1, 1);
                aux0 = aux1 < 0.99 ? 1 : redondear_mas(aux1, 1);
                break;
            case "TIPO 50 en 50":
                valor = unidadmedida > 0 ? (cantPedido / unidadmedida) : cantPedido;
                if (valor < 100) {
                    aux0 = 100;
                } else if (valor < 150 && valor > 100) {
                    aux0 = 150;
                } else if (valor < 200 && valor > 150) {
                    aux0 = 200;
                } else if (valor < 250 && valor > 200) {
                    aux0 = 250;
                } else if (valor < 300 && valor > 250) {
                    aux0 = 300;
                } else {
                    //aux0 = RedondeaMas(valor, 0);
                    aux0 = redondear_mas(valor, 0);
                }
                break;
            case "TIPO 0.5 en 0.5":
                aux1 = unidadmedida > 0 ? (cantPedido / unidadmedida) : cantPedido;
                if (aux1 < 0.1) {
                    valor = 0.1;
                } else if (aux1 < 0.15 && aux1 > 0.1) {
                    valor = 0.15;
                } else if (aux1 < 0.2 && aux1 > 0.15) {
                    valor = 0.2;
                } else if (aux1 < 0.25 && aux1 > 0.2) {
                    valor = 0.25;
                } else if (aux1 < 0.3 && aux1 > 0.25) {
                    valor = 0.3;
                } else if (aux1 < 0.35 && aux1 > 0.3) {
                    valor = 0.35;
                } else if (aux1 < 0.4 && aux1 > 0.35) {
                    valor = 0.4;
                } else if (aux1 < 0.45 && aux1 > 0.4) {
                    valor = 0.45;
                } else if (aux1 < 0.5 && aux1 > 0.45) {
                    valor = 0.5;
                } else if (aux1 < 0.55 && aux1 > 0.5) {
                    valor = 0.55;
                } else if (aux1 < 0.6 && aux1 > 0.55) {
                    valor = 0.6;
                } else if (aux1 < 0.65 && aux1 > 0.55) {
                    valor = 0.65;
                } else if (aux1 < 0.7 && aux1 > 0.65) {
                    valor = 0.7;
                } else {
                    //valor = RedondeaMas(aux1, 1);
                    valor = redondear_mas(aux1, 1);
                }
                aux0 = valor;
                break;
            default:
                aux0 = cantPedido;
                break;
        }
        this.cantPedido = aux0;
    }

    public double RedondeaMas(double numero, int decimales) {
        double resul = 0;
        if (decimales == 0) {
            resul = Math.floor(numero) + 1;
        } else {
            String numTex = String.valueOf(numero);
            int numDecimales = numTex.substring(numTex.indexOf("."), numTex.length()).length() - 1;
            double divisor = 1 * (Math.pow(10, numDecimales));
            double entera = (int) numero;
            float parteD = (float) (numero - entera);
            float prod = (float) (parteD * divisor);
            int integer = (int) (prod / (divisor / 10));
            double aumentado = 0;
            if (prod <= 9) {
                aumentado = ((integer) * (divisor / 10));
            } else {
                aumentado = ((integer + 1) * (divisor / 10));
            }
            double a = (prod + (aumentado - prod)) / divisor;
            resul = entera + a;
        }
        return resul;
    }

    public int getIgv() {
        return igv;
    }

    public void setIgv(int igv) {
        this.igv = igv;
    }
}
