package bean;

import funciones.operaciones;

public class beanresultado {

    private int indice;
    private double cantidad, precio, subtotal, igv, neto;
    private String descripcion;
    operaciones op = new operaciones();

    public double getNeto() {
        neto = subtotal + igv;
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getSubtotal() {
        subtotal = precio * cantidad;
        return op.redondear(subtotal, 2);
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getIgv() {
        igv = subtotal * 0.18;
        return op.redondear(igv, 2);
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
