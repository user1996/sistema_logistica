package bean;

public class beanproveedor {

    private int idproveedor, idtipo;
    private String ruc, razonsocial, direccion;
    private boolean valido;

    public int getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(int idproveedor) {
        this.idproveedor = idproveedor;
    }

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial.equals("")?null:razonsocial.toUpperCase().trim();
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion.equals("")?null:direccion.toUpperCase().trim();
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setRegistro(Object[] datos) {
        valido = datos != null;
        if (valido) {
            idproveedor = (int) datos[0];
            ruc = (String) datos[1];
            razonsocial = (String) datos[2];
            direccion = (String) datos[3];
            idtipo = (int) datos[4];
        }

    }

    public Object[] getRegistro() {
        return new Object[]{idproveedor, ruc, razonsocial, direccion, idtipo};
    }

}
