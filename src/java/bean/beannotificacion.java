package bean;

import funciones.operaciones;

public class beannotificacion {

    private operaciones op = new operaciones();
    private int indice, iddetalle, idrequerimiento, idpersona, idusuario, idarea, estado, incluye,idsede,idalarma,idtipo;
    private double cant, precio, subtotal, igv, neto;
    private String msjnotificacion, hora, fecha, documento, datos, area, nombreSede, descripcion, correlativo,nomprov,ruc,estadoS;

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public String getEstadoS() {
        return estadoS;
    }

    public void setEstadoS(String estadoS) {
        this.estadoS = estadoS;
    }

    public String getNomprov() {
        return nomprov;
    }

    public void setNomprov(String nomprov) {
        this.nomprov = nomprov;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public int getIdalarma() {
        return idalarma;
    }

    public void setIdalarma(int idalarma) {
        this.idalarma = idalarma;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public int getIncluye() {
        return incluye;
    }

    public void setIncluye(int incluye) {
        this.incluye = incluye;
    }

    public int getIdarea() {
        return idarea;
    }

    public void setIdarea(int idarea) {
        this.idarea = idarea;
    }

    public double getPrecio() {
        return precio;
    }

    public double getSubtotal() {
        subtotal=cant*precio;
        return op.redondear(subtotal, 2);
    }

    public double getIgv() {
        igv=subtotal*0.18;
        return op.redondear(igv, 2);
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }

    public double getNeto() {
        neto = op.redondear(subtotal + igv, 2);
        return neto;
    }

    public void setNeto(double neto) {
        this.neto = neto;
    }

    public void setPrecio(double precio) {
        double aux=0;
        if (incluye==1){
            aux=precio/1.18;
        }else{
            aux=precio;
        }
        this.precio = op.redondear(aux, 2);
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public double getCant() {
        return cant;
    }

    public void setCant(double cant) {
        this.cant = cant;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdrequerimiento() {
        return idrequerimiento;
    }

    public void setIdrequerimiento(int idrequerimiento) {
        this.idrequerimiento = idrequerimiento;
    }

    public String getMsjnotificacion() {
        return msjnotificacion;
    }

    public void setMsjnotificacion(String msjnotificacion) {
        this.msjnotificacion = msjnotificacion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(int iddetalle) {
        this.iddetalle = iddetalle;
    }

}
