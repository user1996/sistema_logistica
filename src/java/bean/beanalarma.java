package bean;

public class beanalarma {
    private int idalarma, estado;
    private String descripcion, fechacambio;
    public int getIdalarma() {
        return idalarma;
    }

    public void setIdalarma(int idalarma) {
        this.idalarma = idalarma;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion.equals("") ? null : descripcion.toUpperCase().trim();
    }

    public String getFechacambio() {
        return fechacambio;
    }

    public void setFechacambio(String fechacambio) {
        this.fechacambio = fechacambio.equals("")?null:fechacambio;
    }

    public Object[] getDatoAlarma() {
        return new Object[]{idalarma, estado, descripcion, fechacambio};
    }
}
