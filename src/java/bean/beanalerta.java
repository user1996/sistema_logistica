
package bean;

public class beanalerta {
    private int indice,idalerta,idtipo,idprov,idsede,iddetalle,estado,ndias;
    private String nrosuministro,fechavencimiento,fechaalerta,razonsocial,nombretipo;

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public String getNombretipo() {
        return nombretipo;
    }

    public void setNombretipo(String nombretipo) {
        this.nombretipo = nombretipo;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public int getIdalerta() {
        return idalerta;
    }

    public void setIdalerta(int idalerta) {
        this.idalerta = idalerta;
    }

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public int getIdprov() {
        return idprov;
    }

    public void setIdprov(int idprov) {
        this.idprov = idprov;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public int getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(int iddetalle) {
        this.iddetalle = iddetalle;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getNrosuministro() {
        return nrosuministro;
    }

    public void setNrosuministro(String nrosuministro) {
        this.nrosuministro = nrosuministro.equals("")?null:nrosuministro.toUpperCase().trim();
    }

    public String getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(String fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public String getFechaalerta() {
        return fechaalerta;
    }

    public void setFechaalerta(String fechaalerta) {
        this.fechaalerta = fechaalerta;
    }

    public int getNdias() {
        return ndias;
    }

    public void setNdias(int ndias) {
        this.ndias = ndias;
    }
    
    
    public Object[] getRegistraAlerta(){
        return new Object[]{idalerta,nrosuministro,idtipo,idprov,idsede};
    }
    public Object[] getRegistraDetalleAlerta(){
        return new Object[]{iddetalle,idalerta,fechavencimiento,estado,ndias};
    }
}
