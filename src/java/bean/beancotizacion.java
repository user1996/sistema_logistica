
package bean;

public class beancotizacion {
    private int idprovinsumo,idproveedor,idinsumo,igv;
    private double precio;
    
    public int getIdprovinsumo() {
        return idprovinsumo;
    }

    public void setIdprovinsumo(int idprovinsumo) {
        this.idprovinsumo = idprovinsumo;
    }

    public int getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(int idproveedor) {
        this.idproveedor = idproveedor;
    }

    public int getIdinsumo() {
        return idinsumo;
    }

    public void setIdinsumo(int idinsumo) {
        this.idinsumo = idinsumo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public Object[] getRegistro(){
        return new Object[]{idprovinsumo,idinsumo,idproveedor,precio,igv};
    }

    public int getIgv() {
        return igv;
    }

    public void setIgv(int igv) {
        this.igv = igv;
    }
}
