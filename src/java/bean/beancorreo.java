
package bean;

public class beancorreo {
    private int idCorreo,idProveedor;
    private String correo;

    public int getIdCorreo() {
        return idCorreo;
    }

    public void setIdCorreo(int idCorreo) {
        this.idCorreo = idCorreo;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public Object[] getRegistro(){
        return new Object[]{idCorreo,idProveedor,correo};
    }
}
