package bean;

public class beanusuario {

    private int idusuario, idpersona, idtipo, idsede,idarea;
    private String nombreusuario, claveusuario;
    private String documento, primernombre, segundonombre, apellidopaterno, apellidomaterno, genero, telefono;

    public int getIdarea() {
        return idarea;
    }

    public void setIdarea(int idarea) {
        this.idarea = idarea;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero.trim().toUpperCase();
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public int getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(int idtipo) {
        this.idtipo = idtipo;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public String getClaveusuario() {
        return claveusuario;
    }

    public void setClaveusuario(String claveusuario) {
        this.claveusuario = claveusuario;
    }

    public Object[] setData() {
        return new Object[]{idusuario, idpersona, idtipo, nombreusuario, claveusuario, idsede};
    }

    public Object[] getPersonaDato() {
        return new Object[]{idpersona, documento, primernombre, segundonombre, apellidopaterno, apellidomaterno, genero,telefono};
    }

    public Object[] getUsuario() {
        return new Object[]{idusuario, idpersona, idtipo, nombreusuario, claveusuario, idsede,idarea};
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getPrimernombre() {
        return primernombre;
    }

    public void setPrimernombre(String primernombre) {
        this.primernombre = primernombre.equals("") ? null : primernombre.trim().toUpperCase();
    }

    public String getSegundonombre() {
        return segundonombre;
    }

    public void setSegundonombre(String segundonombre) {
        this.segundonombre = segundonombre.equals("") ? null : segundonombre.trim().toUpperCase();
    }

    public String getApellidopaterno() {
        return apellidopaterno;
    }

    public void setApellidopaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno.equals("") ? null : apellidopaterno.trim().toUpperCase();
    }

    public String getApellidomaterno() {
        return apellidomaterno;
    }

    public void setApellidomaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno.equals("") ? null : apellidomaterno.trim().toUpperCase();
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono.equals("")?null:telefono.trim();
    }
}
