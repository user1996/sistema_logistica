package negocio;

import bean.beanalarma;
import bean.beannotificacion;
import bean.beanrequerimiento;
import com.google.gson.JsonObject;
import funciones.functions;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class requerimiento_neg {

    functions func = new functions();

    //Para listar Requerimientos 
    public JsonObject ListaRequerimientosHist() {
        func.LeerStatement("select\n"
                + "ord.idorden,\n"
                + "ord.nroorden,\n"
                + "ord.fechaorden,\n"
                + "pro.razonSocial,\n"
                + "COUNT(det.iddetalleorden) as cant,\n"
                + "sed.nombresede\n"
                + "from ordenCompra ord inner join detalleordenCompra det \n"
                + "on ord.idorden=det.idorden inner join sede sed\n"
                + "on ord.iddestino=sed.idsede inner join proveedor pro\n"
                + "on pro.idproveedor=ord.idproveedor\n"
                + "where ord.idtipo=2\n"
                + "group by ord.idorden,ord.nroorden,ord.fechaorden,sed.nombresede,pro.razonSocial order by ord.idorden desc");
        func.EjecutarResultSet();
        return func.getJson();
    }

    public ArrayList<beannotificacion> listaDet(int idOrden) {
        ArrayList<beannotificacion> lista = new ArrayList<>();
        func.LeerStatement("select\n"
                + "det.cantidadRequerida,\n"
                + "det.descripcionArt,\n"
                + "det.precio,\n"
                + "det.igv,\n"
                + "pro.razonSocial,\n"
                + "pro.ruc,\n"
                + "ord.fechaorden,\n"
                + "ord.nroorden\n"
                + "from ordenCompra ord inner join proveedor pro\n"
                + "on ord.idproveedor=pro.idproveedor inner join detalleordenCompra det\n"
                + "on ord.idorden=det.idorden\n"
                + "where ord.idorden=?\n"
                + "group by det.cantidadRequerida,det.descripcionArt,det.precio,det.igv,pro.razonSocial,pro.ruc,ord.fechaorden,ord.nroorden");
        func.Parametros(new Object[]{idOrden});
        func.EjecutarResultSet();
        DefaultTableModel table = func.getDataTable();
        beannotificacion b_r = null;
        for (int i = 0; i < table.getRowCount(); i++) {
            b_r = new beannotificacion();
            b_r.setCant(Double.parseDouble(table.getValueAt(i, 0).toString()));
            b_r.setDescripcion(table.getValueAt(i, 1).toString());
            b_r.setPrecio(Double.parseDouble(table.getValueAt(i, 2).toString()));
            String incluye = table.getValueAt(i, 3).toString();
            b_r.setIncluye(incluye != null ? incluye.equals("0") ? 0 : 1 : 1);
            b_r.setNomprov(table.getValueAt(i, 4).toString());
            b_r.setRuc(table.getValueAt(i, 5).toString());
            b_r.setFecha(table.getValueAt(i, 6).toString());
            b_r.setCorrelativo(table.getValueAt(i, 7).toString());
            lista.add(b_r);
        }
        return lista;
    }

    public JsonObject ListaRequerimientosHistDet(int idOrden) {
        func.LeerStatement("select\n"
                + "ord.idorden,\n"
                + "det.iddetalleorden,\n"
                + "det.cantidadRequerida,\n"
                + "det.descripcionArt,\n"
                + "det.precio,\n"
                + "det.igv\n"
                + "from ordenCompra ord inner join detalleordenCompra det \n"
                + "on ord.idorden=det.idorden inner join sede sed\n"
                + "on ord.iddestino=sed.idsede inner join proveedor pro\n"
                + "on pro.idproveedor=ord.idproveedor\n"
                + "where ord.idtipo=2 and ord.idorden=?");
        func.Parametros(new Object[]{idOrden});
        func.EjecutarResultSet();
        return func.getJson();
    }

    //
    //para las alarmas
    public int RegistraAlarma(beanalarma balar) {
        func.LeerStatement("{call sp_" + (balar.getIdalarma() > 0 ? "actualiza" : "registra") + "Alarma(?,?,?,?)}");
        func.Parametros(balar.getDatoAlarma());
        return func.Execute(balar.getIdalarma() > 0 ? 1 : 0);
    }

    public ArrayList<beannotificacion> listNotificacion() {
        String sql = "select\n"
                + "det.idrequerimiento,\n"
                + "concat((select CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) from persona per where per.idpersona=(select idpersona from usuario where idusuario=req.idusuario)),' del area de ',ar.nombrearea,' tiene ',(select count(*) from detallerequerimiento det inner join alarma al\n"
                + "on al.idalarma=det.idalarma where det.idrequerimiento=req.idrequerimiento and al.estado=0),' requerimiento(s) para la sala ',sed.nombresede) as msjnotif,\n"
                + "concat(DATENAME(MONTH,req.fechahora),' ',DAY(req.fechahora),', ',year(req.fechahora)) as fecha,\n"
                + "format(req.fechahora,'HH:mm:ss') as hora\n"
                + "from usuario usu inner join sede sed\n"
                + "on usu.idsede=sed.idsede inner join area ar \n"
                + "on usu.idarea=ar.idarea inner join requerimiento req\n"
                + "on usu.idusuario=req.idusuario inner join detallerequerimiento det\n"
                + "on req.idrequerimiento=det.idrequerimiento inner join alarma al\n"
                + "on det.idalarma=al.idalarma\n"
                + "where al.estado=0\n"
                + "group by det.idrequerimiento,req.idrequerimiento,req.idusuario,ar.nombrearea,sed.nombresede,req.fechahora";
        func.LeerStatement(sql);
        func.EjecutarResultSet();
        ArrayList<beannotificacion> lstNotif = new ArrayList<>();
        DefaultTableModel tabl = func.getDataTable();
        int cols = tabl.getColumnCount();
        int rows = tabl.getRowCount();
        if (rows > 0) {
            beannotificacion b_notif = null;
            for (int i = 0; i < rows; i++) {
                b_notif = new beannotificacion();
                b_notif.setIdrequerimiento(Integer.parseInt(tabl.getValueAt(i, 0).toString()));
                b_notif.setMsjnotificacion(tabl.getValueAt(i, 1).toString());
                b_notif.setFecha(tabl.getValueAt(i, 2).toString());
                b_notif.setHora(tabl.getValueAt(i, 3).toString());
                lstNotif.add(b_notif);
            }
        } else {
            lstNotif = null;
        }
        return lstNotif;
    }

    public ArrayList<beannotificacion> listNotificacionUsuario(int idUsuario) {
        String sql = "select\n"
                + "det.idrequerimiento,\n"
                + "(\n"
                + "select SUM(a.estado)from detallerequerimiento d inner join alarma a\n"
                + "on d.idalarma=a.idalarma where d.idrequerimiento=det.idrequerimiento\n"
                + ") as cantidad,\n"
                + "concat(DATENAME(MONTH,req.fechahora),' ',DAY(req.fechahora),', ',year(req.fechahora)) as fecha,\n"
                + "format(req.fechahora,'HH:mm:ss') as hora\n"
                + "from usuario usu inner join sede sed\n"
                + "on usu.idsede=sed.idsede inner join area ar \n"
                + "on usu.idarea=ar.idarea inner join requerimiento req\n"
                + "on usu.idusuario=req.idusuario inner join detallerequerimiento det\n"
                + "on req.idrequerimiento=det.idrequerimiento inner join alarma al\n"
                + "on det.idalarma=al.idalarma\n"
                + "where req.idusuario=? and al.estado in (1,2,3)\n"
                + "group by det.idrequerimiento,req.idrequerimiento,req.idusuario,ar.nombrearea,sed.nombresede,req.fechahora";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idUsuario});
        func.EjecutarResultSet();
        ArrayList<beannotificacion> lstNotif = new ArrayList<>();
        DefaultTableModel tabl = func.getDataTable();
        int cols = tabl.getColumnCount();
        int rows = tabl.getRowCount();
        if (rows > 0) {
            beannotificacion b_notif = null;
            for (int i = 0; i < rows; i++) {
                b_notif = new beannotificacion();
                b_notif.setIdrequerimiento(Integer.parseInt(tabl.getValueAt(i, 0).toString()));
                b_notif.setCant(Integer.parseInt(tabl.getValueAt(i, 1).toString()));
                b_notif.setMsjnotificacion(b_notif.getCant() > 0 ? "Su Requerimiento fue Verificado y Evaluado" : "PRueba");
                b_notif.setFecha(tabl.getValueAt(i, 2).toString());
                b_notif.setHora(tabl.getValueAt(i, 3).toString());
                lstNotif.add(b_notif);
            }
        } else {
            lstNotif = null;
        }
        return lstNotif;
    }

    public JsonObject getjsonDetalleNotif(String idreq) {
        String sql = "select\n"
                + "det.iddetalle,\n"
                + "req.idrequerimiento,\n"
                + "per.idpersona,\n"
                + "usu.idusuario,\n"
                + "per.documento,\n"
                + "CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) as datos,\n"
                + "sed.nombresede,\n"
                + "CONCAT(DATENAME(WEEKDAY,req.fechahora),' ',DATENAME(DAY,req.fechahora),' de ',DATENAME(MONTH,req.fechahora),' del ',DATENAME(YEAR,req.fechahora)) as fecha,\n"
                + "--FORMAT(req.fechahora,'yyyy-MM-dd') as fecha,\n"
                + "FORMAT(req.fechahora,'hh:mm tt', 'en-US') as hora,\n"
                + "det.cant,\n"
                + "det.descripcion,\n"
                + "al.estado,\n"
                + "case al.estado\n"
                + "when 0 then 'Pendiente'\n"
                + "when 1 then 'Aprobado'\n"
                + "when 2 then 'Rechazado'\n"
                + "else 'Comprado'\n"
                + "end as estadoInfo,\n"
                + "al.idalarma,\n"
                + "al.descripcion as descAlarma\n"
                + "from persona per inner join  usuario usu\n"
                + "on per.idpersona=usu.idpersona inner join sede sed\n"
                + "on usu.idsede=sed.idsede inner join requerimiento req\n"
                + "on usu.idusuario=req.idusuario inner join detallerequerimiento det\n"
                + "on req.idrequerimiento=det.idrequerimiento inner join alarma al\n"
                + "on det.idalarma=al.idalarma\n"
                + "where req.idrequerimiento=?";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idreq});
        func.EjecutarResultSet();
        return func.getJson();
    }

    ///Alarmas
    public int Actualizaestado(int idalarma, int estado, String motivo) {
        func.LeerStatement("update alarma set estado=?,fechacambio=getdate(),descripcion=? where idalarma=?");
        func.Parametros(new Object[]{estado, motivo, idalarma});
        return func.Execute(1);
    }

    public int ActualizaCant(int idDetalle, double cant) {
        func.LeerStatement("update detallerequerimiento set cant=? where iddetalle=?");
        func.Parametros(new Object[]{cant,idDetalle});
        return func.Execute(1);
    }

    /////
    public ArrayList<beannotificacion> listDetalleNotif(String idRequerimiento) {
        String sql = "select\n"
                + "det.iddetalle,\n"
                + "req.idrequerimiento,\n"
                + "per.idpersona,\n"
                + "usu.idusuario,\n"
                + "per.documento,\n"
                + "CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) as datos,\n"
                + "sed.nombresede,\n"
                + "CONCAT(DATENAME(WEEKDAY,req.fechahora),' ',DATENAME(DAY,req.fechahora),' de ',DATENAME(MONTH,req.fechahora),' del ',DATENAME(YEAR,req.fechahora)) as fecha,\n"
                + "--FORMAT(req.fechahora,'yyyy-MM-dd') as fecha,\n"
                + "FORMAT(req.fechahora,'hh:mm:ss tt','en-US') as hora,\n"
                + "det.cant,\n"
                + "det.descripcion,\n"
                + "al.estado,\n"
                + "req.correlativo\n"
                + "from persona per inner join  usuario usu\n"
                + "on per.idpersona=usu.idpersona inner join sede sed\n"
                + "on usu.idsede=sed.idsede inner join requerimiento req\n"
                + "on usu.idusuario=req.idusuario inner join detallerequerimiento det\n"
                + "on req.idrequerimiento=det.idrequerimiento inner join alarma al\n"
                + "on det.idalarma=al.idalarma\n"
                + "where req.idrequerimiento=?";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idRequerimiento});
        func.EjecutarResultSet();
        ArrayList<beannotificacion> lstNotif = new ArrayList<>();
        DefaultTableModel tabl = func.getDataTable();
        int cols = tabl.getColumnCount();
        int rows = tabl.getRowCount();
        if (rows > 0) {
            beannotificacion b_notif = null;
            for (int i = 0; i < rows; i++) {
                b_notif = new beannotificacion();
                b_notif.setIddetalle(Integer.parseInt(tabl.getValueAt(i, 0).toString()));
                b_notif.setIdrequerimiento(Integer.parseInt(tabl.getValueAt(i, 1).toString()));
                b_notif.setIdpersona(Integer.parseInt(tabl.getValueAt(i, 2).toString()));
                b_notif.setIdusuario(Integer.parseInt(tabl.getValueAt(i, 3).toString()));
                b_notif.setDocumento(tabl.getValueAt(i, 4).toString());
                b_notif.setDatos(tabl.getValueAt(i, 5).toString());
                b_notif.setNombreSede(tabl.getValueAt(i, 6).toString());
                b_notif.setFecha(tabl.getValueAt(i, 7).toString());
                b_notif.setHora(tabl.getValueAt(i, 8).toString());
                b_notif.setCant(Double.parseDouble(tabl.getValueAt(i, 9).toString()));
                b_notif.setDescripcion(tabl.getValueAt(i, 10).toString());
                b_notif.setEstado(Integer.parseInt(tabl.getValueAt(i, 11).toString()));
                b_notif.setCorrelativo(tabl.getValueAt(i, 12).toString());
                lstNotif.add(b_notif);
            }
        } else {
            lstNotif = null;
        }
        return lstNotif;
    }

    public JsonObject listaJsonfiltro(int idarea, int idsede, int estado) {
        func.LeerStatement("{call VistaFiltro(?,?,?)}");
        func.Parametros(new Object[]{idsede, idarea, estado});
        func.EjecutarResultSet();
        return func.getJson();
    }

    public ArrayList<beannotificacion> listaCompra(int idarea, int idsede, int estado) {
        func.LeerStatement("{call VistaFiltro(?,?,?)}");
        func.Parametros(new Object[]{idsede, idarea, estado});
        func.EjecutarResultSet();
        DefaultTableModel table = func.getDataTable();
        ArrayList<beannotificacion> lista = new ArrayList<>();
        for (int i = 0; i < table.getRowCount(); i++) {
            beannotificacion b_n = new beannotificacion();
            b_n.setCant(Double.parseDouble(table.getValueAt(i, 0).toString()));
            b_n.setDescripcion(table.getValueAt(i, 1).toString());
            b_n.setIdusuario(Integer.parseInt(table.getValueAt(i, 2).toString()));
            b_n.setIdarea(Integer.parseInt(table.getValueAt(i, 3).toString()));
            b_n.setEstado(Integer.parseInt(table.getValueAt(i, 4).toString()));
            b_n.setIdsede(Integer.parseInt(table.getValueAt(i, 6).toString()));
            b_n.setIdalarma(Integer.parseInt(table.getValueAt(i, 7).toString()));
            b_n.setIdtipo(Integer.parseInt(table.getValueAt(i, 8).toString()));
            b_n.setArea(table.getValueAt(i, 9).toString());
            b_n.setDatos(table.getValueAt(i, 10).toString());
            b_n.setIddetalle(Integer.parseInt(table.getValueAt(i, 12).toString()));
            lista.add(b_n);
        }
        return lista;
    }

    public List<Integer> listaId(int idusuario) {
        String sqlf = idusuario > 0 ? " where idusuario=" + idusuario : "";
        ArrayList<Integer> lista = new ArrayList<>();
        String sql = "select idrequerimiento from requerimiento" + sqlf;
        func.LeerStatement(sql);
        //func.Parametros(new Object[]{idusuario});
        func.EjecutarResultSet();
        Object[] datoar = func.getDato1();
        if (datoar != null) {
            for (int i = 0; i < datoar.length; i++) {
                lista.add(i, Integer.parseInt(datoar[i].toString()));
            }
        }
        return lista;
    }

    public ArrayList<beannotificacion> listNotif(int idusuario) {
        String fragSql = idusuario > 0 ? "where req.idusuario=" + idusuario + "\n" : "";
        String sql = "select\n"
                + "req.idrequerimiento,\n"
                + "req.correlativo,\n"
                + "format(req.fechahora, 'dd/MM/yyyy') as fecha,\n"
                + "FORMAT(req.fechahora,'hh:mm:ss tt','en-US') as hora,\n"
                + "concat(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) as persona,\n"
                + "COUNT(det.iddetalle) as Cantrequerimientos,\n"
                + "ar.nombrearea,\n"
                + "sed.nombresede\n"
                + "from requerimiento req inner join detallerequerimiento det\n"
                + "on req.idrequerimiento=det.idrequerimiento inner join alarma al\n"
                + "on det.idalarma=al.idalarma inner join usuario usu\n"
                + "on req.idusuario=usu.idusuario inner join persona per on\n"
                + "usu.idpersona=per.idpersona inner join sede sed on \n"
                + "req.idsede=sed.idsede inner join area ar on\n"
                + "ar.idarea=req.idarea\n"
                + fragSql
                + "group by req.idrequerimiento,\n"
                + "req.correlativo,\n"
                + "req.fechahora,\n"
                + "per.primernombre,\n"
                + "per.apellidopaterno,\n"
                + "per.apellidomaterno,\n"
                + "ar.nombrearea,\n"
                + "sed.nombresede\n"
                + "order by req.fechahora desc";
        func.LeerStatement(sql);
        func.EjecutarResultSet();
        ArrayList<beannotificacion> lstNotif = new ArrayList<>();
        DefaultTableModel tabl = func.getDataTable();
        int cols = tabl.getColumnCount();
        int rows = tabl.getRowCount();
        if (rows > 0) {
            beannotificacion b_notif = null;
            for (int i = 0; i < rows; i++) {
                b_notif = new beannotificacion();
                b_notif.setIdrequerimiento(Integer.parseInt(tabl.getValueAt(i, 0).toString()));
                b_notif.setCorrelativo(tabl.getValueAt(i, 1).toString());
                b_notif.setFecha(tabl.getValueAt(i, 2).toString());
                b_notif.setHora(tabl.getValueAt(i, 3).toString());
                b_notif.setDatos(tabl.getValueAt(i, 4).toString());
                b_notif.setCant(Integer.parseInt(tabl.getValueAt(i, 5).toString()));
                b_notif.setArea(tabl.getValueAt(i, 6).toString());
                b_notif.setNombreSede(tabl.getValueAt(i, 7).toString());
                lstNotif.add(b_notif);
            }
        } else {
            lstNotif = null;
        }
        return lstNotif;
    }

    public ArrayList<beannotificacion> listNotifUsuario(int idUsuario) {
        String sql = "select\n"
                + "req.idrequerimiento,\n"
                + "det.descripcion,\n"
                + "al.estado,\n"
                + "case\n"
                + "when al.estado=1 then 'Aceptado'\n"
                + "when al.estado=2 then 'Rechazado'\n"
                + "when al.estado=3 then 'Aceptado'\n"
                + "end as estado,\n"
                + "FORMAT(al.fechacambio,'yyyy/MM/dd') as fecha,\n"
                + "FORMAT(al.fechacambio,'HH:mm:ss') as hora\n"
                + "from requerimiento req inner join\n"
                + "detallerequerimiento det on req.idrequerimiento=det.idrequerimiento\n"
                + "inner join alarma al on al.idalarma=det.idalarma\n"
                + "where req.idusuario=?";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idUsuario});
        func.EjecutarResultSet();
        ArrayList<beannotificacion> lstNotif = new ArrayList<>();
        DefaultTableModel tabl = func.getDataTable();
        int cols = tabl.getColumnCount();
        int rows = tabl.getRowCount();
        if (rows > 0) {
            beannotificacion b_notif = null;
            for (int i = 0; i < rows; i++) {
                b_notif = new beannotificacion();
                b_notif.setIdrequerimiento(Integer.parseInt(tabl.getValueAt(i, 0).toString()));
                b_notif.setDescripcion(tabl.getValueAt(i, 1).toString());
                b_notif.setEstado(Integer.parseInt(tabl.getValueAt(i, 2).toString()));
                b_notif.setEstadoS(tabl.getValueAt(i, 3).toString());
                b_notif.setFecha(tabl.getValueAt(i, 4).toString());
                b_notif.setHora(tabl.getValueAt(i, 5).toString());
                lstNotif.add(b_notif);
            }
        } else {
            lstNotif = null;
        }
        return lstNotif;
    }

    public JsonObject listAlarmas() {
        String sql1 = "select \n"
                + "req.idrequerimiento,\n"
                + "(select CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) from persona per where per.idpersona=req.idusuario) as Responsable,\n"
                + "ar.nombrearea,\n"
                + "sed.nombresede,\n"
                + "format(req.fechahora,'dd-MM-yyyy') as fecha,\n"
                + "format(req.fechahora,'HH:mm:ss') as hora,\n"
                + "(select COUNT(*) from detallerequerimiento det inner join alarma al\n"
                + "on al.idalarma=det.idalarma where det.idrequerimiento=req.idrequerimiento) as total,\n"
                + "(select COUNT(*) from detallerequerimiento det inner join alarma al\n"
                + "on al.idalarma=det.idalarma where det.idrequerimiento=req.idrequerimiento and al.estado=0) as cantPendientes,\n"
                + "(select COUNT(*) from detallerequerimiento det inner join alarma al\n"
                + "on al.idalarma=det.idalarma where det.idrequerimiento=req.idrequerimiento and al.estado=1) as cantAtendidas,\n"
                + "(select COUNT(*) from detallerequerimiento det inner join alarma al\n"
                + "on al.idalarma=det.idalarma where det.idrequerimiento=req.idrequerimiento and al.estado=2) as cantRechazadas\n"
                + "from requerimiento req inner join area ar \n"
                + "on req.idarea=ar.idarea inner join sede sed\n"
                + "on req.idsede=sed.idsede";
        func.LeerStatement(sql1);
        func.EjecutarResultSet();
        return func.getJson();
    }

    //////
    public int CantRows(int idSede) {
        func.LeerStatement("select count(*) from requerimiento where idsede=?");
        func.Parametros(new Object[]{idSede});
        func.EjecutarResultSet();
        Object[] dato = func.getDato1();
        return dato != null ? Integer.parseInt(dato[0].toString()) : 0;
    }

    public int RegistraRequerimiento(beanrequerimiento breq) {
        func.LeerStatement("{call sp_registrarRequerimiento(?,?,?,?,?,?)}");
        func.Parametros(breq.getDatoRequerimiento());
        return func.Execute(0);
    }

    public int updateDetalleRequerimiento(int iddetalle, double cantidad) {
        func.LeerStatement("update detallerequerimiento set cant=? where iddetalle=?");
        func.Parametros(new Object[]{cantidad, iddetalle});
        return func.Execute(1);
    }

    public int RegistrarDetalleRequrimiento(beanrequerimiento breq) {
        func.LeerStatement("{call sp_registraDetalleRequerimiento(?,?,?,?,?,?)}");
        func.Parametros(breq.getDatoDetalleRequrimiento());
        return func.Execute(0);
    }

    public static void main(String[] args) {
        requerimiento_neg r = new requerimiento_neg();
        System.err.println(r.listaDet(3739).size());
    }
}
