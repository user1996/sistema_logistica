
package negocio;

import bean.beantelefono;
import com.google.gson.JsonObject;
import funciones.functions;

public class telefono_neg {
    functions fun=new functions();
    
    public JsonObject getListaTelefono(int idProveedor){
        fun.LeerStatement("select * from telefonoProveedor where idproveedor=?");
        Object[] parametros={idProveedor};
        fun.Parametros(parametros);
        fun.EjecutarResultSet();
        return fun.getJson();
    }
    public int GuardaTelefono(beantelefono btelf){
        fun.LeerStatement("{call sp_"+(btelf.getIdTelefono()>0?"actualiza":"registra")+"Telefono(?,?,?)}");
        fun.Parametros(btelf.getRegistro());
        return fun.Execute(btelf.getIdTelefono()>0?1:0);
    }
}
