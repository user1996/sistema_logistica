package negocio;

import bean.beanproveedor;
import com.google.gson.JsonObject;
import funciones.functions;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public class proveedor_neg {

    beanproveedor beanpro;
    
    functions fu = new functions();

    public JsonObject ListaProveedor() {
        fu.LeerStatement("select prov.idproveedor,\n"
                + "prov.ruc,\n"
                + "prov.razonSocial,\n"
                + "prov.direccion,\n"
                + "pt.descripcion\n"
                + "from proveedor prov inner join provTipoProducto pt\n"
                + "on prov.idTipo=pt.idTipo");
        fu.EjecutarResultSet();
        return fu.getJson();
    }

    public ArrayList<beanproveedor> lstProveedor() {
        fu.LeerStatement("select prov.idproveedor,\n"
                + "prov.ruc,\n"
                + "prov.razonSocial,\n"
                + "prov.direccion,\n"
                + "pt.descripcion\n"
                + "from proveedor prov inner join provTipoProducto pt\n"
                + "on prov.idTipo=pt.idTipo");
        fu.EjecutarResultSet();
        DefaultTableModel tb=fu.getDataTable();
        ArrayList<beanproveedor> ls=new ArrayList<>();
        for (int i=0;i<tb.getRowCount();i++){
            beanproveedor b=new beanproveedor();
            b.setIdproveedor(Integer.parseInt(tb.getValueAt(i, 0).toString()));
            b.setRazonsocial(tb.getValueAt(i, 2).toString());
            ls.add(b);
        }
        return ls;
    }

    public JsonObject getProveedorId(int idProveedor) {
        fu.LeerStatement("select idproveedor,\n"
                + "prov.ruc,\n"
                + "prov.razonSocial,\n"
                + "prov.direccion,\n"
                + "provtipo.idTipo,\n"
                + "provtipo.descripcion\n"
                + "from proveedor prov inner join provTipoProducto provtipo on\n"
                + "prov.idTipo=provtipo.idTipo\n"
                + " where prov.idproveedor=?");
        Object[] param = {idProveedor};
        fu.Parametros(param);
        fu.EjecutarResultSet();
        return fu.getJson();
    }

    public JsonObject ListaProvInsumo() {
        String sq = "select\n"
                + "pin.idProvInsumo,\n"
                + "ins.codigoInsumo,\n"
                + "pro.ruc,\n"
                + "pro.razonSocial,\n"
                + "ins.descripcionInsumo,\n"
                + "CONVERT(decimal(18,2),pin.precio) as precio,\n"
                + "CONVERT(decimal(18,2),iif(igv=1,pin.precio/1.18,pin.precio)) as base,\n"
                + "CONVERT(decimal(18,2),iif(igv=1,(pin.precio/1.18)*0.18,0)) as igv\n"
                + "from ProvInsumo pin inner join proveedor pro\n"
                + "on pin.idProveedor=pro.idproveedor inner join insumo ins\n"
                + "on pin.idInsumo=ins.idInsumo";
        fu.LeerStatement(sq);
        fu.EjecutarResultSet();
        return fu.getJson();
    }

    public int GuardaProveedor(beanproveedor bprov) {
        String sql = "{call sp_" + (bprov.getIdproveedor() > 0 ? "actualiza" : "registra") + "proveedor(?,?,?,?,?)}";
        fu.LeerStatement(sql);
        fu.Parametros(bprov.getRegistro());
        return fu.Execute(bprov.getIdproveedor() > 0 ? 1 : 0);
    }

    public int GuardaProveedorAux(beanproveedor bprov) {
        String sql = "{call sp_registraproveedoraux(?,?,?)}";
        fu.LeerStatement(sql);
        fu.Parametros(bprov.getRegistro());
        return fu.Execute(bprov.getIdproveedor() > 0 ? 1 : 0);
    }

    public static void main(String args[]) {
        proveedor_neg p = new proveedor_neg();
        System.err.println(p.ListaProvInsumo());
    }
}
