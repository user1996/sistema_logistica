package negocio;

import bean.beandetalleordencompra;
import bean.beanordencompra;
import com.google.gson.JsonObject;
import funciones.functions;

public class ordenpedido_neg {

    functions func = new functions();

    public int RegistraOrdenCompra(beanordencompra borden) {
        func.LeerStatement("{call sp_" + (borden.getIdorden() > 0 ? "actualiza" : "registra") + "OrdenPedido(?,?,?,?,?,?,?,?,?)}");
        func.Parametros(borden.getRegistro());
        return func.Execute(borden.getIdorden() > 0 ? 1 : 0);
    }

    public int RegistraDetalleOrdenCompra(beandetalleordencompra bdet) {
        func.LeerStatement("{call sp_" + (bdet.getIddetalle() > 0 ? "actualiza" : "registra") + "detallecompra(?,?,?,?,?,?,?,?)}");
        func.Parametros(bdet.getRegistro());
        return func.Execute(bdet.getIddetalle() > 0 ? 1 : 0);
    }

    public int RegistraDetalleOrdenCompraReq(beandetalleordencompra bdet) {
        func.LeerStatement("{call sp_" + (bdet.getIddetalle() > 0 ? "actualiza" : "registra") + "detallecompra(?,?,?,?,?,?,?,?)}");
        func.Parametros(bdet.getRegistroRequerimiento());
        return func.Execute(bdet.getIddetalle() > 0 ? 1 : 0);
    }

    public int ActualizaDetalle(int iddetalle, double precio) {
        func.LeerStatement("update detalleordenCompra set precio=? where iddetalleorden=?");
        func.Parametros(new Object[]{precio, iddetalle});
        return func.Execute(1);
    }

    public int ActualizaCotiz(int idprovinsumo, double precio) {
        func.LeerStatement("update ProvInsumo set precio=? where idProvInsumo=?");
        func.Parametros(new Object[]{precio, idprovinsumo});
        return func.Execute(1);
    }

    public int AddDetalleCompra(double cant, int idInsumo, int idOrden, double precio, int iddetalle, int igv, int idprovinsumo) {
        func.LeerStatement("{call sp_addDetalle(?,?,?,?,?,?,?)}");
        func.Parametros(new Object[]{iddetalle, cant, idOrden, idInsumo, precio, igv, idprovinsumo});
        return func.Execute(0);
    }

    public String getTotalPagar(String idOrden) {
        String sql = "declare @subtotal decimal(18,2)\n"
                + "declare @exonerado decimal(18,2)\n"
                + "declare @igv decimal(18,2)\n"
                + "set @subtotal=(\n"
                + "select iif(sum(cantidadrequerida*precio)is null,0,sum(cantidadrequerida*precio)) from detalleordenCompra where idorden=? and igv=1\n"
                + ")\n"
                + "set @exonerado=(select iif(sum(cantidadrequerida*precio)is null,0,sum(cantidadrequerida*precio)) from detalleordenCompra where idorden=? and igv=0\n"
                + ")\n"
                + "set @igv=(select @subtotal*0.18)\n"
                + "select (@subtotal+@igv+@exonerado)";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idOrden, idOrden});
        func.EjecutarResultSet();
        Object[] dato = func.getDato1();
        return dato != null ? dato[0].toString() : "0";
    }

    public Object[] getTotalPagarReq(String idorden) {
        String sql = "declare @subtotal decimal(18,2)\n"
                + "declare @igv decimal(18,2)\n"
                + "set @subtotal=(\n"
                + "select sum(cantidadRequerida*precio) from detalleordenCompra where idorden=?\n"
                + ")\n"
                + "set @igv=(\n"
                + "select @subtotal*0.18\n"
                + ")\n"
                + "select (@subtotal+@igv) as monto,(select m.nombremoneda from ordenCompra o inner join moneda m on o.idmoneda=m.idmoneda where o.idorden=?) as nombremoneda";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idorden, idorden});
        func.EjecutarResultSet();
        Object[] dato = func.getDato1test();
        return dato != null ? dato : null;
    }

    public int Cantordenes(int idSede, int idtipo) {
        String sql = "select count(*) from ordenCompra ord inner join usuario usu\n"
                + "on ord.idusuario=usu.idusuario inner join sede sed\n"
                + "on usu.idsede=sed.idsede\n"
                + "where sed.idsede=? and ord.idtipo=?";
        func.LeerStatement("select count(*) from ordenCompra ord inner join usuario usu\n"
                + "on ord.idusuario=usu.idusuario inner join sede sed\n"
                + "on usu.idsede=sed.idsede\n"
                + "where ord.iddestino =? and ord.idtipo=?");
        func.Parametros(new Object[]{idSede, idtipo});
        func.EjecutarResultSet();
        Object[] data = func.getDato1test();
        return data != null ? Integer.parseInt(data[0].toString()) : 0;
    }

    public JsonObject ListaOrden(int idTipoUsuario, int idSede, int idTipo) {
        String sqlfrag = "";
        if (idTipoUsuario == 1 || idTipoUsuario == 4) {//si es Administrador o Supervisor
            sqlfrag = " and ord.iddestino=ord.iddestino";
        } else {
            sqlfrag = " and ord.iddestino=" + idSede;
        }
        String sql = "select\n"
                + "ord.idorden,\n"
                + "ord.nroorden,\n"
                + "ord.fechaorden,\n"
                + "pro.razonSocial,\n"
                + "sed.nombresede,\n"
                + "(select count(*) from detalleordenCompra where idorden=ord.idorden) as cant\n"
                + "from ordenCompra ord inner join proveedor pro\n"
                + "on ord.idproveedor=pro.idproveedor inner join usuario usu\n"
                + "on ord.idusuario=usu.idusuario inner join sede sed\n"
                + "on ord.iddestino=sed.idsede where ord.idtipo=? " + sqlfrag + " order by ord.idorden desc";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idTipo});
        func.EjecutarResultSet();
        return func.getJson();
    }

    public JsonObject ListaDetalleOrden(int idOrden) {
        String sql = "select\n"
                + "det.iddetalleorden,\n"
                + "ins.idInsumo,\n"
                + "ins.codigoInsumo,\n"
                + "ins.descripcionInsumo,\n"
                + "uni.descripcion,\n"
                + "det.igv,\n"
                + "det.cantidadRequerida,\n"
                + "det.precio,\n"
                + "pro.ruc,\n"
                + "pro.razonSocial\n"
                + "--(select min(precio) from ProvInsumo where idInsumo=ins.idInsumo) as precio\n"
                + "from ordenCompra ord inner join proveedor pro\n"
                + "on ord.idproveedor=pro.idproveedor inner join detalleordenCompra det\n"
                + "on ord.idorden=det.idorden inner join insumo ins\n"
                + "on det.idInsumo=ins.idInsumo inner join unidadMedida uni\n"
                + "on ins.idUnidad=uni.idUnidad\n"
                + "where det.idorden=?";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idOrden});
        func.EjecutarResultSet();
        return func.getJson();
    }

    public JsonObject ListaDetalleOrdenGetId(int idDetalle) {
        String where = idDetalle > 0 ? "where det.iddetalleorden=" + idDetalle : "";
        String sql = "select\n"
                + "det.iddetalleorden,\n"
                + "det.idProvInsumo,\n"
                + "ins.idInsumo,\n"
                + "ins.codigoInsumo,\n"
                + "ins.descripcionInsumo,\n"
                + "uni.descripcion,\n"
                + "det.igv,\n"
                + "det.cantidadRequerida,\n"
                + "det.precio,\n"
                + "pro.ruc,\n"
                + "pro.razonSocial\n"
                + "--(select min(precio) from ProvInsumo where idInsumo=ins.idInsumo) as precio\n"
                + "from ordenCompra ord inner join proveedor pro\n"
                + "on ord.idproveedor=pro.idproveedor inner join detalleordenCompra det\n"
                + "on ord.idorden=det.idorden inner join insumo ins\n"
                + "on det.idInsumo=ins.idInsumo inner join unidadMedida uni\n"
                + "on ins.idUnidad=uni.idUnidad\n"
                + where;
        func.LeerStatement(sql);
        func.EjecutarResultSet();
        return func.getJson();
    }

    public JsonObject ListaInsumoFiltro(int idOrden) {
        String sql = "select\n"
                + "ins.idInsumo,\n"
                + "pin.idProvInsumo,\n"
                + "pin.idProveedor,\n"
                + "ins.codigoInsumo,\n"
                + "ins.descripcionInsumo,\n"
                + "un.descripcion,\n"
                + "pin.precio,\n"
                + "pin.igv\n"
                + "from ProvInsumo pin inner join insumo ins\n"
                + "on pin.idInsumo=ins.idInsumo inner join unidadMedida un\n"
                + "on ins.idUnidad=un.idUnidad\n"
                + "where pin.idProveedor=(select idproveedor from ordenCompra where idorden=?) and pin.idInsumo not in(select idInsumo from detalleordenCompra where idorden=?)";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{idOrden, idOrden});
        func.EjecutarResultSet();
        return func.getJson();
    }

    public static void main(String[] args) {
        ordenpedido_neg ord = new ordenpedido_neg();
        System.err.println(ord.ListaOrden(0, 0, 0));
    }
}
