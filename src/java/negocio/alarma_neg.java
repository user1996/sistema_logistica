package negocio;

import bean.beanalerta;
import bean.beantipoalerta;
import com.google.gson.JsonObject;
import funciones.functions;
import funciones.operaciones;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;

public class alarma_neg {

    functions func = new functions();

    public ArrayList<beantipoalerta> ListTipo() {
        ArrayList<beantipoalerta> lista = new ArrayList<>();
        func.LeerStatement("SELECT * FROM tipoalerta");
        func.EjecutarResultSet();
        DefaultTableModel table = func.getDataTable();
        beantipoalerta btip;
        for (int i = 0; i < table.getRowCount(); i++) {
            btip = new beantipoalerta();
            btip.setIdtipo(Integer.parseInt(table.getValueAt(i, 0).toString()));
            btip.setNombretipo(table.getValueAt(i, 1).toString());
            lista.add(btip);
        }
        return lista;
    }

    public JsonObject listAlertas() {
        String sql = "select\n"
                + "al.idalerta,\n"
                + "pro.razonSocial,\n"
                + "al.nrosuministro,\n"
                + "tip.nombretipo,\n"
                + "sed.nombresede,\n"
                + "(select MAX(fechavencimiento) from detallealerta where idalerta=al.idalerta and estado=0) as fechaVenc\n"
                + "from alerta al inner join proveedor pro\n"
                + "on pro.idproveedor=al.idproveedor inner join tipoalerta tip\n"
                + "on al.idtipo=tip.idtipo inner join sede sed\n"
                + "on al.idsede=sed.idsede";
        func.LeerStatement(sql);
        func.EjecutarResultSet();
        return func.getJson();
    }

    public int GuardaAlerta(beanalerta bale) {
        func.LeerStatement("{call sp_registraralerta(?,?,?,?,?)}");
        func.Parametros(bale.getRegistraAlerta());
        int id = 0;
        id = func.Execute(0);
        System.err.println(id > 0 ? "Alerta Registrada con Id :" + id : " Alerta no Registrada ");
        return id;
    }

    public List<Integer> listIds() {
        beanalerta bal;
        func.LeerStatement("select\n"
                + "det.idalerta,\n"
                + "fechavencimiento\n"
                + "from detallealerta det \n"
                + "where \n"
                + "det.estado=0 and\n"
                + "((GETDATE() between det.fechaalerta and det.fechavencimiento) or (det.fechaalerta<getdate()))");
        func.EjecutarResultSet();
        DefaultTableModel tab = func.getDataTable();
        if (tab != null) {
            ArrayList<beanalerta> list = new ArrayList<>();
            for (int i = 0; i < tab.getRowCount(); i++) {
                bal = new beanalerta();
                bal.setIdalerta(Integer.parseInt(tab.getValueAt(i, 0).toString()));
                bal.setFechavencimiento(tab.getValueAt(i, 1).toString());
                list.add(bal);
            }
            operaciones op = new operaciones();
            return op.ListaIds(list);
        } else {
            return null;
        }
    }

    public ArrayList<beanalerta> listDetalleAlerta(int id) {
        String sql = "select\n"
                + "al.idalerta,\n"
                + "pr.razonSocial,\n"
                + "tip.nombretipo,\n"
                + "det.fechavencimiento,\n"
                + "det.iddetalle,\n"
                + "det.estado,\n"
                + "det.fechaalerta,\n"
                + "al.idsede,\n"
                + "al.idtipo\n"
                + "from alerta al\n"
                + "inner join proveedor pr on pr.idproveedor=al.idproveedor\n"
                + "inner join tipoalerta tip on tip.idtipo=al.idtipo\n"
                + "inner join detallealerta det on det.idalerta=al.idalerta\n"
                + "where al.idalerta=?";
        func.LeerStatement(sql);
        func.Parametros(new Object[]{id});
        func.EjecutarResultSet();
        DefaultTableModel tbl = func.getDataTable();
        if (tbl != null) {
            beanalerta bal;
            ArrayList<beanalerta> lista = new ArrayList<>();
            for (int i = 0; i < tbl.getRowCount(); i++) {
                bal = new beanalerta();
                bal.setIdalerta(Integer.parseInt(tbl.getValueAt(i, 0).toString()));
                bal.setRazonsocial(tbl.getValueAt(i, 1).toString());
                bal.setNombretipo(tbl.getValueAt(i, 2).toString());
                bal.setFechavencimiento(tbl.getValueAt(i, 3).toString());
                bal.setIddetalle(Integer.parseInt(tbl.getValueAt(i, 4).toString()));
                bal.setEstado(Integer.parseInt(tbl.getValueAt(i, 5).toString()));
                bal.setFechaalerta(tbl.getValueAt(i, 6).toString());
                bal.setIdsede(Integer.parseInt(tbl.getValueAt(i, 7).toString()));
                bal.setIdtipo(Integer.parseInt(tbl.getValueAt(i, 8).toString()));
                lista.add(bal);
            }
            return lista;
        } else {
            return null;
        }
    }

    public Object[] nexDetalle(int iddetalle) {
        func.LeerStatement("select\n"
                + "det.idalerta,\n"
                + "DATEADD(MONTH,1,det.fechavencimiento) as proxvenc,\n"
                + "DATEDIFF(DAY,det.fechaalerta,det.fechavencimiento) as dias,\n"
                + "0 as estado,\n"
                + "(select idproveedor from alerta where idalerta=det.idalerta) as idproveedor,\n"
                + "(select idsede from alerta where idalerta=det.idalerta) as idsede\n"
                + "from detallealerta det where iddetalle=?");
        func.Parametros(new Object[]{iddetalle});
        func.EjecutarResultSet();
        return func.getDato1test();
    }

    public int GuardaDetalleAlerta(beanalerta bale) {
        func.LeerStatement("{call sp_registradetallealerta(?,?,?,?,?)}");
        func.Parametros(bale.getRegistraDetalleAlerta());
        int id = 0;
        id = func.Execute(0);
        System.err.println(id > 0 ? "Alerta Detalle Registrada con Id :" + id : " Detalle Alerta no Registrada ");
        return id;
    }

    public int ActualizaEstado(int iddetalle) {
        func.LeerStatement("update detallealerta set estado=1 where iddetalle=?");
        func.Parametros(new Object[]{iddetalle});
        int id = 0;
        id = func.Execute(1);
        System.err.println(id > 0 ? "Estado actualizado " : "Estado no Actualizado");
        return id;
    }

    public static void main(String[] args) {
        alarma_neg al = new alarma_neg();
        Object [] d=al.nexDetalle(3040);
        for (Object o:d){
            System.err.println(o);
        }
    }
}
