package negocio;

import bean.beaninsumo;
import com.google.gson.JsonObject;
import funciones.functions;

public class insumo_neg {

    functions fun = new functions();

    public Object[][] getIdInsumo(String codInsumo) {
        fun.LeerStatement("select idInsumo from insumo where codigoInsumo=?");
        Object[] parametros = {codInsumo};
        fun.Parametros(parametros);
        fun.EjecutarResultSet();
        return fun.getDato();
    }

    public Object[] getInsumoConsulta(String idInsumo) {
        String sql = "select top 1\n"
                + "pin.idProvInsumo,\n"
                + "ins.idInsumo,\n"
                + "ins.descripcionInsumo,\n"
                + "un.descripcion,\n"
                + "pro.idproveedor,\n"
                + "pro.razonSocial,\n"
                + "pin.precio,\n"
                + "ins.unidadMed\n"
                + "pin.igv\n"
                + "from insumo ins inner join ProvInsumo pin\n"
                + "on ins.idInsumo=pin.idInsumo inner join unidadMedida un\n"
                + "on ins.idUnidad=un.idUnidad inner join proveedor pro\n"
                + "on pin.idProveedor=pro.idproveedor\n"
                + "where ins.idInsumo=? order by pin.precio asc";
        String sql2 = "select top 1\n"
                + "pin.idProvInsumo,\n"
                + "ins.idInsumo,\n"
                + "ins.descripcionInsumo,\n"
                + "un.descripcion,\n"
                + "pro.idproveedor,\n"
                + "pro.razonSocial,\n"
                + "pin.precio,\n"
                + "ins.unidadMed,\n"
                + "pin.igv,\n"
                + "cl.nombreclass\n"
                + "from insumo ins inner join ProvInsumo pin\n"
                + "on ins.idInsumo=pin.idInsumo inner join unidadMedida un\n"
                + "on ins.idUnidad=un.idUnidad inner join proveedor pro\n"
                + "on pin.idProveedor=pro.idproveedor inner join clasificacionInsumo cl\n"
                + "on cl.idclas=ins.idClas\n"
                + "where ins.idInsumo=? order by pin.precio asc";
        fun.LeerStatement(sql2);
        fun.Parametros(new Object[]{idInsumo});
        fun.EjecutarResultSet();
        Object[] data = fun.getDato1test();
        return data;
    }

    public Object[][] getInsumoProveedor(String idInsumo) {
        String sql = "select top 1\n"
                + "pin.idProvInsumo,\n"
                + "ins.unidadMed,\n"
                + "uni.descripcion,\n"
                + "pin.idProveedor\n"
                + "--,\n"
                + "--pin.idInsumo,\n"
                + "--min(convert(decimal(18,2),pin.precio)) as precio,\n"
                + "--min(convert(decimal(18,2),pin.precio-(pin.precio/1.18))) as base,\n"
                + "--(convert(decimal(18,2),precio/1.18)) as subtotal\n"
                + "from ProvInsumo pin inner join insumo ins\n"
                + "on pin.idInsumo=ins.idInsumo inner join unidadMedida uni\n"
                + "on ins.idUnidad=uni.idUnidad\n"
                + "where ins.idInsumo=?\n"
                + "group by pin.idProvInsumo,pin.precio,pin.idInsumo,ins.unidadMed,uni.descripcion,pin.idProveedor\n"
                + "order by pin.precio asc";
        fun.LeerStatement(sql);
        Object[] parametros = {idInsumo};
        fun.Parametros(parametros);
        fun.EjecutarResultSet();
        return fun.getDato();
    }

    public JsonObject getInsumo(int idInsumo) {
        String sql = idInsumo == 0 ? "" : " where ins.idInsumo=" + idInsumo;
        fun.LeerStatement("select\n"
                + "*\n"
                + "from insumo ins inner join unidadMedida un \n"
                + "on ins.idUnidad=un.idUnidad inner join categoriaInsumo cat\n"
                + "on ins.idCategoria=cat.idCategoria" + sql);
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public int GuardaInsumo(beaninsumo bin) {
        fun.LeerStatement("{call sp_" + (bin.getIdinsumo() > 0 ? "actualiza" : "registra") + "insumo(?,?,?,?,?,?,?)}");
        fun.Parametros(bin.getRegistro());
        return fun.Execute(bin.getIdinsumo() > 0 ? 1 : 0);
    }

    public boolean verificaExistencia(String codigoInsumo) {
        fun.LeerStatement("select idInsumo from insumo where codigoInsumo=?");
        fun.Parametros(new Object[]{codigoInsumo});
        fun.EjecutarResultSet();
        return fun.getDato() != null;
    }

    public double RedondeaMas(double numero, int decimales) {
        return 0;
    }

    public static void main(String args[]) {
        insumo_neg ins = new insumo_neg();
        System.err.println(ins.getInsumoConsulta("1"));

    }
}
