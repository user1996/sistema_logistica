package negocio;

import bean.beanpersona;
import funciones.functions;
import java.util.ArrayList;

public class login_neg {

    functions fun = new functions();

    public int getIdusuario(String nombreusuario, String claveusuario) {
        fun.LeerStatement("select\n"
                + "idpersona\n"
                + "from usuario where nombreusuario=? and claveusuario=?");
        fun.Parametros(new Object[]{nombreusuario, claveusuario});
        fun.EjecutarResultSet();
        Object[] dato = fun.getDato1();
        return dato != null ? Integer.parseInt(dato[0].toString()) : 0;
    }

    public ArrayList<beanpersona> datoSesionPersona(int idPersona) {
        int contador = 0;
        ArrayList<beanpersona> lista = new ArrayList<>();
        String ssl = "select\n"
                + "per.idpersona,\n"
                + "usu.idusuario,\n"
                + "CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) as datos,\n"
                + "tip.idtipo,\n"
                + "tip.rol,\n"
                + "sed.idsede,\n"
                + "sed.nombresede,\n"
                + "ar.idarea,\n"
                + "ar.nombrearea,\n"
                + "per.genero\n"
                + "from persona per inner join usuario usu\n"
                + "on per.idpersona=usu.idpersona inner join tipoUsuario tip\n"
                + "on usu.idtipo=tip.idtipo inner join sede sed\n"
                + "on usu.idsede=sed.idsede inner join area ar\n"
                + "on usu.idarea=ar.idarea\n"
                + "where per.idpersona=?";
        fun.LeerStatement(ssl);
        fun.Parametros(new Object[]{idPersona});
        fun.EjecutarResultSet();
        Object[] datoSesion = fun.getDato1test();
        if (datoSesion != null) {
            beanpersona bper = new beanpersona();
            bper.setIdpersona(Integer.parseInt(datoSesion[0].toString()));
            bper.setIdusuario(Integer.parseInt(datoSesion[1].toString()));
            bper.setDatos(datoSesion[2].toString());
            bper.setIdtipo(Integer.parseInt(datoSesion[3].toString()));
            bper.setNombretipo(datoSesion[4].toString());
            bper.setIdsede(Integer.parseInt(datoSesion[5].toString()));
            bper.setNombresede(datoSesion[6].toString());
            bper.setIdarea(Integer.parseInt(datoSesion[7].toString()));
            bper.setNombrearea(datoSesion[8].toString());
            bper.setGenero(datoSesion[9].toString());
            lista.add(bper);
        }
        return lista;
    }

    public static void main(String[] args) {

    }
}
