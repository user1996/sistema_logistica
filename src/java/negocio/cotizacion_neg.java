package negocio;

import bean.beancotizacion;
import com.google.gson.JsonObject;
import funciones.functions;

public class cotizacion_neg {

    functions func = new functions();

    public int GuardaInsumoCotizacion(int idInsumo) {
        Object[] para = {idInsumo};
        func.LeerStatement("{call sp_registraInsumoProv(?)}");
        func.Parametros(para);
        return func.Execute(0);
    }

    public JsonObject FiltraProveedorTest(String ruc) {
        func.LeerStatement("select top 10\n"
                + "pro.idproveedor,\n"
                + "concat(pro.ruc,' - ',pro.razonSocial) as concat\t"
                + "from proveedor pro where pro.ruc like '" + ruc + "%'");
        func.EjecutarResultSet();
        return func.getJson();
    }

    public JsonObject FiltraProveedor(String ruc) {
        func.LeerStatement("select\n"
                + "pro.idproveedor,\n"
                + "pro.ruc,\n"
                + "pro.razonSocial\n"
                + "from proveedor pro where pro.ruc like '" + ruc + "%'");
        func.EjecutarResultSet();
        return func.getJson();
    }

    public int GuardaCotizacion(beancotizacion bcot) {
        func.LeerStatement("{call sp_" + (bcot.getIdprovinsumo() > 0 ? "actualiza" : "registra") + "Cotizacion(?,?,?,?,?)}");
        func.Parametros(bcot.getRegistro());
        return func.Execute(bcot.getIdprovinsumo() > 0 ? 1 : 0);
    }

    public JsonObject getCotizacionId(int idProvInsumo) {
        func.LeerStatement("select\n"
                + "pin.idProvInsumo,\n"
                + "pin.idInsumo,\n"
                + "ins.codigoInsumo,\n"
                + "pin.idProveedor,\n"
                + "pro.ruc,\n"
                + "pro.razonSocial,\n"
                + "ins.descripcionInsumo,\n"
                + "un.descripcion as unidad,\n"
                + "ptipo.descripcion,\n"
                + "pin.precio,\n"
                + "pin.igv\n"
                + "from ProvInsumo pin inner join proveedor pro\n"
                + "on pin.idProveedor=pro.idproveedor inner join provTipoProducto ptipo\n"
                + "on pro.idTipo=ptipo.idTipo inner join insumo ins\n"
                + "on pin.idInsumo=ins.idInsumo inner join unidadMedida un\n"
                + "on ins.idUnidad=un.idUnidad\n"
                + "where pin.idProvInsumo=?");
        func.Parametros(new Object[]{idProvInsumo});
        func.EjecutarResultSet();
        return func.getJson();
    }

    public static void main(String[] args) {
        cotizacion_neg cot = new cotizacion_neg();
        System.err.println(cot.FiltraProveedorTest("101"));
    }
}
