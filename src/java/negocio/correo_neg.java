
package negocio;
import bean.beancorreo;
import com.google.gson.JsonObject;
import funciones.functions;
public class correo_neg {
    functions fun=new functions();
    
    public JsonObject getListaCorreo(int idProveedor){
        fun.LeerStatement("select * from correoProveedor where idproveedor=?");
        Object[] parametros={idProveedor};
        fun.Parametros(parametros);
        fun.EjecutarResultSet();
        return fun.getJson();
    }
    public int GuardaCorreo(beancorreo bcorr){
        fun.LeerStatement("{call sp_"+(bcorr.getIdCorreo()>0?"actualiza":"registra")+"correoproveedor(?,?,?)}");
        fun.Parametros(bcorr.getRegistro());
        return fun.Execute(bcorr.getIdCorreo()>0?1:0);
    }
}
