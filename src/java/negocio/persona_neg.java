package negocio;

import bean.beanusuario;
import com.google.gson.JsonObject;
import funciones.functions;

public class persona_neg {

    functions fun = new functions();

    public JsonObject getLista() {
        fun.LeerStatement("select\n"
                + "idpersona,\n"
                + "documento,\n"
                + "primernombre,\n"
                + "iif(segundonombre is null,'--',segundonombre) as segundonombre,\n"
                + "apellidopaterno,\n"
                + "iif(apellidomaterno is null,'--',apellidomaterno) as apellidomaterno,\n"
                + "genero\n"
                + "from persona");
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public Object[] getDetalleLista(int idPersona) {
        fun.LeerStatement("select\n"
                + "idpersona,\n"
                + "documento,\n"
                + "primernombre,\n"
                + "segundonombre,\n"
                + "apellidopaterno,\n"
                + "apellidomaterno,\n"
                + "genero,\n"
                + "telefono\n"
                + "from persona where idpersona=" + idPersona);
        fun.EjecutarResultSet();
        Object[] data = fun.getDato1test();

        return data;
    }

    public int GuardaPersona(beanusuario busu) {
        fun.LeerStatement("{call sp_" + (busu.getIdpersona() > 0 ? "actualiza" : "registra") + "persona(?,?,?,?,?,?,?,?)}");
        fun.Parametros(busu.getPersonaDato());
        return fun.Execute(busu.getIdpersona() > 0 ? 1 : 0);
    }

    public int GuardaUsuario(beanusuario busu) {
        fun.LeerStatement("{call sp_" + (busu.getIdusuario() > 0 ? "actualiza" : "registra") + "Usuario(?,?,?,?,?,?,?)}");
        fun.Parametros(busu.getUsuario());
        return fun.Execute(busu.getIdusuario() > 0 ? 1 : 0);
    }

    public static void main(String[] args) {
        persona_neg per = new persona_neg();
        System.err.println(per.getLista());
    }

    public JsonObject getListUsuario() {
        fun.LeerStatement("select\n"
                + "usu.idusuario,\n"
                + "per.idpersona,\n"
                + "per.documento,\n"
                + "usu.nombreusuario,\n"
                + "usu.claveusuario,\n"
                + "tip.idtipo,\n"
                + "tip.rol,\n"
                + "sed.idsede,\n"
                + "sed.nombresede,\n"
                + "ar.idarea\n"
                + "from usuario usu inner join tipoUsuario tip\n"
                + "on usu.idtipo=tip.idtipo inner join sede sed\n"
                + "on usu.idsede=sed.idsede inner join persona per\n"
                + "on usu.idpersona=per.idpersona inner join area ar\n"
                + "on usu.idarea=ar.idarea");
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public JsonObject getListaPersona() {
        fun.LeerStatement("select\n"
                + "idpersona,\n"
                + "CONCAT(primernombre,' ',apellidopaterno,' ',apellidomaterno) as datos\n"
                + "from persona");
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public JsonObject getListPersonaUsuario() {
        String sql="select per.idpersona,CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) as datos from persona per where idpersona not in (\n"
                + "select distinct idpersona from usuario\n"
                + ")";
        String sql2="select idpersona,CONCAT(per.primernombre,' ',per.apellidopaterno,' ',per.apellidomaterno) as datos from persona per";
        fun.LeerStatement(sql2);
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public JsonObject getListaTipo() {
        fun.LeerStatement("select * from tipoUsuario");
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public JsonObject getListaSede() {
        fun.LeerStatement("select * from sede");
        fun.EjecutarResultSet();
        return fun.getJson();
    }

    public JsonObject getListaArea() {
        fun.LeerStatement("select * from area");
        fun.EjecutarResultSet();
        return fun.getJson();
    }
}
