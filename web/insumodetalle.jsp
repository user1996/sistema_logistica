<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    String name = request.getParameter("name");
    String cod = request.getParameter("cod");
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Detalle Insumo</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header"><h3>Datos Insumo</h3></div>
            <div class="card-body">
                <form id="frminsumo" action="insumo?action=registraInsumo" method="post">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label>Codigo</label>
                            <input type="text" id="codigoinsumo" name="codigoinsumo" class="form-control form-control-sm" value="<%=cod != null ? cod : ""%>">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Descripcion</label>
                            <input type="text" id="descripcioninsumo" name="descripcioninsumo" class="form-control form-control-sm" value="<%=name != null ? name : ""%>">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Unidad Medida</label>
                            <input type="number" id="unidadmedida" name="unidadmedida" class="form-control form-control-sm">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Presentacion</label>
                            <select id="idunidad" name="idunidad" class="form-control"></select>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Categoria</label>
                            <select id="idcategoria" name="idcategoria" class="form-control"></select>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Clasificacion Insumo</label>
                            <select id="idclasificacion" name="idclasificacion" class="form-control"></select>
                        </div>
                        <input type="text" id="idinsumo" name="idinsumo" class="form-control form-control-sm" style="visibility: hidden">
                        <div class="col-md-12 col-xl-12">
                            <button type="button" class="btn btn-success" onclick="guardaInsumo()"> Registrar</button>
                            <button type="reset" class="btn btn-danger"> Cancelar</button>
                            <a href="<%=cod != null ? "index?action=ordenList" : "index?action=insumo"%>"><span class="btn btn-info">Regresar</span></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var idinsumo = 0;
    $(document).ready(function () {
    <% String id = request.getParameter("id");%>
        idinsumo =<%=id != null ? id : "0"%>
        cargaComboUnidad();
        cargaComboCateg();
        cargaComboClasificacion();
        getDetalle(idinsumo);
    });
    function cargaComboUnidad() {
        $.ajax({
            type: "POST",
            url: "unidadmedida?action=listUnidadMedida",
            success: function (response)
            {
                var json1 = JSON.parse(response);
                $("#idunidad").append('<option value=0>Seleccione...</option>');
                $.each(json1.data, function (key, registro) {
                    $("#idunidad").append('<option value=' + registro.idUnidad + '>' + registro.descripcion + '</option>');
                });
            }
        });
    }
    function cargaComboClasificacion() {
        $.ajax({
            type: "POST",
            url: "clasificacion?action=list",
            success: function (response)
            {
                var json1 = JSON.parse(response);
                $("#idclasificacion").append('<option value=0>Seleccione...</option>');
                $.each(json1.data, function (key, registro) {
                    $("#idclasificacion").append('<option value=' + registro.idclas + '>' + registro.nombreclass + '</option>');
                });
            }
        });
    }
    function cargaComboCateg() {
        $.ajax({
            type: "POST",
            url: "categoria?action=listCategoria",
            success: function (response)
            {
                var json2 = JSON.parse(response);
                $("#idcategoria").append('<option value=0>Seleccione...</option>');
                $.each(json2.data, function (key, registro) {
                    $("#idcategoria").append('<option value=' + registro.idCategoria + '>' + registro.descripcionCategoria + '</option>');
                });
            }
        });
    }
    function getDetalle(idInsumo) {
        if (idInsumo > 0) {
            $.ajax({
                type: "POST",
                url: "insumo?action=listInsumoId&idInsumo=" + idInsumo,
                success: function (response)
                {
                    var json = JSON.parse(response);
                    console.log(json)
                    $.each(json.data, function (key, registro) {
                        document.getElementById("idcategoria").selectedIndex = registro.idCategoria;
                        document.getElementById("idunidad").selectedIndex = registro.idUnidad;
                        $('#idinsumo').val(registro.idInsumo);
                        $('#codigoinsumo').val(registro.codigoInsumo);
                        $('#descripcioninsumo').val(registro.descripcionInsumo);
                        $('#unidadmedida').val(registro.unidadMed);
                        $('#idclasificacion').val(registro.idClas);
                    });
                }
            });
        } else {
            $('#idinsumo').val('0');
        }
    }
    function guardaInsumo() {
        var formulario = $('#frminsumo');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data, textStatus, jqXHR) {
                if (isNaN(data)) {
                    $.alert({
                        title: '<i class="fas fa-info"></i> Aviso',
                        content: data
                    });
                } else {
                    $.alert({
                        title: '<i class="fas fa-info"></i> Aviso',
                        content: "Insumo Registrado"
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.alert(jqXHR + "\t" + textStatus + "\t" + errorThrown);
            }
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
