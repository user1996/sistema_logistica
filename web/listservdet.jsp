<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Orden de Compra (Servicios)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 12px; }
    td { font-size: 13px; }
</style>
<div class="container">
    <div class="card border-info">
        <div class="card-header">Detalle Orden de Compra (Servicios)</div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Nro :</label>
                    <input value="" id="correlativo" class="form-control form-control-sm" disabled="">
                </div>
                <div class="form-group col-md-6">
                    <label>Proveedor :</label>
                    <input value="" id="nomprov" class="form-control form-control-sm" disabled="">
                </div>
                <div class="form-group col-md-6">
                    <label>Ruc :</label>
                    <input value="" id="ruc" class="form-control form-control-sm" disabled="">
                </div>
                <div class="form-group col-md-6">
                    <label>Fecha :</label>
                    <input value="" id="fecha" class="form-control form-control-sm" disabled="">
                </div>
                <div class="form-group col-md-12">
                    <div class="table table-responsive">
                        <table id="TBLDETALLEORDEN"class="table table-hover">
                            <thead>
                            <th>Cant</th>
                            <th>Producto</th>
                            <th>Precio</th>
                            <th>Total</th>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Subtotal :</strong></td>
                                    <td><span id="idst"></span></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><strong>IGV :</strong></td>
                                    <td><span id="idigv"></span></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Neto :</strong></td>
                                    <td><span id="idneto"></span></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>            
        </div>
        <div class="card-footer">
            <a href="index?action=ordServ"><span class="btn btn-info">Regresar</span></a>
        </div>
    </div>
</div>
<script>
    var idreq =<%=request.getParameter("id")%>
    $(document).ready(function () {
        CargaTableDetHist();
        getTotales();
    });
    function CargaTableDetHist() {
        var tabla = $('#TBLDETALLEORDEN');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            ajax: {
                method: "post",
                url: "requerimiento?action=getDetHist",
                dataSrc: "data"
            },
            columns: [
                //{data: "indice"},
                {data: "cantidad"},
                {data: "descripcion"},
                {data: "precio"},
                {data: "subtotal"}
            ]
        });
    }
    function getTotales() {
        $.ajax({
            type: "POST",
            url: "requerimiento?action=getTotales",
            success: function (response)
            {
                var json = JSON.parse(response);
                var data = json.data;
                $.each(data, function (i, item) {
                    $("#nomprov").val(data[i].nombreprov);
                    $("#ruc").val(data[i].ruc);
                    $("#fecha").val(data[i].fecha);
                    $("#correlativo").val(data[i].correlativo);
                    $("#idst").text(data[i].subtotal);
                    $("#idigv").text(data[i].igv);
                    $("#idneto").text(data[i].neto);
                });
            }, error: function (response) {
                console.log(response);
            }
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
