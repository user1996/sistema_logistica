<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    ArrayList<beannotificacion> lstnotiff = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataNotifList");
%>
<style>
    th { font-size: 12px; }
    td { font-size: 12px; }
</style>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Requerimiento</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header">Historial de Requerimientos</div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table id="TBLREQ" class="table table-hover" style="width:100%">
                        <thead>
                        <th>NroRequerimiento</th>
                        <th>Sede</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Persona</th>
                        <th>Cant</th>
                        <th>Area</th>
                        <th></th>
                        </thead>
                        <tbody>
                            <%if (lstnotiff != null) {%>
                            <%for (beannotificacion b : lstnotiff) {%>
                            <tr>
                                <td><strong><%=b.getCorrelativo()%></strong></td>
                                <td><%=b.getNombreSede()%></td>
                                <td><%=b.getFecha()%></td>
                                <td><%=b.getHora()%></td>
                                <td><%=b.getDatos()%></td>
                                <td><%=b.getCant()%></td>
                                <td><%=b.getArea()%></td>
                                <th><a href="notif?action=getNotif&idreq=<%=b.getIdrequerimiento()%>"><span class="btn btn-info btn-sm"><i class="fas fa-search"></i></span></a></th>
                            </tr>
                            <%}%><%}%>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>     
<%@include file="partes/footer.jsp" %>
