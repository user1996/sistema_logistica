<%@page import="bean.beanproveedor"%>
<%@page import="negocio.proveedor_neg"%>
<%@page import="negocio.persona_neg"%>
<%@page import="com.google.gson.JsonObject"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    persona_neg per = new persona_neg();
    JsonObject jsonsede = per.getListaSede();
    JsonObject jsonarea = per.getListaArea();
%>
<style>
    th { font-size: 12px; }
    td { font-size: 11px; }
</style>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Orden de Servicio</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="card border-primary">
        <form id="frmservicio">
            <div class="card-header bg-primary text-white">Registro Orden de Servicio</div>
            <div class="card-body">
                <div class="row">
                    <div class="alert-info col-md-12 mb-3">Aviso</div>
                    <div class="col-md-6 mb-3">
                        <label>Sala</label>
                        <select name="idsede" id="idsede" class="form-control form-control-sm"></select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Responsable</label>
                        <input type="text" id="idresponsable" name="idresponsable" value="<%=idusuario%>" readonly="" style="display: none">
                        <input type="text" value="<%=datos%>" class="form-control form-control form-control-sm">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label>Proveedor</label>
                        <input type="text" name="ruc" id="ruc" class="form-control form-control-sm">
                        <input type="text" name="idproveedor" id="idproveedor" class="form-control form-control-sm" style="display: none">
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label>Cantidad</label>
                        <input type="number" name="cantidad" id="cantidad" class="form-control form-control form-control-sm">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label>Precio</label>
                        <input type="number" name="precio" id="precio" class="form-control form-control form-control-sm">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label>Descripcion Servicio</label>
                        <input type="text" name="descripcion" id="descripcion" class="form-control form-control form-control-sm">
                    </div>
                </div>
                <button type="button" class="btn btn-secondary" onclick="addItem()">A�adir Item</button>
            </div>
        </form>
    </div>
    <div class="card border-info">
        <div class="card-header bg-info text-white">Registro Servicio</div>
        <div class="card-body">
            <div class="table table-responsive">
                <table id="TBLSERVICIO" class="table table-hover">
                    <thead>
                    <th>Cant.</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Total</th>
                    <th>Accion</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-secondary btn-sm" onclick="RegistraServicio()">Registrar</button>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        cargacomboSede();
        $("#ruc").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "cotizacion?action=filtroProv",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    focus: function (event, ui) {
                        event.preventDefault();
                        alert();
                        $(this).val(ui.item.ruc + "-----");
                    },
                    success: function (data) {
                        if (data.data.length == 0) {
                            $('#idproveedor').val(0);
                        }
                        response($.map(data.data, function (item) {
                            return {
                                value: item.concat,
                                id: item.idproveedor
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                var idprov = ui.item.id;
                $('#idproveedor').val(idprov);
            }
        });
    });
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    function RegistraServicio() {
        var form = $('#frmservicio');
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: "ordenservice?action=regServicio",
            data: data,
            success: function (response)
            {
                var confirm = $.confirm({
                    title: 'Aviso',
                    content: response == "1" ? "Registrado" : response,
                    buttons: {
                        Ok: {
                            btnClass: 'btn btn-info',
                            action() {
                                if (response == "1") {
                                    window.location = "index?action=ordServ";
                                } else {
                                    return true;
                                }
                            }
                        }
                    }
                });

            }, error: function (response) {
                console.log(response);
            }
        });
    }
    function addItem() {
        var form = $('#frmservicio');
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: "ordenservice?action=addItem",
            data: data,
            success: function (response)
            {
                if (IsJsonString(response)) {
                    GeneraTabla(JSON.parse(response));
                } else {
                    var confirm = $.confirm({
                        title: 'Aviso',
                        content: response,
                        buttons: {
                            Ok: {
                                btnClass: 'btn btn-info',
                                action() {

                                }
                            }
                        }
                    });
                }
            }, error: function (response) {
                console.log(response);
            }
        });
    }
    function DeleteItem(indice) {
        $.ajax({
            type: "POST",
            url: "ordenservice?action=deleteItem",
            data: "indice=" + indice,
            success: function (response)
            {
                if (IsJsonString(response)) {
                    GeneraTabla(JSON.parse(response));
                } else {
                    alert(response);
                }
            }, error: function (response) {
                console.log(response);
            }
        });
    }
    function GeneraTabla(json) {
        $('#TBLSERVICIO').dataTable().fnDestroy();
        $('#TBLSERVICIO').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            data: json.data,
            columns: [
                //{data: "indice"},
                {data: "cant"},
                {data: "descripcion"},
                {data: "precio"},
                {data: "subtotal"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.indice;
                        return '<button class="btn btn-secondary btn-sm" onclick="DeleteItem(' + indice + ')">Eliminar</button>'
                    }
                }
            ]
        });
        $("#cantidad").val('');
        $("#precio").val('');
        $("#descripcion").val('');
        $("#cantidad").focus();
    }
    function cargacomboSede() {
        var json = <%=jsonsede%>
        $("#idsede").append('<option value=0>SELECCIONE...</option>');
        $.each(json.data, function (key, registro) {
            $("#idsede").append('<option value=' + registro.idsede + '>' + registro.nombresede + '</option>');
        });
        $("#idsede").val(<%=idsede%>);
    }
</script>
<%@include file="partes/footer.jsp" %>

