            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright � 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
<!-- Bootstrap JS-->
        <script src="<%=patch%>/assets/jquery/popper.min.js"></script>
        <script src="<%=patch%>/assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- Vendor JS       -->
        <script src="<%=patch%>/assets/estiloExterno/vendor/slick/slick.min.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/wow/wow.min.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/animsition/animsition.min.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/circle-progress/circle-progress.min.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="<%=patch%>/assets/estiloExterno/vendor/select2/select2.min.js">
        </script>
        <!-- Main JS-->
        <script src="<%=patch%>/assets/estiloExterno/js/main.js"></script>
    </body>
</html>