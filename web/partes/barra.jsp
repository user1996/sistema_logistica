<%@page import="bean.beanalerta"%>
<%@page import="bean.beanalarma"%>
<%@page import="java.util.List"%>
<%@page import="negocio.alarma_neg"%>
<%@page import="bean.beannotificacion"%>
<%@page import="bean.beanpersona"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<beanpersona> listaSesion = (ArrayList<beanpersona>) request.getSession().getAttribute("dataSesion");
    boolean booleanoAdm = false, booleanSup = false, booleanAlm = false, booleanOtro = false, checkNotif = false;
    int idusuario = 0, idsede = 0, idarea = 0, contador = 0;
    String datos = "", nombretipo = "", nombresede = "", sexo = "", area = "";
    if (listaSesion != null) {
        datos = listaSesion.get(0).getDatos();
        idusuario = listaSesion.get(0).getIdusuario();
        nombretipo = listaSesion.get(0).getNombretipo();
        idsede = listaSesion.get(0).getIdsede();
        nombresede = listaSesion.get(0).getNombresede();
        sexo = listaSesion.get(0).getGenero();
        idarea = listaSesion.get(0).getIdarea();
        area = listaSesion.get(0).getNombrearea();
        ArrayList<beannotificacion> lstNotif = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataNotif");
        checkNotif = lstNotif != null && !lstNotif.isEmpty();
        if (nombretipo.equals("ADMINISTRADOR")) {
            booleanoAdm = true;
        } else {
            switch (nombretipo) {
                case "ENCARGADO ALMACEN":
                    booleanAlm = true;
                    break;
                case "SUPERVISOR":
                    booleanSup = true;
                    break;
                default:
                    booleanOtro = true;
                    break;
            }
        }
        alarma_neg al_neg;
%>
<style>
    th { font-size: 12px; }
    td { font-size: 10px; }
</style>
<body class="animsition">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar2">
            <div class="logo">
                <a href="index?action=index">
                    <img src="<%=patch%>/assets/estiloExterno/Nueva carpeta/logo-blue.png">
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar1">
                <div class="account2">
                    <div class="image img-cir img-120">
                        <img src="assets/estiloExterno/Nueva carpeta/icono<%=sexo.equals("M") ? "masculino" : "femenino"%>.png" alt="<%=listaSesion.get(0).getDatos()%>" />
                    </div>
                    <center><h6 class="name"><%=datos%></h6></center>
                    <center><small><strong>Tipo usuario :</strong> <%=nombretipo%></small></center>
                    <center><small><strong>Sede :</strong> <%=nombresede%></small></center>
                    <center><small><strong>Area :</strong> <%=area%></small></center>
                </div>
                <nav class="navbar-sidebar2">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a href="#"><i class="fas fa-tachometer-alt"></i> Dashboard<span class="arrow"><i class="fas fa-angle-down"></i></span>
                            </a>
                        </li>
                    </ul><%if (booleanoAdm) {%>
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a href="#" class="js-arrow"><i class="fas fa-users"></i> Usuarios<span class="arrow"><i class="fas fa-angle-down"></i></span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="dropdown-item">
                                    <a href="index?action=persona"><i class="fas fa-users"></i>Usuarios</a>
                                </li>
                            </ul>
                        </li>
                    </ul><%}%>
                    <%if (booleanoAdm || booleanAlm || booleanSup) { %>
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a href="#" class="js-arrow"><i class="fas fa-clipboard-list"></i> Ordenes<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                    <%if (booleanoAdm || booleanSup) { %>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="dropdown-item has-sub">
                                    <a class="js-arrow" href="#"><i class="fas fa-money-check"></i><small>Orden de Servicio</small><span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                    <ul class="list-unstyled navbar__list js-sub-list">
                                        <li class="dropdown-item">
                                            <a href="index?action=ordServ"><i class="fas fa-eye"></i><small>Ver Ordenes</small></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul><%}%>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="dropdown-item has-sub">
                                    <a class="js-arrow" href="#"><i class="fas fa-shopping-cart"></i><small>Orden de Compra</small><span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                    <ul class="list-unstyled navbar__list js-sub-list">
                                        <%if (booleanoAdm || booleanAlm) {%>
                                        <% if (booleanoAdm) {%>
                                        <li class="dropdown-item">
                                            <a href="index?action=insumo"><i class="fas fa-boxes"></i> <small>Insumos</small></a>
                                        </li><%}%><% if (booleanoAdm) {%>
                                        <li class="dropdown-item">
                                            <a href="index?action=prov"><i class="fas fa-shipping-fast"></i> <small>Proveedor</small></a>
                                        </li><%}%><% if (booleanoAdm) {%>
                                        <li class="dropdown-item">
                                            <a href="index?action=cotiz"><i class="fas fa-tags"></i> <small>Cotizacion</small></a>
                                        </li><%}%><% if (booleanoAdm || booleanAlm) {%>
                                        <li class="dropdown-item">
                                            <a href="index?action=orden"><i class="fas fa-shopping-basket"></i> <small>Generar Orden</small></a>
                                        </li><%}%><% if (booleanoAdm || booleanAlm) {%>
                                        <li class="dropdown-item">
                                            <a href="orden?action=listOrdenH"><i class="fas fa-list-alt"></i> <small>Consultar Ordenes</small></a>
                                        </li><%}
                                            }%>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul><%}%>
                    <% if (booleanOtro || booleanoAdm || booleanSup) {%>
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a href="#" class="js-arrow"><i class="fas fa-list"></i> Requerimiento<span class="arrow"><i class="fas fa-angle-down"></i></span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="dropdown-item">
                                    <a href="index?action=req"><i class="fas fa-address-book"></i><small>Ingreso</small></a>
                                </li>
                            </ul>
                            <%if (booleanoAdm) {%>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="dropdown-item">
                                    <a href="index?action=reqGen"><i class="fas fa-shopping-cart"></i><small>Generar Compra</small></a>
                                </li>
                            </ul>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="dropdown-item">
                                    <a href="index?action=listReq"><i class="fas fa-search"></i><small>Consulta Requerimientos</small></a>
                                </li>
                            </ul><%}%>
                        </li>
                    </ul><%}%>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container2">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop2">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap2">
                            <div class="logo d-block d-lg-none">
                                <a href="index?action=index">
                                    <img src="<%=patch%>/assets/estiloExterno/Nueva carpeta/logo-blue.png">
                                </a>
                            </div>
                            <div class="header-button2">
                                <div class="header-button-item js-item-menu">
                                    <i class="zmdi zmdi-search"></i>
                                    <div class="search-dropdown js-dropdown">
                                        <form action="">
                                            <input class="au-input au-input--full au-input--h65" type="text" placeholder="Search for datas &amp; reports..." />
                                            <span class="search-dropdown__icon">
                                                <i class="zmdi zmdi-search"></i>
                                            </span>
                                        </form>
                                    </div>
                                </div>
                                <div class="header-button-item <%=checkNotif == true ? "has-noti" : ""%> js-item-menu">
                                    <i class="zmdi zmdi-notifications"></i>
                                    <div class="notifi-dropdown js-dropdown" style="width:100px; height:400px; overflow: scroll;">
                                        <div class="notifi__title">
                                            <p><%=checkNotif == true ? "Tienes " + lstNotif.size() + " notificaciones" : "No tienes notificaciones"%></p>
                                        </div>
                                        <%if (checkNotif) {%>
                                        <div style="overflow: auto">
                                            <%for (beannotificacion bno : lstNotif) {%>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="fas fa-exclamation"></i>
                                                </div>
                                                <div class="content">
                                                    <a href="notif?action=getNotif&idreq=<%=bno.getIdrequerimiento()%>"><p><small><%=bno.getMsjnotificacion()%></small></p></a>
                                                    <span class="date"><%=bno.getFecha() + " " + bno.getHora()%></span>
                                                </div>
                                            </div>
                                            <%}%>
                                        </div><%}%>
                                        <div class="notifi__footer">
                                            <a href="notif?action=getNotifM">ver Todo</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="header-button-item mr-0 js-sidebar-btn">
                                    <i class="zmdi zmdi-menu"></i>
                                </div>
                                <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                    <%if (booleanoAdm || booleanSup) {%>
                                    <div class="account-dropdown__item">
                                        <a href="index?action=alarm" onclick="">
                                            <i class="fas fa-bell"></i>Alertas Frecuentes
                                        </a>
                                    </div><%}%>
                                    <div class="account-dropdown__item">
                                        <a href="logout">
                                            <i class="zmdi zmdi-account"></i>Cerrar Sesion
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
                <div class="logo">
                    <a href="#">
                        <img src="images/icon/logo-white.png" alt="Cool Admin" />
                    </a>
                </div>
                <div class="menu-sidebar2__content js-scrollbar2">
                    <div class="account2">
                        <div class="image img-cir img-120">
                            <img src="assets/estiloExterno/Nueva carpeta/icono<%=sexo.equals("M") ? "masculino" : "femenino"%>.png" alt="<%=listaSesion.get(0).getDatos()%>" />
                        </div>
                        <center><span><a href="logout"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a></span></center><br>
                        <center><h6 class="name"><%=datos%></h6></center>
                        <center><small><strong>Tipo usuario :</strong> <%=nombretipo%></small></center>
                        <center><small><strong>Sede :</strong> <%=nombresede%></small></center>
                        <center><small><strong>Area :</strong> <%=area%></small></center>
                    </div>
                    <nav class="navbar-sidebar2">
                        <ul class="list-unstyled navbar__list">
                            <li class="active has-sub">
                                <a href="#"><i class="fas fa-tachometer-alt"></i> Dashboard<span class="arrow"><i class="fas fa-angle-down"></i></span>
                                </a>
                            </li>
                        </ul><%if (booleanoAdm) {%>
                        <ul class="list-unstyled navbar__list">
                            <li class="active has-sub">
                                <a href="#" class="js-arrow"><i class="fas fa-users"></i> Usuarios<span class="arrow"><i class="fas fa-angle-down"></i></span>
                                </a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li class="dropdown-item">
                                        <a href="index?action=persona"><i class="fas fa-users"></i>Usuarios</a>
                                    </li>
                                </ul>
                            </li>
                        </ul><%}%>
                        <%if (booleanoAdm || booleanAlm || booleanSup) { %>
                        <ul class="list-unstyled navbar__list">
                            <li class="active has-sub">
                                <a href="#" class="js-arrow"><i class="fas fa-clipboard-list"></i> Ordenes<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                        <%if (booleanoAdm || booleanSup) { %>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li class="dropdown-item has-sub">
                                        <a class="js-arrow" href="#"><i class="fas fa-money-check"></i><small>Orden de Servicio</small><span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                        <ul class="list-unstyled navbar__list js-sub-list">
                                            <li class="dropdown-item">
                                                <a href="index?action=ordServ"><i class="fas fa-eye"></i><small>Ver Ordenes</small></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul><%}%>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li class="dropdown-item has-sub">
                                        <a class="js-arrow" href="#"><i class="fas fa-shopping-cart"></i><small>Orden de Compra</small><span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                        <ul class="list-unstyled navbar__list js-sub-list">
                                            <%if (booleanoAdm || booleanAlm) {%>
                                            <% if (booleanoAdm) {%>
                                            <li class="dropdown-item">
                                                <a href="index?action=insumo"><i class="fas fa-boxes"></i> <small>Insumos</small></a>
                                            </li><%}%><% if (booleanoAdm) {%>
                                            <li class="dropdown-item">
                                                <a href="index?action=prov"><i class="fas fa-shipping-fast"></i> <small>Proveedor</small></a>
                                            </li><%}%><% if (booleanoAdm) {%>
                                            <li class="dropdown-item">
                                                <a href="index?action=cotiz"><i class="fas fa-tags"></i> <small>Cotizacion</small></a>
                                            </li><%}%><% if (booleanoAdm || booleanAlm) {%>
                                            <li class="dropdown-item">
                                                <a href="index?action=orden"><i class="fas fa-shopping-basket"></i> <small>Generar Orden</small></a>
                                            </li><%}%><% if (booleanoAdm || booleanAlm) {%>
                                            <li class="dropdown-item">
                                                <a href="orden?action=listOrdenH"><i class="fas fa-list-alt"></i> <small>Consultar Ordenes</small></a>
                                            </li><%}
                                            }%>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul><%}%>
                        <% if (booleanOtro || booleanoAdm || booleanSup) {%>
                        <ul class="list-unstyled navbar__list">
                            <li class="active has-sub">
                                <a href="#" class="js-arrow"><i class="fas fa-list"></i> Requerimiento<span class="arrow"><i class="fas fa-angle-down"></i></span>
                                </a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li class="dropdown-item">
                                        <a href="index?action=req"><i class="fas fa-address-book"></i><small>Ingreso</small></a>
                                    </li>
                                </ul>
                                <%if (booleanoAdm) {%>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li class="dropdown-item">
                                        <a href="index?action=reqGen"><i class="fas fa-shopping-cart"></i><small>Generar Compra</small></a>
                                    </li>
                                </ul>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li class="dropdown-item">
                                        <a href="index?action=listReq"><i class="fas fa-search"></i><small>Consulta Requerimientos</small></a>
                                    </li>
                                </ul><%}%>
                            </li>
                        </ul><%}%>
                    </nav>
                </div>
            </aside>
            <script>
                $(document).ready(function () {
                <% if (booleanoAdm) {%>
                    testPush1();<%}%>
                });
                function testPush1() {
                    //toastr.warning('Already signed!')           
                    //Command: toastr["info"]('Servicio de Cable - <a href="www.google.com">BEST</a> CABLE', 'Pago pendiente ')
                <%
                    al_neg = new alarma_neg();
                    List<Integer> listaId = al_neg.listIds();
                    ArrayList<beanalerta> listaDet = null;
                    for (int i = 0; i < listaId.size(); i++) {
                        listaDet = new ArrayList<>();
                        listaDet = al_neg.listDetalleAlerta(listaId.get(i));
                        for (beanalerta b : listaDet) {
                            out.print("Command: toastr[\"info\"](\"" + b.getNombretipo() + " - " + b.getRazonsocial() + "<br><button class='btn btn-light btn-sm' onClick=" + "'" + "verDetalle(" + b.getIdalerta() + ")" + "'" + ">Ver</button> \", \"Pago pendiente\");");
                        }
                    }%>
                }
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "8000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                function verDetalle(idalerta) {
                    window.location = "alarma?action=detAlarma&id=" + idalerta;
                }
            </script>
            <!-- END HEADER DESKTOP-->
            <%
                } else {
                    response.sendRedirect("index?action=login");
                }
            %>
