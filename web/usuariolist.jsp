<%@page import="negocio.persona_neg"%>
<%@page import="com.google.gson.JsonObject"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    persona_neg per = new persona_neg();
    JsonObject jsonpersona = per.getListPersonaUsuario();
    JsonObject jsonsede = per.getListaSede();
    JsonObject jsontipo = per.getListaTipo();
    JsonObject jsonarea = per.getListaArea();
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Usuarios</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header"><h3>Lista de Usuarios</h3></div>
            <div class="card-body table table-responsive">
                <table id="TBLUSUARIO" class="table table-hover" style="width:100%">
                    <thead>
                    <th>Documento</th>
                    <th>Nombre Usuario</th>
                    <th>Clave Usuario</th>
                    <th>Tipo Usuario</th>
                    <th>Sede</th>
                    <th></th>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-sm" onclick="RegistraUsuario(0)" type="button">Nuevo Usuario</button>
                <button class="btn btn-warning btn-sm" onclick="regresar()" type="button">Regresar</button>
            </div>
        </div>
    </div>
</div>
<style>
    th { font-size: 12px; }
    td { font-size: 11px; }
</style>
<script>
    $(document).ready(function () {
        GeneraTabla();
    });
    function regresar(){
        window.location="index?action=persona";
    }
    function GeneraTabla() {
        $('#TBLUSUARIO').dataTable().fnDestroy();
        $('#TBLUSUARIO').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            ajax: {
                method: "post",
                url: "persona?action=getlistausuario",
                dataSrc: "data"
            },
            columns: [
                //{data: "idusuario"},
                {data: "documento"},
                {data: "nombreusuario"},
                {data: "claveusuario"},
                {data: "rol"},
                {data: "nombresede"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var nombreusuario = data.nombreusuario, claveusuario = data.claveusuario;
                        return '<button class="btn btn-secondary btn-sm" onclick="RegistraUsuario(' + data.idusuario + ',' + data.idpersona + ',' + data.idtipo + ',' + data.idsede + ',' + "'" + '' + nombreusuario + '' + "'" + ',' + "'" + '' + claveusuario + '' + "'" + ','+data.idarea+')">Modificar</button>'
                    }
                }
            ]
        });
    }
    function RegistraUsuario(idUsuario, idpersona, idtipo, idsede, nombreusuario, claveusuario,idarea) {
        var style = "";
        var confirm = $.confirm({
            title: '',
            columnClass: 'col-md-7 col-md-offset-12 col-xs-4 col-xs-offset-8',
            content: '<div class="card border-primary" style="display: ' + style + '">' +
                    '<div class="card-header alert alert-primary"><h5>Nuevo Usuario</h5></div>' +
                    '<div class="card-body alert alert-primary">' +
                    '<form id="frmusuario" action="persona?action=registrausuario" method="post">' +
                    '<div class="row">' +
                    '<div class="col-md-6 mb-3">' +
                    '<label>Nombre Usuario</label>' +
                    '<input name="idusuario" value=' + idUsuario + ' type="text" style="display : none">' +
                    '<input type="text" id="nombreusuario" name="nombreusuario" class="form-control form-control-sm" value="' + (nombreusuario == undefined ? "" : nombreusuario) + '">' +
                    '</div>' +
                    '<div class="col-md-6 mb-3">' +
                    '<label>Clave Usuario</label>' +
                    '<input type="password" id="claveusuario" name="claveusuario" class="form-control form-control-sm" value="' + (claveusuario == undefined ? "" : claveusuario) + '">' +
                    '</div>' +
                    '<div class="col-md-6 mb-3">' +
                    '<label>Persona</label>' +
                    '<select id="idpersona" name="idpersona" class="form-control"></select>' +
                    '</div>' +
                    '<div class="col-md-6 mb-3">' +
                    '<label>Rol</label>' +
                    '<select id="idtipo" name="idtipo" class="form-control"></select>' +
                    '</div>' +
                    '<div class="col-md-6 mb-3">' +
                    '<label>Sede</label>' +
                    '<select id="idsede" name="idsede" class="form-control"></select>' +
                    '</div>' +
                    '<div class="col-md-6 mb-3">' +
                    '<label>Area</label>' +
                    '<select id="idarea" name="idarea" class="form-control"></select>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>' +
                    '</div>',
            buttons: {
                Registrar: {
                    btnClass: 'btn btn-success',
                    action() {
                        guardaPersona(confirm);
                        return false;
                    }
                }, Cancelar: {
                    btnClass: 'btn btn-danger',
                    action() {

                    }
                }
            },
            onContentReady: function () {
                cargaComboPersona();
                cargaComboRol();
                cargaComboSede();
                cargacomboArea();
                $('#idpersona').val(idpersona == undefined ? 0 : idpersona);
                $('#idtipo').val(idtipo == undefined ? 0 : idtipo);
                $('#idsede').val(idsede == undefined ? 0 : idsede);
                $('#idarea').val(idarea == undefined ? 0 : idarea);
            }
        });
    }
    function guardaPersona(confirm) {
        var formulario = $('#frmusuario');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response)
            {
                var txt = "";
                if (response > 0) {
                    GeneraTabla();
                    confirm.close();
                    txt = "Cambios Guardados";
                } else {
                    txt = response;
                }
                $.alert({
                    title: '<i class="fas fa-info"></i> Aviso',
                    content: txt
                });
            }, error: function (response) {
                alert(response);
            }
        });
    }
    function cargaComboPersona() {
        var json = <%=jsonpersona%>
        $("#idpersona").append('<option value=0>Seleccione...</option>');
        $.each(json.data, function (key, registro) {
            $("#idpersona").append('<option value=' + registro.idpersona + '>' + registro.datos + '</option>');
        });
    }
    function cargaComboRol() {
        var json = <%=jsontipo%>
        $("#idtipo").append('<option value=0>Seleccione...</option>');
        $.each(json.data, function (key, registro) {
            $("#idtipo").append('<option value=' + registro.idtipo + '>' + registro.rol + '</option>');
        });
    }
    function cargaComboSede() {
        var json = <%=jsonsede%>
        $("#idsede").append('<option value=0>Seleccione...</option>');
        $.each(json.data, function (key, registro) {
            $("#idsede").append('<option value=' + registro.idsede + '>' + registro.nombresede + '</option>');
        });
    }
    function cargacomboArea() {
        var json = <%=jsonarea%>
        $("#idarea").append('<option value=0>Seleccione...</option>');
        $.each(json.data, function (key, registro) {
            $("#idarea").append('<option value=' + registro.idarea + '>' + registro.nombrearea + '</option>');
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
