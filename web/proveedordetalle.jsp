<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Proveedor</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        <style>
            table{
                width:100%;
            }
            th { font-size: 13px; }
            td { font-size: 13px; }
        </style>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card border-success mb-3">
                        <div class="card-header"><h3>Datos Proveedor</h3></div>
                        <div class="card-body">
                            <form id="frmproveedor" action="proveedor?action=regProv" method="post">
                                <div class="row">
                                    <input type="text" id="idproveedor" name="idproveedor" class="form-control form-control-sm" style="visibility: hidden">  
                                    <div class="col-md-4 mb-3">
                                        <label>RUC</label>
                                        <input type="text" id="ruc" name="ruc" class="form-control form-control-sm">  
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label>RAZON SOCIAL</label>
                                        <input type="text" id="razonsocial" name="razonsocial" class="form-control form-control-sm">  
                                    </div>
                                    <div class="col-md-9 mb-3">
                                        <label>DIRECCION</label>
                                        <textarea type="text" id="direccion" name="direccion" class="form-control form-control-sm"></textarea>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label>TIPO PRODUCTO</label>
                                        <select id="idtipo" name="idtipo" name="idtipo" class="form-control form-control-sm"></select>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary" onclick="GuardaProveedor()">Guardar Proveedor</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-dark" onclick="Retornar()">Regresar</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-info mb-3">
                        <div class="card-header"><h4>LISTA DE TELEFONOS</h4></div>
                        <div class="card-body table table-responsive">
                            <table id="TABLATELF" class="table table-hover" style="width:100%;text-align: center">
                                <thead>
                                <th>ID</th>
                                <th>NUMERO TELEFONICO</th>
                                </thead>
                                <tbody>   
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer"><button class="btn btn-primary btn-sm" onclick="NuevoTelefono()">Nuevo</button></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-info mb-3">
                        <div class="card-header"><h4>LISTA DE CORREOS</h4></div>
                        <div class="card-body table table-responsive">
                            <table id="TABLACORREO" class="table table-hover" style="width:100%;text-align: center">
                                <thead>
                                <th>ID</th>
                                <th>DIRECCION DE CORREO ELECTRONICO</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer"><button class="btn btn-primary btn-sm" onclick="NuevoCorreo()">Nuevo</button></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        var id =<%=request.getParameter("id")%>
        $(document).ready(function () {
            cargaCombo();
            cargaTablaTelefono(id);
            cargaTablaCorreo(id);
            getProveedor(id);
        });
        function Retornar() {
            window.location = "proveedor?action=index";
        }
        function NuevoTelefono() {
            $.confirm({
                title: 'Nuevo Telefono',
                type: 'green',
                content: '' +
                        '<form id="frmtelefono" action="telefono?action=regTelf" class="formName">' +
                        '<div class="form-group">' +
                        '<input type="number" name="numerotelefono" placeholder="Nro Telefonico" class="name form-control" required />' +
                        '</div>' +
                        '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Registrar',
                        btnClass: 'btn-blue',
                        action: function () {
                            var name = this.$content.find('.name').val();
                            if (!name && id != 0) {
                                $.alert('Ingrese Nro Telefonico!!');
                                return false;
                            } else {
                                var formu = $("#frmtelefono");
                                var url = formu.attr('action');
                                var data = formu.serialize() + "&idProveedor=" + id;
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (response)
                                    {
                                        if (response == "1") {
                                            cargaTablaTelefono(id);
                                        }
                                    }, error: function (response) {
                                        alert(response);
                                    }
                                });
                            }

                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
            });
        }
        function NuevoCorreo() {
            var alerta = $.confirm({
                closeIcon: true, // explicitly show the close icon
                title: 'Nuevo Correo',
                type: 'green',
                content: '<form id="frmcorreo" action="correo?action=regCorreo" class="formName">' +
                        '<div class="form-group newsletter-signup">' +
                        '<input type="text" id="correoelectronico" name="correoelectronico" placeholder="Correo Electronico" class="name form-control" required />' +
                        '</div>' +
                        '<div id="valid"></div>' +
                        '</form>',
                buttons: {
                    buttonA: {
                        text: 'Registrar',
                        action: function (buttonA) {
                            var formu = $("#frmcorreo");
                            var url = formu.attr('action');
                            var data = formu.serialize() + "&idProveedor=" + id;
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (response)
                                {
                                    if (response == "1") {
                                        cargaTablaCorreo(id);
                                    } else {
                                        $.alert(response);
                                    }
                                }, error: function (response) {
                                    alert(response);
                                }
                            });
                        }
                    },
                    buttonB: {
                        text: 'Cancelar',
                    }
                },
                onContentReady: function () {
                    $('.newsletter-signup input:first').on('keyup', function () {
                        var valid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value) && this.value.length;
                        $('#valid').html('Correo Electronico ' + (valid ? '' : ' no') + ' valido').css("color", (valid ? 'green' : 'red'));;
                        if (valid) {
                            this === alerta.buttons.buttonA.enable();
                        } else {
                            this === alerta.buttons.buttonA.disable();
                        }
                    });
                }
            });
        }
        function GuardaProveedor() {
            var formulario = $("#frmproveedor");
            var data = formulario.serialize();
            var url = formulario.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response)
                {
                    if (response > 0) {
                        window.location = "proveedor?action=detalle&id=" + response;
                    } else {
                        $.alert({
                            title: 'Aviso !',
                            type:'red',
                            content: response,
                        });
                    }

                }, error: function (response) {
                    alert(response);
                }
            });
        }
        function getProveedor(id) {
            $.ajax({
                type: "POST",
                url: "proveedor?action=getProv&id=" + id,
                success: function (response)
                {
                    var json = JSON.parse(response);
                    if (json.data.length > 0) {
                        $.each(json.data, function (key, registro) {
                            $('#idproveedor').val(registro.idproveedor);
                            $('#ruc').val(registro.ruc);
                            $('#razonsocial').val(registro.razonSocial);
                            $('#direccion').val(registro.direccion);
                            $('#idtipo').val(registro.idTipo);
                        });
                    } else {
                        $('#idproveedor').val(0);
                    }
                }
            });
        }
        function cargaCombo() {
            $.ajax({
                type: "POST",
                url: "tipoprod?action=list",
                success: function (response)
                {
                    var json = JSON.parse(response);
                    $("#idtipo").append('<option value=0>Seleccione...</option>');
                    $.each(json.data, function (key, registro) {
                        $("#idtipo").append('<option value=' + registro.idTipo + '>' + registro.descripcion + '</option>');
                    });
                }
            });
        }
        function cargaTablaTelefono(id) {
            var tabla = $('#TABLATELF');
            tabla.dataTable().fnDestroy();
            tabla.DataTable({
                "paging": false,
                "ordering": false,
                "info": true,
                ajax: {
                    method: "post",
                    url: "telefono?action=listTelf&id=" + id,
                    dataSrc: "data"
                },
                columns: [
                    {data: "idTelefono"},
                    {data: "nroTelefono"}
                ]

            });
        }
        function cargaTablaCorreo(id) {
            var tabla = $('#TABLACORREO');
            tabla.dataTable().fnDestroy();
            tabla.DataTable({
                "paging": false,
                "ordering": false,
                "info": true,
                ajax: {
                    method: "post",
                    url: "correo?action=listcorreo&id=" + id,
                    dataSrc: "data"
                },
                columns: [
                    {data: "idCorreo"},
                    {data: "correo"}
                ]

            });
        }
    </script>
    <%@include file="partes/footer.jsp" %>
