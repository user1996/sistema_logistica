<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Usuarios</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%    Object[] arreglo = (Object[]) request.getSession().getAttribute("datos");
    String id = "", documento = "", primernombre = "", segundonombre = "", apellidopaterno = "", apellidomaterno = "", genero = "", telefono = "";
    if (arreglo != null) {
        id = arreglo[0] != null ? arreglo[0].toString() : "";
        documento = arreglo[1] != null ? arreglo[1].toString() : "";
        primernombre = arreglo[2] != null ? arreglo[2].toString() : "";
        segundonombre = arreglo[3] != null ? arreglo[3].toString() : "";
        apellidopaterno = arreglo[4] != null ? arreglo[4].toString() : "";
        apellidomaterno = arreglo[5] != null ? arreglo[5].toString() : "";
        genero = arreglo[6] != null ? arreglo[6].toString() : "";
        telefono = arreglo[7] != null ? arreglo[7].toString() : "";
    } else {
        id = "0";
    }
%>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form id="frmpersona" action="persona?action=registra" method="post">
                <div class="card border-success mb-3">
                    <div class="card-header"><h3>Datos Persona</h3></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label>Documento</label>
                                <input type="text" id="documento" name="documento" value="<%=documento%>" class="form-control form-control-sm">  
                                <input type="text" id="idpersona" name="idpersona" value="<%=id%>" style="display: none">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Primer Nombre</label>
                                <input type="text" id="primernombre" name="primernombre" value="<%=primernombre%>" class="form-control form-control-sm">  
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Segundo Nombre</label>
                                <input type="text" id="primernombre" name="segundonombre" value="<%=segundonombre%>" class="form-control form-control-sm">  
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Apellido Paterno</label>
                                <input type="text" id="apellidopaterno" name="apellidopaterno" value="<%=apellidopaterno%>" class="form-control form-control-sm">  
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Apellido Materno</label>
                                <input type="text" id="apellidomaterno" name="apellidomaterno" value="<%=apellidomaterno%>" class="form-control form-control-sm">  
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Telefono</label>
                                <input type="text" id="telefono" name="telefono" value="<%=telefono%>" class="form-control form-control-sm">  
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Genero</label><br>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" id="rbgenero" name="rbgenero" <%=genero.equals("M") ? "checked" : ""%> value="M">Masculino
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" id="rbgenero" name="rbgenero" <%=genero.equals("F") ? "checked" : ""%> value="F">Femenino
                                    </label>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="col-md-12 col-xl-12">
                            <button type="button" class="btn btn-success" onclick="guardaPersona()"><%=id.equals("0") ? "Registrar" : "Modificar"%></button>
                            <a href="index?action=persona"><span class="btn btn-danger">Cancelar</span></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

    });
    function guardaPersona() {
        var formulario = $('#frmpersona');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response)
            {
                $.confirm({
                    title: '<i class="fas fa-info"></i> Aviso',
                    content: response > 0 ? "Cambios Guardados" : response,
                    buttons:{
                        Ok:{
                            btnClass:'btn-blue',
                            action: function(){
                                window.location="index?action=persona";
                            }
                        }
                    }

                });

            }, error: function (response) {
                alert(response);
            }
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
