<%@page import="java.text.DecimalFormat"%>
<%@page import="bean.beandetalleordencompra"%>
<%@page import="java.util.ArrayList"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    ArrayList<beandetalleordencompra> listaDetalle = (ArrayList<beandetalleordencompra>) request.getSession().getAttribute("listaDetalle");
    if (listaDetalle != null) {
        int cont = 0;
        if (!listaDetalle.isEmpty() || listaDetalle != null) {
            String ref1 = "", btntext = "", lockBtn = "";
            for (beandetalleordencompra bd : listaDetalle) {
                if (bd.getIdProveedor() == 0 || bd.getIdInsumo() == 0) {
                    cont++;
                }
            }
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Consultar Orden</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 12px; }
    td { font-size: 11px; }
    .ui-autocomplete {
        position: absolute;
        z-index: 2150000000 !important;
        cursor: default;
        border: 2px solid #ccc;
        padding: 5px 0;
        border-radius: 2px;
    }
</style>
<div class="container">
    <div class="col-md-12">
        <div class="card border-primary mb-3">
            <div class="card-header bg-primary"><h4 class="text-white"><i class="fas fa-clipboard"></i> Listado</h4></div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table id="TBLORDENES" class="table table-hover">
                        <thead>
                        <th style="display: block" >Id</th>
                        <th>Codigo</th>
                        <th>Descripcion</th>
                        <th>Unidad</th>
                        <th>Cant</th>
                        <th>Prov Preferido</th>
                        <th>Precio</th>
                        <th>Accion</th>
                        </thead>
                        <tbody>
                            <%
                                boolean valor = false;
                                for (beandetalleordencompra bdet : listaDetalle) {
                                    valor = bdet.getIdInsumo() > 0 && bdet.getIdProveedor() > 0;
                            %>
                            <tr class="<%=valor ? "" : "alert alert-danger"%>">
                                <td><strong><%=bdet.getIndice() + 1%></strong></td>
                                <td><%=bdet.getCodigoInsumo()%></td>
                                <td><%=bdet.getDescripcion()%></td>
                                <td><%=bdet.getUnidadInsumo()%></td>
                                <td><%=bdet.getCantPedido()%></td>
                                <td><%=bdet.getRazonSocial()%></td>
                                <td><%=bdet.getPrecio()%></td>
                                <td>
                                    <%if (!valor) {%>
                                    <button type="button" class="btn btn-primary btn-sm" onclick="return RegistraInsumoNuevo('<%=bdet.getIdInsumo()%>', '<%=bdet.getIdProveedor()%>', '<%=bdet.getCodigoInsumo()%>', '<%=bdet.getDescripcion()%>')">Arreglar</button>
                                    <%}%> 
                                </td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success" type="button" onclick="RegistrarOrden()" <%=contador > 0 ? "disabled" : ""%>>Registrar Orden de Compra</button>
            </div>
        </div>
    </div>
</div>

<script>
    var confirm;
    var style = "";
    $(document).ready(function () {
        //cargaTablaOrden();
        //RegistraInsumoNuevo();
    });
    function RegistrarOrden() {
        $.ajax({
            type: "POST",
            url: "orden?action=saveOrden",
            success: function (response)
            {
                $.confirm({
                    title: 'Aviso !',
                    content: response,
                    buttons: {
                        confirm: function () {
                            window.location = "orden?action=listOrdenH";
                        },
                    }
                });
            }
        });
    }
    function cargaTablaOrden() {
        var tabla = $('#TBLORDENES');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
        });
    }
    function RegistraInsumoNuevo(indInsumo, idproveedor, codigo, descripcion) {
        var style = indInsumo > 0 ? "none" : "block";
        confirm = $.confirm({
            title: '',
            escapeKey: true,
            backgroundDismiss: true,
            columnClass: 'col-md-7 col-md-offset-12 col-xs-4 col-xs-offset-8',
            content: '<div class="card border-primary" style="display: ' + style + '">' +
                    '<div class="card-header alert alert-primary"><h5>Nuevo Insumo</h5></div>' +
                    '<div class="card-body alert alert-primary">' +
                    '<form id="frminsumo" action="insumo?action=registraInsumo" method="post">' +
                    '<div class="row">' +
                    '<div class="col-md-3 mb-3">' +
                    '<label>Codigo</label>' +
                    '<input type="text" id="codigoinsumo" name="codigoinsumo" class="form-control form-control-sm" value="' + codigo + '" readOnly>' +
                    '</div>' +
                    '<div class="col-md-5 mb-3">' +
                    '<label>Descripcion</label>' +
                    '<input type="text" id="descripcioninsumo" name="descripcioninsumo" class="form-control form-control-sm" value="' + descripcion + '">' +
                    '</div>' +
                    '<div class="col-md-4 mb-3">' +
                    '<label>Unidad Medida</label>' +
                    '<input type="number" id="unidadmedida" name="unidadmedida" class="form-control form-control-sm">' +
                    '</div>' +
                    '<div class="col-md-4 mb-3">' +
                    '<label>Presentacion</label>' +
                    '<select id="idunidad" name="idunidad" class="form-control"></select>' +
                    '</div>' +
                    '<div class="col-md-4 mb-3">' +
                    '<label>Categoria</label>' +
                    '<select id="idcategoria" name="idcategoria" class="form-control"></select>' +
                    '</div>' +
                    '<div class="col-md-4 mb-3"><label>Accion</label><br><button id="btnadd" type="button" onclick="registrar()" class="btn btn-success btn-sm"><i class="fas fa-save"></i> Guarda</button></div>' +
                    '<input type="text" id="idinsumo1" name="idinsumo1" value="0" class="form-control form-control-sm" style="display: none">' +
                    '</div>' +
                    '</form>' +
                    '</div>' +
                    '</div>' +
                    '<div class="card border-success">' +
                    '<div class="card-header alert alert-success"><h5>Registre Proveedor</h5></div>' +
                    '<div class="card-body alert alert-success">' +
                    '<form id="frmcotizacion" action="cotizacion?action=regCotizacion" method="post">' +
                    '<div class="row">' +
                    '<div class="col-md-4 mb-3 ui-widget">' +
                    '<label>Ruc</label>' +
                    '<input type="text" id="ruc" name="ruc" class="form-control form-control-sm" value="">' +
                    '</div>' +
                    '<div class="col-md-8 mb-3">' +
                    '<label>Razon Social</label>' +
                    '<input id="razonsocial" name="razonsocial" type="text" class="form-control form-control-sm" value="" readonly>' +
                    '</div>' +
                    '<div class="col-md-4 mb-3">' +
                    '<label>IGV</label><br>' +
                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" id="rbigv" name="rbigv" value="1">SI' +
                    '</label>' +
                    '</div>' +
                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" id="rbigv" name="rbigv" value="0">NO' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3 mb-3"><input type="text" id="idinsumo2" name="idinsumo2" value="' + (indInsumo > 0 ? indInsumo : 0) + '" style="display : none"><input id="idproveedor" name="idproveedor" type="text" value="0" style="display : none"></div>' +
                    '<div class="col-md-3 mb-3">' +
                    '<label>Precio</label>' +
                    '<input type="number" name="precio" class="form-control form-control-sm">' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>' +
                    '</div>',
            scrollToPreviousElementAnimate: true,
            containerFluid: true,
            buttons: {
                Registrar: {
                    btnClass: 'btn btn-success',
                    keys: ['enter', 'enter'],
                    action() {
                        var form = $("#frmcotizacion");
                        var data = form.serialize();
                        $.ajax({
                            type: "POST",
                            url: "cotizacion?action=RegistraProv",
                            data: data,
                            success: function (response)
                            {
                                if (response > 0) {
                                    window.location = "orden?action=list";
                                } else {
                                    $.alert(response);
                                }
                            }, error: function (response) {
                                alert(response);
                            }
                        });
                        return false;
                    }
                },
                Cancelar: {
                    btnClass: 'btn btn-danger',
                    action() {
                        window.location = "orden?action=list";
                    }
                }
            },
            onOpen: function () {
                cargaComboUnidad();
                cargaComboCateg();
                $(function () {
                    $("#ruc").bind('keyup paste', function () {
                        this.value = this.value.replace(/[^0-9]/g, '');
                        if(this.value.length>11){
                            this.value="";
                        }
                    });
                    $("#ruc").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "cotizacion?action=filtroProv",
                                dataType: "json",
                                data: {
                                    term: request.term
                                },
                                focus: function (event, ui) {
                                    event.preventDefault();
                                    $(this).val(ui.item.ruc);
                                },
                                success: function (data) {
                                    if (data.data.length == 0) {
                                        $('#idproveedor').val(0);
                                        $("#razonsocial").prop('readonly', false);
                                    } else {
                                        $("#razonsocial").prop('readonly', true);
                                    }
                                    $("#razonsocial").val("");
                                    response($.map(data.data, function (item) {
                                        return {
                                            value: item.ruc,
                                            id: item.idproveedor
                                        }
                                    }));
                                }
                            });
                        },
                        select: function (event, ui) {
                            var idprov = ui.item.id;
                            $('#idproveedor').val(idprov);
                            $.ajax({
                                type: "POST",
                                url: "proveedor?action=getProv",
                                data: "id=" + idprov,
                                success: function (response)
                                {
                                    var json = JSON.parse(response);
                                    $.each(json.data, function (key, registro) {
                                        $("#razonsocial").val(registro.razonSocial);
                                    });
                                }
                            });
                        }
                    });

                });
            },
            onContentReady: function () {
                if (indInsumo > 0) {
                    this.buttons.Registrar.enable();
                } else {
                    this.buttons.Registrar.disable();
                }
            }
        });
    }
    function listaProveedor() {
        var alertLista = $.confirm({
            title: '',
            columnClass: '',
            content: '',
            onOpen: function () {

            }
        });
    }
    function cargaTablaProv() {}
    function registrar() {
        var formulario = $('#frminsumo');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (response, textStatus, jqXHR) {
                var valor = response > 0 ? response : "0"
                if (valor > 0) {
                    $('#idinsumo1').val(valor);
                    $('#idinsumo2').val(valor);
                    confirm.buttons.Registrar.enable();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.alert(jqXHR + "\t" + textStatus + "\t" + errorThrown);
                $('#idinsumo').val("0");
            }
        });

    }
    function cargaComboUnidad() {
        $.ajax({
            type: "POST",
            url: "unidadmedida?action=listUnidadMedida",
            success: function (response)
            {
                var json1 = JSON.parse(response);
                $("#idunidad").append('<option value=0>Seleccione...</option>');
                $.each(json1.data, function (key, registro) {
                    $("#idunidad").append('<option value=' + registro.idUnidad + '>' + registro.descripcion + '</option>');
                });
            }
        });
    }
    function cargaComboCateg() {
        $.ajax({
            type: "POST",
            url: "categoria?action=listCategoria",
            success: function (response)
            {
                var json2 = JSON.parse(response);
                $("#idcategoria").append('<option value=0>Seleccione...</option>');
                $.each(json2.data, function (key, registro) {
                    $("#idcategoria").append('<option value=' + registro.idCategoria + '>' + registro.descripcionCategoria + '</option>');
                });
            }
        });
    }

</script>                   
<%} else {
            response.sendRedirect("index?action=index");
        }
    } else {
        response.sendRedirect("index?action=index");
    }%>
<%@include file="partes/footer.jsp" %>

