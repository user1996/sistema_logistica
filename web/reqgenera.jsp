<%@page import="com.google.gson.JsonObject"%>
<%@page import="negocio.persona_neg"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    persona_neg per = new persona_neg();
    JsonObject jsonsede = per.getListaSede();
    JsonObject jsonarea = per.getListaArea();
%>
<style>
    th { font-size: 12px; }
    td { font-size: 12px; }
</style>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Genera Compra</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success">
            <div class="card-header">
                <p>Ver Requerimientos</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>Sala destinada</label>
                        <select id="selectsala" name="idsede" class="form-control form-control-sm"></select>
                    </div>
                    <div class="col-md-6">
                        <label>Area destinada</label>
                        <select id="selectarea" name="idarea" class="form-control form-control-sm"></select>
                    </div>
                    <div class="col-md-12 mb-3">
                        <br>
                        <label>Estado</label><br>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="rbestado" name="rbestado" value="0">Pendiente
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="rbestado" name="rbestado" value="1" checked="">Atendido
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="rbestado" name="rbestado" value="2">Rechazado
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card border-info">
            <div class="card-header">
                <p>Listado Requerimientos</p>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table id="TABLA" class="table table-hover">
                        <thead>
                        <th>Cantidad</th>
                        <th>Descripcion</th>
                        <th>Area</th>
                        <th>Responsable</th>
                        <th></th>
                        <th>Estado</th>
                        <th>Imagen</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-info" onclick="Genera()">Genera Compra</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        ComboArea();
        ComboSala();
        filtra($("#selectarea").val(), $("#selectsala").val(), $('input[type=radio][name=rbestado]:checked').val());
    })
    function Genera() {
        window.location = "requerimiento?action=regOrden";
    }
    $("#selectarea").change(function () {
        filtra($(this).val(), $("#selectsala").val(), $('input[type=radio][name=rbestado]:checked').val());
    });
    $("#selectsala").change(function () {
        filtra($("#selectarea").val(), $(this).val(), $('input[type=radio][name=rbestado]:checked').val());
    });
    $('input[type=radio][name=rbestado]').change(function () {
        filtra($("#selectarea").val(), $("#selectsala").val(), this.value);
    });
    function filtra(idarea, idsede, estado) {
        $.ajax({
            type: "POST",
            url: "requerimiento?action=viewFiltro",
            data: "idarea=" + idarea + "&idsede=" + idsede + "&rbestado=" + estado,
            success: function (response)
            {
                var json = JSON.parse(response);
                GenTable(json);
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    function GenTable(json) {
        $('#TABLA').dataTable().fnDestroy();
        $('#TABLA').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            "data": json.data,
            columns: [
                //{data: "idusuario"},
                {data: "cant"},
                {data: "descripcion"},
                {data: "nombreArea"},
                {data: "persona"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var estado = parseInt(data.estado);
                        var icono = estado == 0 ? "fas fa-clock" : estado == 1 ? "fas fa-thumbs-up" : "fas fa-thumbs-down";
                        var style = estado == 0 ? "warning" : estado == 1 ? "success" : "danger";
                        return '<span class="btn btn-' + style + ' btn-sm"><i class="' + icono + '"></i></span>';
                    }
                },
                {data: "estadoDesc"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var img=data.imagen;
                        return img!=null?'<img src="assets/imgs/'+img+'">':'<div class="text-center alert alert-secondary"><i class="fas fa-images fa-2x"></i></div>';
                    }
                }
                //{data: null,
                //    ClassName: 'center',
                //    mRender: function (data, type, row) {
                //        var nombreusuario = data.nombreusuario, claveusuario = data.claveusuario;
                //        return '<button class="btn btn-secondary btn-sm" onclick="RegistraUsuario(' + data.idusuario + ',' + data.idpersona + ',' + data.idtipo + ',' + data.idsede + ',' + "'" + '' + nombreusuario + '' + "'" + ',' + "'" + '' + claveusuario + '' + "'" + ',' + data.idarea + ')">Modificar</button>'
                //    }
                //}
            ]
        });
    }
    function ComboArea() {
        var json = <%=jsonarea%>
        $("#selectarea").append('<option value=0>Todas...</option>');
        $.each(json.data, function (key, registro) {
            $("#selectarea").append('<option value=' + registro.idarea + '>' + registro.nombrearea + '</option>');
        });
    }
    function ComboSala() {
        var json = <%=jsonsede%>
        $("#selectsala").append('<option value=0>Todas...</option>');
        $.each(json.data, function (key, registro) {
            $("#selectsala").append('<option value=' + registro.idsede + '>' + registro.nombresede + '</option>');
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
