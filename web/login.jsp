<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <!-- Title Page-->
        <title>Login</title>

        <!-- Fontfaces CSS-->
        <link href="assets/estiloExterno/css/font-face.css" rel="stylesheet" media="all">
        <link href="assets/fontAwesome/css/fontawesome.min.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all">
        <!-- Vendor CSS-->
        <link href="assets/estiloExterno/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="assets/estiloExterno/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">


        <!-- Main CSS-->
        <link href="assets/estiloExterno/css/theme.css" rel="stylesheet" media="all">

    </head>

    <body class="animsition">
        <div class="page-wrapper">
            <div class="page-content--bge5">
                <div class="container">
                    <div class="login-wrap">
                        <div class="login-content">
                            <div class="login-logo">
                                <a href="#">
                                    <img src="assets/estiloExterno/Nueva carpeta/logo.jpeg" alt="SunInversiones">
                                </a>
                            </div>
                            <div class="login-form">
                                <form action="login" method="post">
                                    <div class="form-group">
                                        <label>Nombre de Usuario</label>
                                        <input class="au-input au-input--full" type="text" name="nombreusuario" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input class="au-input au-input--full" type="password" name="claveusuario" placeholder="Password">
                                    </div>
                                    <div class="login-checkbox">
                                        <label>
                                            <input type="checkbox" name="remember">Recuerda
                                        </label>
                                        <label>
                                            <a href="#">Olvidaste la Contraseņa?</a>
                                        </label>
                                    </div>
                                    <button class="au-btn au-btn--block au-btn--blue m-b-20" type="submit">sign in</button>
                                </form>
                                <div class="register-link">
                                    <p>
                                        No tienes una cuenta?
                                        <a href="#">Ingresa Aqui</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Jquery JS-->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap JS-->
        <script src="assets/jquery/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- Vendor JS       -->
        <script src="assets/estiloExterno/vendor/slick/slick.min.js"></script>
        <script src="assets/estiloExterno/vendor/wow/wow.min.js"></script>
        <script src="assets/estiloExterno/vendor/animsition/animsition.min.js"></script>
        <script src="assets/estiloExterno/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="assets/estiloExterno/vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="assets/estiloExterno/vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="assets/estiloExterno/vendor/circle-progress/circle-progress.min.js"></script>
        <script src="assets/estiloExterno/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="assets/estiloExterno/vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="assets/estiloExterno/vendor/select2/select2.min.js">
        </script>
        <!-- Main JS-->
        <script src="assets/estiloExterno/js/main.js"></script>
    </body>

</html>
<!-- end document-->