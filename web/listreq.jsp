<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Orden de Compra </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 12px; }
    td { font-size: 12px; }
</style>
<div class="container">
    <div class="card border-info">
        <div class="card-header">Historial de Ordenes de Compra</div>
        <div class="card-body">
            <div class="table table-responsive">
                <table id="TBLORDENES"class="table table-hover">
                    <thead>
                    <th>NroOrden</th>
                    <th>Fecha</th>
                    <th>Proveedor</th>
                    <th>Cant</th>
                    <th>Sede</th><th></th><th></th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
<script>
    $(document).ready(function () {
        CargaTablaOrd();
    });
    function CargaTablaOrd() {
        var tabla = $('#TBLORDENES');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": true,
            ajax: {
                method: "post",
                url: "requerimiento?action=getHist",
                dataSrc: "data"
            },
            columns: [
                //{data: "idorden"},
                {data: "nroorden"},
                {data: "fechaorden"},
                {data: "razonSocial"},
                {data: "cant"},
                {data: "nombresede"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.idorden;
                        return "<a href='requerimiento?action=listDet&id=" + indice + "'><span class='btn btn-info btn-sm'>VerDetalles</span></a>";
                    }
                },
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.idorden;
                        return "<button class='btn btn-danger' onclick='verPdf("+indice+")'><i class='fas fa-file-pdf'></i></button>";
                    }
                }
            ]
        });
    }
    function verPdf(idOrden){
        window.open("reporte?action=getReporteReq&idOrden=" + idOrden)
    }
</script>
<%@include file="partes/footer.jsp" %>
