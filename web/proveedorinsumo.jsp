<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    String cod = request.getParameter("cod");
    if (booleanoAdm) {
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Cotizacion</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 13px; }
    td { font-size: 12px; }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-secondary mb-3">
                <div class="card-header"> Registro Cotizacion</div>
                <div class="card-body">
                    <form id="frmcotizacion" action="cotizacion?action=regCotizacion" method="post">
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label>PROVEEDOR</label>
                                <div class="input-group">
                                    <input type="text" id="ruc" class="form-control form-control-sm" readonly="">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary btn-sm" type="button" onclick="verProveedor()">Buscar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>RAZON SOCIAL</label>
                                <input type="text" id="razonsocial" name="razonsocial" class="form-control form-control-sm" readonly="">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>TIPO</label>
                                <input type="text" id="tipo" name="tipo" class="form-control form-control-sm" readonly="">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label>IGV</label><br>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" id="rbigv" name="rbigv" value="1">SI
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" id="rbigv" name="rbigv" value="0">NO
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>COD.INSUMO</label>
                                <div class="input-group">
                                    <input type="text" id="codinsumo" name="codinsumo" class="form-control form-control-sm" value="<%=cod != null ? cod : ""%>" readonly="">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary btn-sm" onclick="verInsumos()" type="button">Buscar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label>DESCRIPCION</label>
                                <input type="text" id="descripcion" name="descripcion" class="form-control form-control-sm" readonly="">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label>UNIDAD</label>
                                <input type="text" id="unidad" name="unidad" class="form-control form-control-sm" readonly="">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label>PRECIO</label>
                                <input type="number" id="precio" name="precio" class="form-control form-control-sm">
                            </div>
                            <div class="col-md-12 col-xl-12">
                                <button type="button" class="btn btn-success btn-sm" onclick="RegistrarCotizacion()">REGISTRAR</button>
                                <button type="reset" class="btn btn-danger btn-sm">CANCELAR</button>
                                <%if (cod != null) { %>
                                <button type="reset" class="btn btn-danger btn-sm" onclick="Regresar()">REGRESAR</button><%}%>
                            </div>
                            <input type="text" id="idinsumo" name="idinsumo" style="visibility: hidden" readonly="" required="">
                            <input type="text" id="idproveedor" name="idproveedor" style="visibility: hidden" readonly="" required="">
                            <input type="text" id="idprovinsumo" name="idprovinsumo" style="visibility: hidden" value="0" readonly="" required="">
                        </div>
                    </form>
                </div>
                <div class="card-footer">

                </div>
            </div> 
        </div>
        <div class="col-md-12">
            <div class="card border-secondary mb-3">
                <div class="card-header"> LISTADO</div>
                <div class="card-body">
                    <div class="table table-responsive">
                        <table id="TBLCOTIZACION" class="table table-hover">
                            <thead>
                            <th>CODIGO</th>
                            <th>RUC</th>
                            <th>RAZON SOCIAL</th>
                            <th>DESCRIPCION INSUMO</th>
                            <th>BASE</th>
                            <th>IGV</th>
                            <th>PRECIO</th>
                            <th></th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">

                </div>
            </div> 
        </div>
    </div>
</div>
<script>
    var alerta;
    $(document).ready(function () {
        GeneraTablaCotiz();
    });
    function Regresar() {
        window.location = "index?action=ordenList";
    }
    function RegistrarCotizacion() {
        var formulario = $('#frmcotizacion');
        var url = formulario.attr("action");
        var data = formulario.serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response)
            {
                $.confirm({
                    title: 'Aviso !',
                    content: response,
                    buttons: {
                        Ok: function () {
                            GeneraTablaCotiz();
                            formulario.trigger("reset");
                        }
                    }
                });
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    function verProveedor() {
        alerta = $.dialog({
            title: '<i class="fas fa-info-circle"></i>',
            columnClass: 'col-md-9',
            containerFluid: true,
            content: '<div class="">' +
                    '<table id="TBLPROVEEDOR" class="table table-hover" style="width:100%">' +
                    '<thead>' +
                    '<th>RUC</th>' +
                    '<th>RAZON SOCIAL</th>' +
                    '<th>TIPO</th>' +
                    '<th></th>' +
                    '</thead>' +
                    '<tbody></tbody>' +
                    '</table>' +
                    '</div>',
            onOpenBefore: function () {
                GeneraTablaProveedor();
            }
        });
    }
    function verInsumos() {
        alerta = $.dialog({
            title: '<i class="fas fa-info-circle"></i>',
            columnClass: 'col-md-9',
            containerFluid: true,
            content: '<div class="table table-responsive">' +
                    '<table id="TBLINSUMO" class="table table-hover" style="width:100%">' +
                    '<thead>' +
                    '<th>CODIGO</th>' +
                    '<th>DESCRIPCION</th>' +
                    '<th>UNIDAD DE MEDIDA</th>' +
                    '<th></th>' +
                    '</thead>' +
                    '<tbody></tbody>' +
                    '</table>' +
                    '</div>',
            onOpenBefore: function () {
                GeneraTablaInsumo();
            }
        });
    }
    function  GeneraTablaCotiz() {
        var tablaAdjunto = $('#TBLCOTIZACION');
        tablaAdjunto.dataTable().fnDestroy();
        tablaAdjunto.DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            "ordering": false,
            "orderable": false,
            ajax: {
                method: "post",
                url: "cotizacion?action=provInsumo",
                dataSrc: "data"
            }
            ,
            columns: [
                //{data: 'idProvInsumo'},
                {data: 'codigoInsumo'},
                {data: 'ruc'},
                {data: 'razonSocial'},
                {data: 'descripcionInsumo'},
                {data: 'base'},
                {data: 'igv'},
                {data: 'precio'},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return "<span class='btn btn-success btn-sm' onclick='getIdprovInsumo(" + data.idProvInsumo + ")'>Modificar</span>";
                    }
                },
            ]
        }
        );
    }
    function  GeneraTablaInsumo() {
        var tablaAdjunto = $('#TBLINSUMO');
        tablaAdjunto.dataTable().fnDestroy();
        tablaAdjunto.DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            "ordering": false,
            "orderable": false,
            ajax: {
                method: "post",
                url: "insumo?action=list",
                dataSrc: "data"
            }
            ,
            columns: [
                //{data: 'idInsumo'},
                {data: 'codigoInsumo'},
                {data: 'descripcionInsumo'},
                {data: 'descripcion'},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return "<span class='btn btn-success btn-sm' onclick='aņadirInsumo(" + data.idInsumo + ")'>Agregar</span>";
                    }
                },
            ]
        }
        );
    }
    function aņadirInsumo(idInsumo) {
        $.ajax({
            type: "POST",
            url: "insumo?action=listInsumoId",
            data: "idInsumo=" + idInsumo,
            success: function (response)
            {
                var json = JSON.parse(response);
                $.each(json.data, function (i, item) {
                    $('#idinsumo').val(item.idInsumo);
                    $('#codinsumo').val(item.codigoInsumo);
                    $('#descripcion').val(item.descripcionInsumo);
                    $('#unidad').val(item.descripcion);
                });
                alerta.close();
            }, error: function (response)
            {
                $.alert(response);
            }
        });
    }
    function  GeneraTablaProveedor() {
        var tablaAdjunto = $('#TBLPROVEEDOR');
        tablaAdjunto.dataTable().fnDestroy();
        tablaAdjunto.DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            "ordering": false,
            "orderable": false,
            ajax: {
                method: "post",
                url: "proveedor?action=listprov",
                dataSrc: "data"
            }
            ,
            columns: [
                //{data: 'idproveedor'},
                {data: 'ruc'},
                {data: 'razonSocial'},
                {data: 'descripcion'},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return "<span class='btn btn-success btn-sm' onclick='aņadirProveedor(" + data.idproveedor + ")'>Agregar</span>";
                    }
                }
            ]
        }
        );
    }
    function aņadirProveedor(idProveedor) {
        $.ajax({
            type: "POST",
            url: "proveedor?action=getProv",
            data: "id=" + idProveedor,
            success: function (response)
            {
                var json = JSON.parse(response);
                $.each(json.data, function (i, item) {
                    $('#idproveedor').val(item.idproveedor);
                    $('#ruc').val(item.ruc);
                    $('#razonsocial').val(item.razonSocial);
                    $('#tipo').val(item.descripcion);
                });
                alerta.close();
            }, error: function (response)
            {
                $.alert(response);
            }
        });
    }
    function getIdprovInsumo(idProvInsumo) {
        $.ajax({
            type: "POST",
            url: "cotizacion?action=getCotizacion",
            data: "idProvInsumo=" + idProvInsumo,
            success: function (response)
            {
                var json = JSON.parse(response);
                $.each(json.data, function (i, item) {
                    $('#idprovinsumo').val(item.idProvInsumo);
                    $('#idproveedor').val(item.idProveedor)
                    $('#ruc').val(item.ruc)
                    $('#razonsocial').val(item.razonSocial);
                    $('#tipo').val(item.descripcion)
                    $('#idinsumo').val(item.idInsumo);
                    $('#codinsumo').val(item.codigoInsumo);
                    $('#descripcion').val(item.descripcionInsumo);
                    $('#unidad').val(item.unidad);
                    $('#precio').val(item.precio);
                    $("input[name='rbigv'][value='" + item.igv + "']").prop('checked', true);
                });
            }, error: function (response)
            {
                $.alert(response);
            }
        });
    }
</script>
<%} else {
        response.sendRedirect("index?action=index");
    }%>
<%@include file="partes/footer.jsp" %>