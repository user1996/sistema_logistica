<%@page import="negocio.ordenpedido_neg"%>
<%@page import="com.google.gson.JsonObject"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Orden de Compra</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 12px; }
    td { font-size: 12px; }
</style>
<div class="container">
    <div class="card border-primary">
        <div class="card-body">
            <div class="row">
                <div class="col-md-3 mb-3">
                    <strong>RUC : </strong>
                    <span id="ruc"></span>
                </div>
                <div class="col-md-9 mb-3">
                    <strong>PROVEEDOR : </strong>
                    <span id="nameprov"></span>
                </div>
                <div class="col-md-3 mb-3">
                    <strong>SUBTOTAL : </strong>
                    <span id="subtotal"></span>
                </div>
                <div class="col-md-3 mb-3">
                    <strong>EXONERADO : </strong>
                    <span id="exonerado"></span>
                </div>
                <div class="col-md-3 mb-3">
                    <strong>IGV : </strong>
                    <span id="igv"></span>
                </div>
                <div class="col-md-3 mb-3">
                    <strong>NETO : </strong>
                    <span id="neto"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="card border-info">
        <div class="card-header">Detalle Orden de Compra</div>
        <div class="card-body">
            <div class="table table-responsive">
                <table id="TBLDETALLEORDEN"class="table table-hover">
                    <thead>
                    <th>Cant</th>
                    <th>Producto</th>
                    <th>Unidad</th>
                    <th>Precio</th>
                    <th>Total</th>
                    <%if (booleanoAdm) {%><th>Accion</th><%}%>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <a href="orden?action=listOrdenH"><span class="btn btn-info">Regresar</span></a>
            <button class="btn btn-secondary" onclick="A�adirInsumo()">A�adir Insumo</button>
        </div>
    </div>
</div>
<script>
    <%
        String id = request.getParameter("id");
    %>
    var id, alertInsumo,alertt;
    $(document).ready(function () {
        id =<%=id != null ? id : "0"%>
        cargaTablaDetalleOrden(id);
    });
    function getData(idOrden) {
        var neto = 0.0, igv = 0.0, subtotal = 0.0, cant = 0.0, precio = 0.0, igv = 0, migv = 0.0, exon = 0.0, nexon = 0.0;
        $.ajax({
            type: 'POST',
            url: 'orden?action=listaOrdenDetalle',
            data: 'idOrden=' + idOrden,
            success: function (response, textStatus, jqXHR) {
                var json = JSON.parse(response);
                $.each(json.data, function (i, item) {
                    $("#nameprov").text(item.razonSocial);
                    $("#ruc").text(item.ruc);
                    cant = parseFloat(item.cantidadRequerida);
                    precio = parseFloat(item.precio);
                    igv = parseInt(item.igv);
                    exon = exon + (igv == 0 ? (cant * precio) : 0);
                    nexon = nexon + (igv == 1 ? (cant * precio) : 0);
                });
                console.log(nexon);
                subtotal = nexon;
                migv = subtotal * 0.18;
                neto = subtotal + migv + exon;
                $("#subtotal").text(Math.round10(subtotal, -2));
                $("#exonerado").text(Math.round10(exon, -2));
                $("#igv").text(Math.round10(migv, -2));
                $("#neto").text(Math.round10(neto, -2));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR + "\t" + textStatus + "\t" + errorThrown);
            }
        });
        ///
    }
    function cargaTablaDetalleOrden(id) {
        getData(id);
        var tabla = $('#TBLDETALLEORDEN');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": true,
            ajax: {
                method: "post",
                url: "orden?action=listaOrdenDetalle&idOrden=" + id,
                dataSrc: "data"
            },
            columns: [
                //{data: "iddetalleorden"},
                {data: "cantidadRequerida"},
                {data: "descripcionInsumo"},
                {data: "descripcion"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return Math.round(parseFloat(data.precio) * 100) / 100;
                    }
                },
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var res = Math.round(parseFloat(data.precio) * 100) / 100 * Math.round(parseFloat(data.cantidadRequerida) * 100) / 100;
                        return Math.round(res * 100) / 100;
                    }
                },
                <% if(booleanoAdm){%>
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var idProveedor = data.iddetalleorden;
                        return '<button class="btn btn-secondary btn-sm" onclick="EditarDetalle(' + data.iddetalleorden + ')" type="button">Editar</button>';
                    }
                }<%}%>
            ]
        });
    }
    function A�adirInsumo() {
         alertInsumo = $.confirm({
            title: '<strong>A�adir insumo</strong>',
            escapeKey: true,
            backgroundDismiss: false,
            columnClass: 'col-md-7 col-md-offset-12 col-xs-4 col-xs-offset-8',
            content: '<div class="table table-responsive">'
                    + '<table id="TBLSEARCH" class="table table-hover">'
                    + '<thead>'
                    + '<th>CodigoInsumo</th>'
                    + '<th>Descripcion</th>'
                    + '<th>Medida</th>'
                    + '<th>Precio</th>'
                    + '<th>Accion</th>'
                    + '</thead>'
                    + '<tbody></tbody>'
                    + '</table>'
                    + '</div>',
            onContentReady: function () {
                cargaLista(id);
            }
        });
    }
    function cargaLista(idOrden) {
        var tabla = $('#TBLSEARCH');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": false,
            ajax: {
                method: "post",
                url: "orden?action=loadInsumo&idOrden=" + idOrden,
                dataSrc: "data"
            },
            columns: [
                //{data: "idInsumo"},
                //{data:"idProveedor"},
                {data: "codigoInsumo"},
                {data: "descripcionInsumo"},
                {data: "descripcion"},
                {data: "precio"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var idInsumo = data.idInsumo;
                        var precio = data.precio;
                        var desc = data.descripcion;
                        var igv = data.igv;
                        var idprovinsumo = data.idProvInsumo;
                        return '<button class="btn btn-secondary btn-sm" onclick="AddInsumo(' + idInsumo + ',' + precio + ',' + "'" + '' + desc + '' + "'" + ',' + igv + ',' + idprovinsumo + ')">A�adir</button>';
                    }
                }
            ]
        });
    }
    function AddInsumo(idInsumo, precio, desc, igv, idprovinsumo) {
        alertt = $.confirm({
            title: 'Info',
            content: '' +
                    '<form id="frmEdit" action="orden?action=addItemDetalle" class="formName">' +
                    '<div class="form-group">' +
                    '<input type="text" name="idprovinsumo" value="' + idprovinsumo + '" style="display:none"><input type="text" name="idinsumo" value="' + idInsumo + '" style="display:none"><input name="precio" type="text" value="' + precio + '"style="display:none"><input name="idorden" type="text" value="' + id + '"style="display:none"><input name="igv" type="text" value="' + igv + '"style="display:none">' +
                    '<label>Ingrese Cantidad</label>' +
                    '<input type="number" name="cantidad" id="cantidad" placeholder="Cantidad ' + desc + '" class="name form-control" required />' +
                    '</div>' +
                    '</form>',
            buttons: {
                Registrar: {
                    btnClass: 'btn-blue',
                    action: function () {
                        var form = $("#frmEdit");
                        var data = form.serialize();
                        var url = form.attr('action');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: data,
                            success: function (response, textStatus, jqXHR) {
                                var text = "";
                                if (isNaN(response)) {
                                    text = response;
                                } else {
                                    text = response > 0 ? "Registro A�adido" : "Registro no A�adido";
                                    cargaTablaDetalleOrden(id);
                                    alertInsumo.close();
                                    alertt.close();
                                }
                                $.alert({
                                    title: 'Aviso!',
                                    content: text
                                });
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(jqXHR + "\t" + textStatus + "\t" + errorThrown);
                            }
                        });
                        return false;
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                $("#cantidad").focus();
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }
    function EditarDetalle(idDetalle) {
        var alerta = $.confirm({
            title: '<strong>Editar</strong>',
            escapeKey: true,
            backgroundDismiss: false,
            columnClass: 'col-md-3 col-md-offset-12 col-xs-4 col-xs-offset-8',
            content: '<form id="frmdetalle" action="orden?action=editDetalle"><input name="iddetalle" type="text" value="' + idDetalle + '" style="display :none"/>'
                    + '<input name="idprovinsumo" id="idprovinsumo" type="text" value="" style="display : none"/>'
                    + '<div class="form-group">'
                    + '<label>Descripcion</label>'
                    + '<input type="text" id="descripcion" name="descripcion" class="form-control form-control-sm" readonly>'
                    + '</div>'
                    + '<div class="form-group">'
                    + '<label>Precio</label>'
                    + '<input type="number" id="precio" name="precio" class="form-control form-control-sm">'
                    + '</div></form>',
            buttons: {
                Registrar: {
                    btnClass: 'btn btn-success',
                    keys: ['enter', 'enter'],
                    action() {
                        var form = $('#frmdetalle');
                        var data = form.serialize();
                        var url = form.attr('action');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: data,
                            success: function (response, textStatus, jqXHR) {
                                var text = "";
                                if (isNaN(response)) {
                                    text = response;
                                } else {
                                    text = response > 0 ? "Registro Actualizado" : "Registro no Actualizado";
                                    cargaTablaDetalleOrden(id);
                                    alerta.close();
                                }
                                $.alert({
                                    title: 'Aviso!',
                                    content: text
                                });
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(jqXHR + "\t" + textStatus + "\t" + errorThrown);
                            }
                        });
                        return false;
                    }
                }, Cancelar: {
                    btnClass: 'btn btn-warning'
                }
            },
            onContentReady: function () {
                $.ajax({
                    type: 'POST',
                    url: 'orden?action=listaOrdenDetallegetId',
                    data: 'iddetalle=' + idDetalle,
                    success: function (response, textStatus, jqXHR) {
                        var json = JSON.parse(response);
                        $.each(json.data, function (key, registro) {
                            $('#precio').val(registro.precio);
                            $('#descripcion').val(registro.descripcionInsumo);
                            $('#idprovinsumo').val(registro.idProvInsumo);
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR + "\t" + textStatus + "\t" + errorThrown);
                    }
                });
            }
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
