<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Proveedor</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 13px; }
    td { font-size: 12px; }
</style>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header"><h3>Lista de Proveedores</h3></div>
            <div class="card-body table table-responsive">
                <table id="TBLPROVEEDOR" class="table table-hover" style="width:100%">
                    <thead>
                    <th>Ruc</th>
                    <th>Razon Social</th>
                    <th>Tipo de Producto</th>
                    <th></th>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="proveedor?action=detalle&id=0"><span class="btn btn-success"> Nuevo Proveedor</span></a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        cargaTablaProv();
    });
    function cargaTablaProv() {
        var tabla = $('#TBLPROVEEDOR');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": true,
            ajax: {
                method: "post",
                url: "proveedor?action=listprov",
                dataSrc: "data"
            },
            columns: [
                //{data: "idproveedor"},
                {data: "ruc"},
                {data: "razonSocial"},
                {data: "descripcion"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.idproveedor;
                        return "<a href='proveedor?action=detalle&id=" + indice + "'><span class='btn btn-info btn-sm'>VerDetalles</span></a>";
                    }
                }
            ]
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
