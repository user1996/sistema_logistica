<%@page import="bean.beandetalleordencompra"%>
<%@page import="java.util.ArrayList"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    ArrayList<beandetalleordencompra> detLista = (ArrayList<beandetalleordencompra>) request.getSession().getAttribute("listDet");
    int indice = Integer.parseInt(request.getParameter("indice"));
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Consultar Orden</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 13px; }
    td { font-size: 12px; }
</style>
<div class="container">
    <div class="col-md-12">
        <div class="card border-primary mb-3">
            <div class="card-header bg-primary"><h4 class="text-white"><i class="fas fa-clipboard"></i> Detalle Individual</h4></div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label>Codigo</label>
                            <input type="text" name="codigoinsumo" class="form-control form-control-sm" value="<%=detLista.get(indice).getCodigoInsumo()%>" readonly="">
                        </div>
                        <div class="col-md-5 mb-3">
                            <label>Descripcion</label>
                            <input type="text" name="descripcion" class="form-control form-control-sm" value="<%=detLista.get(indice).getDescripcion()%> " readonly="">
                        </div>
                        <div class="col-md-5 mb-3">
                            <label>Proveedor</label>
                            <input type="text" name="ruc" class="form-control form-control-sm" value="<%=detLista.get(indice).getRazonSocial()%>" aria-describedby="basic-addon2">
                            <% if (detLista.get(indice).getIdProveedor() == 0) {%>
                            <a href="cotizacion?action=index&cod=<%=detLista.get(indice).getCodigoInsumo().trim()%>"><span class="btn btn-success btn-sm">Registrar</span></a><%}%>
                        </div>
                    </div>
                </form>
                <div class="card-footer">
                    <a href="index?action=ordenList"><span class="btn btn-info">Regresar</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function ModificaCantidad() {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response)
            {
                $.confirm({
                    title: 'Aviso !',
                    content: response,
                    buttons: {
                        Ok: function () {
                            GeneraTablaCotiz();
                            formulario.trigger("reset");
                        }
                    }
                });
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
