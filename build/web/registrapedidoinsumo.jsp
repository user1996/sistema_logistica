<%-- 
    Document   : registrapedidoinsumo
    Created on : 10/04/2019, 06:37:40 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/jsexcel/xlsx.full.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">Convertir un Archivo Excel a JSON con Sheet JS </h1>

            <p class="text-center">"Selecciona un archivo Excel para convertirlo a JSON"</p>
            <p class="text-center n">Espera unos segundos...</p>

            <div class="row text-center">

                <div class="col-md-12">				
                    <input type="file" name="archivoexcel" id="archivoexcel" />
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12">
                    <pre id="json"></pre>
                </div>
            </div>               

        </div>
    </body>
    <script src="assets/jsexcel/app.js"></script>
</html>
