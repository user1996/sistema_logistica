<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<style>
    th { font-size: 13px; }
    td { font-size: 12px; }
</style>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Insumo</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header"><h3>Lista de Insumos</h3></div>
            <div class="card-body table table-responsive">
                <table id="TBLINSUMO" class="table table-hover" style="width:100%">
                    <thead>
                    <th>Codigo</th>
                    <th>Descripcion Insumo</th>
                    <th>Unidad Medida</th>
                    <th>Categoria</th>
                    <th></th>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="insumo?action=detalle&id=0"><span class="btn btn-success"> Nuevo Insumo</span></a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        cargarTablaInsumo();
    });
    function cargarTablaInsumo() {
        var tabla = $('#TBLINSUMO');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": true,
            ajax: {
                method: "post",
                url: "insumo?action=list",
                dataSrc: "data"
            },
            columns: [
                //{data: "idInsumo"},
                {data: "codigoInsumo"},
                {data: "descripcionInsumo"},
                {data: "descripcion"},
                {data: "descripcionCategoria"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.idInsumo;
                        return "<a href='insumo?action=detalle&id=" + indice + "'><span class='btn btn-info btn-sm'>VerDetalles</span></a>";
                    }
                }
            ]
        });
    }
</script>
<%@include file="partes/footer.jsp" %>