<%@page import="java.util.List"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    ArrayList<beannotificacion> lstNotif = (ArrayList<beannotificacion>) request.getSession().getAttribute("dataDetalleNotif");
    if (lstNotif != null) {
        String tipousuario = listaSesion.get(0).getNombretipo();
        String getDatos = lstNotif.get(0).getDatos();
        String fecha = lstNotif.get(0).getFecha();
        String hora = lstNotif.get(0).getHora();
        String nombSede = lstNotif.get(0).getNombreSede();
        String correlativo = lstNotif.get(0).getCorrelativo();

%>
<style>
    th { font-size: 12px; }
    td { font-size: 12px; }
</style>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Detalle Requerimientos</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header">Requerimiento de <%=getDatos%></div>
            <div class="form-group">
                <p class="text-center"><strong>Nro : <%=correlativo%></strong></p>
                <p class="text-center"><%=fecha%> a las <%=hora%> - Sede <%=nombSede%></p>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table id="TBLDETREQ" class="table table-hover" style="width:100%">
                        <thead>
                        <th>Cant</th>
                        <th>Descripcion</th>
                        <th>Observacion</th>
                        <th>Estado</th>
                        <%if (booleanoAdm) {%><th></th><%}%>
                        </thead>
                        <tbody> 
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-secondary" onclick="Regresar()">Regresar</button>
                <button class="btn btn-primary" onclick="">Siguiente Requerimiento</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
    detRequerimiento(<%=request.getParameter("idreq")%>);
    })
            function Regresar(idReq) {
            window.location = "notif?action=getNotifM"
            }
    function detRequerimiento(id) {
    var tabla = $('#TBLDETREQ');
    tabla.dataTable().fnDestroy();
    tabla.DataTable({
    "paging": false,
            "ordering": false,
            "info": true,
            ajax: {
            method: "post",
                    url: "notif?action=listDet&idreq=" + id,
                    dataSrc: "data"
            },
            columns: [
                    //{data: "iddetalle"},
                    {data: "cant"},
            {data: "descripcion"},
            {data: "descAlarma"},
            {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                    var estad = data.estado;
                    var clase = estad == "1"?"success":estad == "2"?"danger":"warning";
                    return '<div class="alert-' + clase + '"><center><strong>' + data.estadoInfo + '</strong></center></div>'
                    }
            },<%if (booleanoAdm) {%>
            {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                    var str = data.estado == 3?"disabled":"";
                    return '<button class="btn btn-success btn-sm" ' + str + '  onclick="cambiaEstado(' + data.idalarma + ',' + data.iddetalle + ',' + data.cant + ',1)"><i class="fas fa-check-circle"></i></button> <button class="btn btn-danger btn-sm" ' + str + ' onclick="cambiaEstado('+ data.idalarma + ',' + data.iddetalle + ',' + data.cant + ',2)"><i class="fas fa-times-circle"></i></button>';
                    }
            }<%}%>
            ]

    });
    }
    function cambiaEstado(idalarma, iddetalle, cantidad,estado) {
    //var estado = 1;
    $.confirm({
    title: 'A�ade una Motivo!',
            content: '' +
            '<form class="formName">' +
            '<div class="form-group"><label>Cant</label><input type="number" id="cantidad" class="form-control" name="cantidad" value="' + cantidad + '"></div>' +
            '<div class="form-group">' +
            '<label>Descripcion</label>' +
            '<input type="text" id="motivo" class="name form-control" required />' +
            '</div>' +
            '</form>',
            buttons: {
            registrar: {
            text: 'Registrar',
                    btnClass: 'btn-blue',
                    action: function () {
                    var motivo = $("#motivo").val();
                    var cant = $("#cantidad").val();
                    $.ajax({
                    type: "POST",
                            url: "notif?action=cambiaEstado",
                            data: "idalarma=" + idalarma + "&estado=" + estado + "&motivo=" + motivo + "&cantidad=" + cant + "&iddetalle=" + iddetalle,
                            success: function (response)
                            {
                            $.alert(response);
                            detRequerimiento(<%=request.getParameter("idreq")%>);
                            }, error: function (response)
                    {
                    $.alert(response);
                    }
                    });
                    }
            },
                    cancel: function () {
                    //close
                    },
            },
            onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
            }
    });
    }
</script>
<%@include file="partes/footer.jsp" %>
<%
    } else {
        response.sendRedirect("index?action=index");
    }
%>
