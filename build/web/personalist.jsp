<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Usuarios</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 12px; }
    td { font-size: 11px; }
</style>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success mb-3">
            <div class="card-header"><h3>Lista de Personas</h3></div>
            <div class="card-body table table-responsive">
                <table id="TBLPERSONA" class="table table-hover" style="width:100%">
                    <thead>
                    <th>Documento</th>
                    <th>Primer Nombre</th>
                    <th>Segundo Nombre</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th></th>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="persona?action=listPersona&idPersona=0"><span class="btn btn-success"> Nuevo Persona</span></a>
                <a href="persona?action=listaUsuario"><span class="btn btn-secondary"> Ver Usuarios</span></a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        cargaTablaPersona();
    });
    function cargaTablaPersona() {
        var tabla = $('#TBLPERSONA');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": true,
            ajax: {
                method: "post",
                url: "persona?action=list",
                dataSrc: "data"
            },
            columns: [
                //{data: "idpersona"},
                {data: "documento"},
                {data: "primernombre"},
                {data: "segundonombre"},
                {data: "apellidopaterno"},
                {data: "apellidomaterno"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.idpersona;
                        return "<a href='persona?action=listPersona&idPersona=" + indice + "'><span class='btn btn-info btn-sm'>Editar</span></a>";
                    }
                }
            ]
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
