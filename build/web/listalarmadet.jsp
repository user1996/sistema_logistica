<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="funciones.operaciones"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<style>
    th { font-size: 12px; }
    td { font-size: 11px; }

</style>
<%    operaciones op;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    Date fechaSist = new Date();
    alarma_neg al_neg;
    String id = request.getParameter("id");
    JsonObject obj = null;
    if (id != null) {
        al_neg = new alarma_neg();
        ArrayList<beanalerta> listaDetAl = al_neg.listDetalleAlerta(Integer.parseInt(id));
        op = new operaciones();
        obj = op.getJsonDetAlarma(listaDetAl);

%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Alertas Frecuentes(Detalle)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-info">
            <div class="card-header">
                Informacion
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span class="">Razon Social :</span>
                        <span id="razonsocial"></span>
                    </div>
                    <div class="col-6">
                        <span class="">Tipo :</span>
                        <span id="tipoalerta"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card border-info">
            <div class="card-header">
                Detalle
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table id="TBLDETALLE" class="table table-hover">
                        <thead>
                        <th>Fecha vencimiento</th>
                        <th>Fecha alarma</th>
                        <th>Estado</th>
                        <th></th>
                        <th>Accion</th>
                        </thead>
                        <tbody>
                            <%for (beanalerta bal : listaDetAl) {%>
                            <tr>
                                <td><%=bal.getFechavencimiento()%></td>
                                <td><%=bal.getFechaalerta()%></td>
                                <td><%=bal.getEstado() == 0 ? "Pendiente" : "Pagado"%></td>
                                <td><button type="button" class="btn btn-<%=bal.getEstado() == 0 ? "warning" : "success"%> btn-sm"><i class="fas fa-<%=bal.getEstado() == 0 ? "exclamation" : "check"%>"></i></button></td>
                                        <%if (bal.getEstado() == 0) {%>
                                <td><button type="button" class="btn btn-info btn-sm" onclick="RealizarPago(<%=bal.getIddetalle()%>,<%=bal.getIdalerta()%>,<%=bal.getIdtipo()%>,<%=bal.getIdsede()%>)" <%=format.parse(bal.getFechaalerta()).after(format.parse(format.format(fechaSist))) ? "disabled" : ""%>>Pagar</button></td>
                                <%} else {%><td></td><%}%>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-info" onclick="window.location='index?action=alarm'">Regresar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var jsonDetalle;
    //id Sede;1->Palacio;2->Venezuela;3->Huandoy;4->CANTA CITY
    $(document).ready(function () {
        jsonDetalle =<%=obj%>;
        $.each(jsonDetalle.data, function (i, item) {
            $("#razonsocial").text(item.razonsocial);
            $("#tipoalerta").text(item.tipo);
        });
    })
    function RealizarPago(iddetalle, idalerta, idtipo, idsede) {
        var cobrar = 0, idmoneda = 0;//1->Sol;2->Dolar
        var val = 0;
        switch (parseInt(idtipo)) {
            case 1007:
                idmoneda = 2;
                val = 1;
                break;
            case 1008:
                idmoneda = 1;
                switch (parseInt(idsede)) {
                    case 1://palacio
                        cobrar = 9647.80;
                        break;
                    case 2://Venezuela
                        cobrar = 19295.60;
                        break;
                    case 3://Huandoy
                        cobrar = 9647.80;
                        break;
                    default:
                        cobrar = 0;
                        break;
                }
                idmoneda = 1;
                val = 1;
                break;
            case 1009:
                idmoneda = 1;
                val = 1;
                break;
            case 1010:
                idmoneda = 1;
                val = 1;
                break;
            case 1011:
                idmoneda = 1;
                switch (parseInt(idsede)) {
                    case 1:
                        cobrar = 1373.30;
                        break;
                    default:
                        cobrar = 0;
                        break;
                }
                val = 1;
                break;
            case 1012:
                idmoneda = 1;
                val = 1;
                break;
            case 1013:
                idmoneda = 1;
                val = 1;
                break;
            case 1014:
                idmoneda = 2;
                cobrar = 35400.0;
                val = 1;
                break;
            default:
                idmoneda = 1;
                cobrar = 0;
                break;
        }
        if (val == "1") {
            $.confirm({
                title: 'Prompt!',
                content: '' +
                        '<form action="" class="formName">' +
                        '<div class="form-group">' +
                        '<label>Monto a pagar en ' + (idmoneda == 1 ? "soles :" : idmoneda == 2 ? "dolares :" : "") + '</label>' +
                        '<input type="number" id="monto" min="0" placeholder="Monto Pago" value="' + (cobrar == 0 ? "" : cobrar) + '" class="name form-control" required />' +
                        '</div>' +
                        '</form>',
                buttons: {
                    registrar: function () {
                        Update(iddetalle, idmoneda, $("#monto").val(), val);
                    },
                    cancel: function () {
                        //close
                    }
                },
                onContentReady: function () {
                    $("#monto").focus();
                }
            });
        } else {
            Update(iddetalle, idmoneda, $("#monto").val(), val);
        }
    }
    function Update(iddetalle, idmoneda, precio, val) {
        $.ajax({
            type: "POST",
            url: "alarma?action=updateAl",
            data: "iddetalle=" + iddetalle + "&idmoneda=" + idmoneda + "&precio=" + precio + "&val=" + val,
            success: function (response)
            {
                if (response == "1") {
                    window.location = "alarma?action=detAlarma&id=" + <%=id%>;
                } else {
                    $.alert(response);
                }
            }, error: function (response) {
                console.log(response);
            }
        });
    }
</script>
<%
    } else {
        response.sendRedirect("index?action=index");
    }
%>
<%@include file="partes/footer.jsp" %>
