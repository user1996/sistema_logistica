<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Orden de Compra</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    th { font-size: 12px; }
    td { font-size: 10px; }
</style>
<div class="container">
    <form id="frmorden" action="orden?action=testData" method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-success">          	
                    <div class="card-header"><h4><i class="fas fa-file-excel"></i> Carga Archivo Excel</h4></div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="custom-file" id="customCss">
                                <input type="file" class="custom-file-input" id="cargararchivo" name="cargararchivo">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card border-info col-md-12">
                    <div class="card-header"><button type="submit" class="btn btn-success">Generar Pedido</button></div>
                    <div class="card-body table table-responsive">
                        <table id="TBLEXCEL" class="table table-hover">
                            <thead>
                            <th style="height: 10%">CODIGO</th>
                            <th>DESCRIPCION</th>
                            <th>UNIDAD</th>
                            <th>CANTIDAD REQUERIDA</th>
                            </thead>
                            <tbody id="idbody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $('#cargararchivo').filestyle({
        buttonName: 'btn btn-success',
        buttonText: ' Abrir Archivo'
    });
    $(document).ready(function () {
        $("#frmorden").submit(function (e) {
            //e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                url: url,
                type: "POST",
                data: form.serialize(),
                dataType: "text", /* contentType: "application/vnd.ms-excel",*/
                success: function (data, text_status, XMLHttpRequest)
                {
                    console.log(data)
                },
                error: function (XMLHttpRequest, text_status, error)
                {
                    console.log("error" + text_status + ":::" + error);
                }
            });
        });
    });
</script>
<script src="<%=patch%>/jqueryexcel/FileSaver.min.js"></script>
<script src="<%=patch%>/jqueryexcel/app.js"></script>
<%@include file="partes/footer.jsp" %>
