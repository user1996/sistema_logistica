<%@page import="com.google.gson.JsonObject"%>
<%@page import="bean.beansede"%>
<%@page import="negocio.persona_neg"%>
<%@page import="negocio.alarma_neg"%>
<%@page import="bean.beantipoalerta"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<style>
    th { font-size: 12px; }
    td { font-size: 11px; }

</style>
<%    alarma_neg aln = new alarma_neg();
    persona_neg per = new persona_neg();
    ArrayList<beantipoalerta> lista = aln.ListTipo();
    JsonObject obj = per.getListaSede();
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Alertas Frecuentes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success">
            <form id="frmalerta">
                <div class="card-header">
                    <p>Alertas</p>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Codigo Suministro :</label>
                            <input type="text" class="form-control form-control-sm" name="codsuministro" id="codsuministro">
                        </div>
                        <div class="form-group col-md-7">
                            <label>Ruc :</label>
                            <input type="text" name="ruc" id="ruc" class="form-control form-control-sm">
                            <input type="text" name="idproveedor" id="idproveedor" class="form-control form-control-sm" style="display: none">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Sede :</label>
                            <select id="idsede" name="idsede" class="form-control form-control-sm"></select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Tipo :</label>
                            <select name="idtipo" id="idtipo" class="form-control form-control-sm">
                                <option value="0">SELECCIONE...</option><%for (beantipoalerta bn : lista) {%>
                                <option value="<%=bn.getIdtipo()%>"><%=bn.getNombretipo()%></option><%}%>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Fecha Vencimiento :</label>
                            <input type="date" class="form-control form-control-sm" id="fechavencimiento" name="fechavencimiento">
                        </div>
                        <div class="form-group col-md-4">
                            <span id="txtdias" style="color: red">Avisar 0 dias antes</span>
                            <input type="range" name="nrodias" value="0" min="0" max="30"  list="tickmarks" id="nrodias" class="form-control-range">
                            <datalist id="tickmarks"><%for (int i = 0; i <= 30; i = i + 5) {%>
                                <option value="<%=i%>" label="<%=i%>">
                                    <%}%>
                            </datalist>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" class="btn btn-success btn-sm" onclick="registraAlarma()" <%=nombretipo.equals("SUPERVISOR")?"disabled":"" %>>Registrar</button>
                </div>
            </form>
        </div>
        <div class="card border-info">
            <div class="card-header">
                Listado
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table id="TBLDETALLE" class="table table-hover">
                        <thead>
                        <th>Razon Social</th>
                        <th>Suministro</th>
                        <th>Tipo</th>
                        <th>Sede</th>
                        <th>Vencimiento</th>
                        <th>Estado</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        GenTable();
        cargaComboSede();
        const sld = document.getElementById("nrodias");

        const da1 = document.getElementById("txtdias");

        da1.textContent = "Avisar " + sld.value + " dias antes";

        sld.addEventListener("input", ((x) => {
            da1.textContent = "Avisar " + sld.value + " dias antes";
        }), false);


        $("#ruc").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "cotizacion?action=filtroProv",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    focus: function (event, ui) {
                        event.preventDefault();
                        alert();
                        $(this).val(ui.item.ruc + "-----");
                    },
                    success: function (data) {
                        if (data.data.length == 0) {
                            $('#idproveedor').val(0);
                        }
                        response($.map(data.data, function (item) {
                            return {
                                value: item.concat,
                                id: item.idproveedor
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                var idprov = ui.item.id;
                $('#idproveedor').val(idprov);
            }
        });
    })
    function GenTable() {
        var tabla = $('#TBLDETALLE');
        tabla.dataTable().fnDestroy();
        tabla.DataTable({
            "paging": true,
            "ordering": false,
            "info": false,
            ajax: {
                method: "post",
                url: "alarma?action=getJsonList",
                dataSrc: "data"
            },
            columns: [
                //{data: "idalerta"},
                {data: "razonSocial"},
                {data: "nrosuministro"},
                {data: "nombretipo"},
                {data: "nombresede"},
                {data: "fechaVenc"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return '<span class="btn btn-info btn-sm" onClick="dirigeDetalle('+data.idalerta+')"><i class="fas fa-exclamation"></i> pendiente</span>';
                    }
                }
            ]
        });
    }
    function dirigeDetalle(idalerta){
        window.location="alarma?action=detAlarma&id="+idalerta;
    }
    function registraAlarma() {
        var form = $("#frmalerta");
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: "alarma?action=regAlarma",
            data: data,
            success: function (response)
            {
                if (response == "1") {
                    $.alert({
                        title: 'Aviso!',
                        content: 'Registrado',
                        buttons: {
                            Ok: function () {
                                window.location="index?action=alarm";
                            }
                        }
                    });
                }else{
                    $.alert(response);
                }
            }, error: function (response) {
                alert(response);
            }
        });
    }
    function cargaComboSede() {
        var json = <%=obj%>
        $("#idsede").append('<option value=0>SELECCIONE...</option>');
        $.each(json.data, function (key, registro) {
            $("#idsede").append('<option value=' + registro.idsede + '>' + registro.nombresede + '</option>');
        });
    }
</script>
<%@include file="partes/footer.jsp" %>
