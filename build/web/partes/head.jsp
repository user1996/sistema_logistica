<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String patch=request.getContextPath();
%>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=ISO-8859-1″ />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Fontfaces CSS-->
        <link href="<%=patch%>/assets/estiloExterno/css/font-face.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/fontAwesome/css/all.min.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="<%=patch%>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all">
        <!-- Toast tr-->
        <link href="<%=patch%>/assets/toasttr/toastr.min.css" rel="stylesheet" media="all">
        <!--DATA TABLE-->
        <link rel="stylesheet" href="<%=patch%>/assets/datatable/dataTables.bootstrap4.min.css">
        <!--JqueryConfirm-->
        <link rel="stylesheet" href="<%=patch%>/assets/jquery/jquery-confirm.min.css">
        <!--Jquery UI Css -->
        <link href="<%=patch%>/assets/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <!-- Vendor CSS-->
        <link href="<%=patch%>/assets/estiloExterno/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="<%=patch%>/assets/estiloExterno/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
        <!-- Main CSS-->
        <link href="<%=patch%>/assets/estiloExterno/css/theme.css" rel="stylesheet" media="all">
        
        <!--JQUERY -->
        <script src="<%=patch%>/assets/jquery/jquery-3.3.1.min.js"></script>
        <!--DATA TABLE JS-->
        <script src="<%=patch%>/assets/datatable/jquery.dataTables.min.js"></script>
        <script src="<%=patch%>/assets/datatable/dataTables.bootstrap4.min.js"></script>
        <!--JQUERY CONFIRM -->
        <script src="<%=patch%>/assets/jquery/jquery-confirm.min.js"></script>
        <!--Jquery JS-->
        <script src="<%=patch%>/assets/jquery/jquery-ui.js" type="text/javascript"></script>
        <!--File Saver-->
        <script src="<%=patch%>/assets/fileinput/bootstrap-filestyle.min.js"></script>
        <!-- Excel -->
        <script src="<%=patch%>/jqueryexcel/xlsx.full.min.js"></script>
        <!--Push JS -->
        <script src="<%=patch%>/assets/toasttr/toastr.min.js"></script>
        <title>Logistica</title>
        <script src="<%=patch%>/assets/estiloExterno/js/funcion.js"></script>
    </head>
