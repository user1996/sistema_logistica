<%@page import="negocio.persona_neg"%>
<%@page import="com.google.gson.JsonObject"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<%    persona_neg per = new persona_neg();
    JsonObject jsonsede = per.getListaSede();
    JsonObject jsonarea = per.getListaArea();
%>
<style>
    th { font-size: 12px; }
    td { font-size: 10px; }
</style>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Requerimiento</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="card border-primary">
        <form id="frmrequerimiento" enctype="multipart/form-data">
            <div class="card-header bg-primary text-white">Registro Requerimiento</div>
            <div class="card-body">
                <div class="row">
                    <div class="alert-info col-md-12 mb-3">Aviso</div>
                    <div class="col-md-6 mb-3">
                        <label>Sala</label>
                        <select name="idsede" id="idsede" class="form-control form-control-sm"></select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Responsable</label>
                        <input type="text" id="idresponsable" name="idresponsable" value="<%=idusuario%>" readonly="" style="display: none">
                        <input type="text" value="<%=datos%>" class="form-control form-control form-control-sm">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Area</label>
                        <select name="idarea" id="idarea" class="form-control form-control-sm"></select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label>Cantidad</label>
                        <input type="number" name="cantidad" id="cantidad" class="form-control form-control form-control-sm">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label>Tipo de Orden</label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="tipo" type="radio" id="inlineCheckbox1" value="1">
                            <label class="form-check-label" for="inlineCheckbox1">Servicio</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="tipo" type="radio" id="inlineCheckbox1" checked="" value="0">
                            <label class="form-check-label" for="inlineCheckbox1">Compra</label>
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label>Descripcion Requerimiento</label>
                        <input type="text" name="descripcion" id="descripcion" class="form-control form-control form-control-sm">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Imagen (Opcional)</label>
                        <input type="file" name="image" id="image" onchange="fileValidation()" class="form-control-file">
                    </div>
                    <!-- Image preview -->
                    <div id="imagePreview" class="col-md-6 mb-3" >
                        <img src="" alt="" style="height: 10"  class="img-thumbnail">
                    </div>
                </div><br>
                <button type="button" id="btnsubmit" class="btn btn-secondary" onclick="AddItemx2(event)">A�adir Item</button>
            </div>
        </form>
    </div>
    <div class="card border-info">
        <div class="card-header bg-info text-white">Registro Requerimiento</div>
        <div class="card-body">
            <div class="table table-responsive">
                <table id="TBLREQUERIMIENTO" class="table table-hover">
                    <thead>
                    <th>Cant.</th>
                    <th>Descripcion</th>
                    <th>Accion</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-secondary btn-sm" onclick="RegistrarRequerimiento()">Registrar</button>
        </div>
    </div>
</div>
<script>
    var fileInput = document.getElementById('image');
    $(document).ready(function () {
        cargacomboSede();
        cargaComboArea();
    });
    function fileValidation() {
        var imgg = document.getElementById('imagePreview');
        var filePath = fileInput.value;
        var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            $.confirm({
                title: 'Aviso !',
                content: 'Por favor solo se admiten imagenes !',
                buttons: {
                    confirm: function () {
                        fileInput.value = '';
                        $('#imagePreview').empty();
                        return true;
                    }
                }
            });
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
    }
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    function RegistrarRequerimiento() {
        var form = $('#frmrequerimiento');
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: "requerimiento?action=registraTest",
            data: data,
            success: function (response)
            {
                var confirm = $.confirm({
                    title: 'Aviso',
                    content: response == "1" ? "Registrado" : response,
                    buttons: {
                        Ok: {
                            btnClass: 'btn btn-info',
                            action() {
                                if (response == "1") {
                                    window.location = "index?action=index";
                                } else {
                                    return true;
                                }
                            }
                        }
                    }
                });

            }, error: function (response) {
                console.log(response);
            }
        });
    }
    function AddItemx2(event) {
        //stop submit the form, we will post it manually.
        event.preventDefault();

        // Get form
        var form = $('#frmrequerimiento')[0];

        // Create an FormData object 
        var data = new FormData(form);
        //alert(data);
        // If you want to add an extra field for the FormData
        //data.append("CustomField", "This is some extra data, testing");

        // disabled the submit button
        //$("#btnSubmit").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "requerimiento?action=addTest",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (response) {
                if (IsJsonString(response)) {
                    GeneraTabla(JSON.parse(response));
                    $('#cantidad').val("");
                    $('#descripcion').val("");
                    $('#cantidad').focus();
                    fileInput.value = '';
                    $('#imagePreview').empty();
                } else {
                    var confirm = $.confirm({
                        title: 'Aviso',
                        content: response,
                        buttons: {
                            Ok: {
                                btnClass: 'btn btn-info',
                                action() {

                                }
                            }
                        }
                    });
                }
            },
            error: function (e) {

                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnsubmit").prop("disabled", false);

            }
        });

    }
    function addItem() {
        var form = $('#frmrequerimiento');
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: "requerimiento?action=addItem",
            data: data,
            success: function (response)
            {
                if (IsJsonString(response)) {
                    GeneraTabla(JSON.parse(response));
                    $('#cantidad').val("");
                    $('#descripcion').val("");
                    $('#cantidad').focus();
                } else {
                    var confirm = $.confirm({
                        title: 'Aviso',
                        content: response,
                        buttons: {
                            Ok: {
                                btnClass: 'btn btn-info',
                                action() {

                                }
                            }
                        }
                    });
                }
            }, error: function (response) {
                console.log(response);
            }
        });
    }
    function DeleteItem(indice) {
        $.ajax({
            type: "POST",
            url: "requerimiento?action=deleteItem",
            data: "indice=" + indice,
            success: function (response)
            {
                if (IsJsonString(response)) {
                    GeneraTabla(JSON.parse(response));
                } else {
                    alert(response);
                }
            }, error: function (response) {
                console.log(response);
            }
        });
    }
    function GeneraTabla(json) {
        $('#TBLREQUERIMIENTO').dataTable().fnDestroy();
        $('#TBLREQUERIMIENTO').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            data: json.data,
            columns: [
                //{data: "idusuario"},
                {data: "cantidad"},
                {data: "descripcion"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var indice = data.indice;
                        return '<button class="btn btn-secondary btn-sm" onclick="DeleteItem(' + indice + ')">Eliminar</button>'
                    }
                }
            ]
        });
    }
    function cargacomboSede() {
        var json = <%=jsonsede%>
        $("#idsede").append('<option value=0>SELECCIONE...</option>');
        $.each(json.data, function (key, registro) {
            $("#idsede").append('<option value=' + registro.idsede + '>' + registro.nombresede + '</option>');
        });
        $("#idsede").val(<%=idsede%>);
    }
    function cargaComboArea() {
        var json = <%=jsonarea%>
        $("#idarea").append('<option value=0>SELECCIONE...</option>');
        $.each(json.data, function (key, registro) {
            $("#idarea").append('<option value=' + registro.idarea + '>' + registro.nombrearea + '</option>');
        });
        $("#idarea").val(<%=idarea%>);
    }
</script>
<%@include file="partes/footer.jsp" %>
