<%@page import="bean.beanproveedor"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="negocio.proveedor_neg"%>
<%@include file="partes/head.jsp" %>
<%@include file="partes/barra.jsp" %>
<style>
    th { font-size: 12px; }
    td { font-size: 12px; }
</style>
<%    proveedor_neg pr = new proveedor_neg();
    ArrayList<beanproveedor> ls = pr.lstProveedor();
%>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="#">Home</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">Genera Orden Compra</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-md-12">
        <div class="card border-success">
            <div class="card-header">
                <p>Orden de Compra</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Seleccione Proveedor</label>
                        <input type="text" name="ruc" id="ruc" class="form-control form-control-sm">
                        <input type="text" name="idproveedor" id="idproveedor" class="form-control form-control-sm" style="display: none">
                    </div>
                    <div class="col-md-12 mb-3">
                        <br>
                        <label>Incluye IGV ?</label><br>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="rbestado" name="rbigv" value="1" checked="">Si
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="rbestado" name="rbigv" value="0">No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table table-responsive">
                            <table id="TBLDETALLE" class="table table-hover">
                                <thead>
                                <th>Cant</th>
                                <th>Descripcion</th>
                                <th>Precio</th>
                                <th>TOTAL</th>
                                <th>Accion</th>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><strong>Subtotal :</strong></td>
                                        <td><strong><span id="totsubtotal"></span></strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><strong>IGV :</strong></td>
                                        <td><strong><span id="totigv"></span></strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><strong>Neto :</strong></td>
                                        <td><strong><span id="totneto"></span></strong></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-sm btn-block" onclick="RegistrarOrdencita()">Registrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        //ComboProv();
        load();
        $("#ruc").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "cotizacion?action=filtroProv",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    focus: function (event, ui) {
                        event.preventDefault();
                        alert();
                        $(this).val(ui.item.ruc + "-----");
                    },
                    success: function (data) {
                        if (data.data.length == 0) {
                            $('#idproveedor').val(0);
                        }
                        response($.map(data.data, function (item) {
                            return {
                                value: item.concat,
                                id: item.idproveedor
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                var idprov = ui.item.id;
                $('#idproveedor').val(idprov);
            }
        });
    });
    function RegistrarOrdencita() {
        $.ajax({
            type: "POST",
            url: "reqCompra?action=regOrden",
            data: "idproveedor=" + $("#idproveedor").val(),
            success: function (response)
            {
                if (response == "1") {
                    $.confirm({
                        title: 'Aviso!',
                        content: 'Exito!',
                        buttons: {
                            confirm: function () {
                                window.location = "index?action=reqGen";
                            }
                        }
                    });
                } else {
                    $.alert(response);
                }


            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    $('input[type=radio][name=rbigv]').change(function () {
        isIgv(this.value);
        load();
    });
    function isIgv(estado) {
        $.ajax({
            type: "POST",
            url: "reqCompra?action=checkigv",
            data: "rbigv=" + estado,
            success: function (response)
            {
                console.log(response);
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    function test() {
        $.ajax({
            type: "POST",
            url: "reqCompra?action=getPrecios",
            success: function (response)
            {
                var json = JSON.parse(response);
                $.each(json.data, function (key, registro) {
                    $("#totsubtotal").text(registro.item0);
                    $("#totigv").text(registro.item1);
                    $("#totneto").text(registro.item2);
                });
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    function KeyUp(indice, event) {
        var codigo = event.which || event.keyCode;
        var precio = event.target.value;
        if (codigo == 13 && precio != "") {
            $.ajax({
                type: "POST",
                url: "reqCompra?action=addPrecio",
                data: "indice=" + indice + "&precio=" + precio,
                success: function (response)
                {
                    var json = JSON.parse(response);
                    GenTableDet(json);
                    test();
                }, error: function (response) {
                    $.alert(response);
                }
            });
        }
    }
    function KeyUpCant(indice, event) {
        var codigo = event.which || event.keyCode;
        var cantidad = event.target.value;
        if (codigo == 13 && cantidad != "") {
            $.ajax({
                type: "POST",
                url: "reqCompra?action=changeCant",
                data: "indice=" + indice + "&cantidad=" + cantidad,
                success: function (response)
                {
                    var json = JSON.parse(response);
                    GenTableDet(json);
                    test();
                }, error: function (response) {
                    $.alert(response);
                }
            });
        }
    }
    function deleteItem(indice) {
        $.ajax({
            type: "POST",
            url: "reqCompra?action=delete",
            data: "indice=" + indice,
            success: function (response)
            {
                var json = JSON.parse(response);
                GenTableDet(json);
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    function load() {
        $.ajax({
            type: "POST",
            url: "reqCompra?action=viewReq",
            success: function (response)
            {
                var json = JSON.parse(response);
                GenTableDet(json);
            }, error: function (response) {
                $.alert(response);
            }
        });
    }
    function GenTableDet(json) {
        $('#TBLDETALLE').dataTable().fnDestroy();
        $('#TBLDETALLE').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            "data": json.data,
            columns: [
                //{data: "indice"},
                {data: null, //cant
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return '<input id="cantid" class="form-control form-control-sm" name="cantidad" type="number" onkeyUp="KeyUpCant(' + data.indice + ',event)" value="' + data.cant + '">';
                    }
                },
                {data: "descripcion"},
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        var precio = data.precio;
                        return '<input id="precioid' + data.indice + '" type="number" onkeyup="KeyUp(' + data.indice + ',event)" class="form-control form-control-sm" value="' + precio + '">';
                    }
                },
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return '<strong>' + data.total + '</strong>'
                    }
                },
                {data: null,
                    ClassName: 'center',
                    mRender: function (data, type, row) {
                        return '<button class="btn btn-danger btn-sm" onclick="deleteItem(' + data.indice + ')"><i class="fas fa-trash-alt"></i></button>';
                    }
                }
            ]
        });
        test();
    }
</script>
<%@include file="partes/footer.jsp" %>